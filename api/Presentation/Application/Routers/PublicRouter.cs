﻿using AutoMapper;
using Core.Models;
using Core;
using Microsoft.AspNetCore.Mvc;
using Application.Controllers.PublicCtrl;
using Application.Controllers.PublicCtrl.Presenter; 
using Application.Controllers;
using Domain.Util;

namespace Application.Routers
{
    public class PublicRouter
    { 
        public IEnumerable<RouterModel> Get(byte[] secretKey)
        {
            List<RouterModel> routers = new List<RouterModel>();
            var routeInfos = new List<RouterModel>
            {
                  new RouterModel()
                  {
                        Method = "POST",
                        Module = "Auth",
                        Path = "auth/login",
                        ProfileType = PermUtil.PUBLIC_PROFILE,
                        Action = ([FromBody] LoginPresenter authPresenter, DataContext db, IMapper mapper) => 
                                 new LoginCtrl(db, secretKey, mapper).Execute(authPresenter)
                  },
                  new RouterModel()
                  {
                        Method = "POST",
                        Module = "Auth",
                        Path = "auth/refresh-token",
                        ProfileType = PermUtil.PUBLIC_PROFILE,
                        Action = ([FromBody] TokenPresenter model, DataContext db, IMapper mapper) => new RefreshTokenCtrl(db, secretKey, mapper).Execute(model)
                  }, 
                  new RouterModel()
                  {
                        Method = "GET",
                        Module = "Seed",
                        Path = "seed",
                        ProfileType = PermUtil.PUBLIC_PROFILE,
                        Action = async (HttpContext httpContext, DataContext db, IMapper mapper) => 
                                 await new SeedCtrl(httpContext, db, secretKey, mapper).Execute()
                  },
                  new RouterModel()
                  {
                        Method = "POST",
                        Module = "Upload",
                        Path = "upload",
                        ProfileType = PermUtil.PUBLIC_PROFILE,
                        Action = async (IFormFile file) => await new FileCtrl().Post(file)
                  }
            };

            foreach (var routeInfo in routeInfos)
            {
                routers.Add(routeInfo);
            }
            return routers;
        }
    }
}
