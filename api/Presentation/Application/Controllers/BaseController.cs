﻿using AutoMapper;
using Core;

namespace Application.Controllers
{
    public class BaseController
    {
        #region common
        protected readonly DataContext context;
        protected readonly byte[] secretKey;
        protected readonly IMapper mapper;
        protected readonly HttpContext httpContext;
        #endregion
          

        public BaseController(HttpContext _httpContext, DataContext ctx, byte[] _secretKey, IMapper _mapper)
        {
            #region common
            secretKey = _secretKey;
            mapper = _mapper;
            context = ctx;
            httpContext = _httpContext;
            #endregion
        }

    }
}
