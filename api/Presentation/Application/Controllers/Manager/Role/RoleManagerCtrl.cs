﻿using Application.Controllers.Manager.Role.Presenter;
using AutoMapper;
using Core;
using Core.Domain.Business;
using Core.Models;
using Domain.Schemas;
using Microsoft.EntityFrameworkCore;

namespace Application.Controllers.Manager.Role
{
    public class RoleManagerCtrl
    {
        private readonly IMapper mapper;
        private readonly DataContext dataContext;

        public RoleManagerCtrl(DataContext _dataContext, IMapper _mapper)
        {
            mapper = _mapper;
            dataContext = _dataContext;
        }

        public async Task<IResult> Get()
        {
            var roleWithPerms = dataContext.Roles.Include(r => r.RolesPerms)
                            .ThenInclude(rp => rp.Perm)
                            .Select(u => UserRule.GetPerm(u))
                            .ToList();
            return Results.Ok(roleWithPerms);
        }
        public IResult Create(CreateRolePresenter model)
        {
            var existRole = dataContext.Roles.FirstOrDefault(u => u.Title.Equals(model.Title));

            if (existRole != null)
            {
                return Results.BadRequest("Title is already used !");
            }

            CreateRoleModel roleModel = mapper.Map<CreateRoleModel>(model);

            RoleSchema role = new RoleSchema
            {
                Description = roleModel.Description,
                Title = roleModel.Title,
            };

            var response = dataContext.Roles.Add(role);
            return Results.Ok(response);
        }

        public async Task<IResult> Update(UpdateRolePresenter model)
        {
            var existRole = dataContext.Roles.FirstOrDefault(u => u.Title.Equals(model.Title));
            if (existRole != null)
            {
                return Results.BadRequest("Title is already used!");
            }

            var role = await dataContext.Roles.FirstOrDefaultAsync(x=>x.Id == model.Id);
            if (role == null)
            {
                return Results.BadRequest("Role not found");
            }

            UpdateRoleModel roleModel = mapper.Map<UpdateRoleModel>(model);

            role.Description = roleModel.Description;
            role.Title = roleModel.Title;

            var response = dataContext.Roles.Update(role);
            return Results.Ok(response);
        }

        public async Task<IResult> UpdatePermForRole(UpdatePermForRolePresenter model)
        {
            var roleModel = mapper.Map<UpdatePermForRoleModel>(model);

            var role = await dataContext.Roles.FirstOrDefaultAsync(x => x.Id == model.RoleId);
            if (role == null)
            {
                return Results.BadRequest("Not found role");
            }

            var oldPerm = dataContext.RolesPerms.Where(rp => !model.PermIds.Contains(rp.PermId) && rp.RoleId == model.RoleId);

            dataContext.RolesPerms.RemoveRange(oldPerm);

            foreach (var permId in model.PermIds)
            {
                var rolePerm = dataContext.RolesPerms.FirstOrDefault(rp => rp.PermId == permId && rp.RoleId == model.RoleId);
                if (rolePerm == null)
                {
                    dataContext.RolesPerms.Add(new RolesPerms
                    {
                        RoleId = model.RoleId,
                        PermId = permId
                    });
                }
            }

            dataContext.SaveChanges();
            var newPermList = dataContext.Roles.Include(r => r.RolesPerms)
                            .ThenInclude(rp => rp.Perm)
                            .Select(u => UserRule.GetPerm(u))
                            .ToList(); 

            return Results.Ok(newPermList);
        }

        public async Task<IResult> Delete(long id)
        {
            var role = await dataContext.Roles.FirstOrDefaultAsync(x => x.Id == id);

            if (role == null)
            {
                return Results.BadRequest("Role not found");
            }

            dataContext.Roles.Remove(role);
            return Results.NoContent();
        }
    }
}
