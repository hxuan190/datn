﻿using Core.Models;
using Core;
using AutoMapper;
using Application.Controllers.PublicCtrl.Presenter;
using UseCase;
using Domain;
using Domain.Util;

namespace Application.Controllers.PublicCtrl
{
    public class LoginCtrl
    {
        private readonly IMapper _mapper;
        private readonly LoginFlow workflow;
        private readonly byte[] secretKey;

        public LoginCtrl(DataContext ctx, byte[] _secretKey, IMapper mapper)
        {
            secretKey = _secretKey;
            _mapper = mapper;
            workflow = new LoginFlow(ctx);
        }

        public IResult Execute(LoginPresenter authPresenter)
        {
            LoginModel model = _mapper.Map<LoginModel>(authPresenter);
            ResponseModel response = workflow.Execute(model, secretKey);
            if (response.Status == GlobalVariable.ERROR)
            {
                return Results.BadRequest();
            }
            return Results.Ok(response.Result);
        }
    }
}
