﻿namespace Core.Models
{
	public class CreateRoleModel
	{
		public string Title { get; set; } 
		public string Description { get; set; } 
	}
}
