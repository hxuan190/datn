﻿using Core.Models.SignalR;

namespace Domain.Interface
{
    public interface IHubClientService
    {
        Task PushNotification(BaseSignalRModel model);
    }
}
