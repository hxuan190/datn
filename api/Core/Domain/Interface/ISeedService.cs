﻿using Core.Models; 
namespace Domain.Interface
{
    public interface ISeedService
    {
        Task<ResponseModel> Execute(List<RouterModel> routers);
    }
}
