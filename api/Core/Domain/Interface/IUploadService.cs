﻿

using Microsoft.AspNetCore.Http;

namespace Domain.Interface
{
	public interface IUploadService
	{
		Task<string> UploadImage(IFormFile file);
	}
}
