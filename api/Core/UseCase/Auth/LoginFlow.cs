﻿using Core.Models;
using Core;
using Microsoft.EntityFrameworkCore;
using Domain;
using Domain.Schemas;
using Domain.Util;
using Core.Domain.Business;

namespace UseCase
{
    public class LoginFlow
    {
        private readonly DataContext dataContext;
        public LoginFlow(DataContext ctx)
        {
            dataContext = ctx;
        }
         
        public ResponseModel Execute(LoginModel model, byte[] secretKey)
        {
            UserSchema user = dataContext.Users.Where(u => u.Email.Equals(model.Email)).FirstOrDefault();
            if (user == null)
            {
                return new ResponseModel(GlobalVariable.ERROR, new { });
            } 
            bool isMatched = JwtUtil.Compare(model.Password, user.Password);
            if (!isMatched)
            {
                return new ResponseModel(GlobalVariable.ERROR, new { });
            }
            UserRoleModel userPermModel = dataContext.Users
               .Where(u => u.Id == user.Id)
               .Include(u => u.UserRoles)
                   .ThenInclude(ur => ur.Role)
                       .ThenInclude(r => r.RolesPerms)
                           .ThenInclude(rp => rp.Perm)
               .Select(u => UserRule.GetUserRole(u))
               .FirstOrDefault();

            UserRoleModel userPerm = userPermModel;
            string accessToken = JwtUtil.GenerateAccessToken(userPerm, secretKey);
            string refreshToken = JwtUtil.GenerateRefreshToken(); 
            UserSchema? u = dataContext.Users.Find(user.Id);
            if (u != null)
            {
                u.HashRefreshToken = refreshToken;
                u.LastLogin = DateTime.UtcNow;
                dataContext.SaveChanges();
            }
            return new ResponseModel(GlobalVariable.SUCCESS, new { AccessToken = accessToken, RefreshToken = refreshToken, User = userPerm });
        }
    }
}
