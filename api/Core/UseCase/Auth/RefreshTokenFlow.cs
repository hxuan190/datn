﻿using Core.Models;
using Microsoft.EntityFrameworkCore;
using Core;
using Domain;
using System.IdentityModel.Tokens.Jwt;
using Domain.Schemas;
using Domain.Util;
using Core.Domain.Business;

namespace UseCase
{
    public class RefreshTokenFlow
    {
        private readonly DataContext dataContext;
        public RefreshTokenFlow(DataContext ctx)
        {
            dataContext = ctx;
        }

        public async Task<ResponseModel> Execute(TokenModel model, byte[] secretKey)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(model.AccessToken);
            var userCredentialString = jwtToken.Claims.First(x => x.Type == "id").Value;
            long userId = long.Parse(userCredentialString);
            UserSchema u = await dataContext.Users.FirstOrDefaultAsync(x=> x.Id == userId);

			bool isMatched = u.HashRefreshToken.Equals(model.RefreshToken);
            if (!isMatched)
            {
                return new ResponseModel(GlobalVariable.ERROR, new { });
            }
            UserRoleModel userPermModel = dataContext.Users
            .Where(u => u.Id == u.Id)
            .Include(u => u.UserRoles)
                .ThenInclude(ur => ur.Role)
                    .ThenInclude(r => r.RolesPerms)
                        .ThenInclude(rp => rp.Perm)
            .Select(u => UserRule.GetUserRole(u))
            .FirstOrDefault();
            UserRoleModel userPerm = userPermModel;
            var newToken = JwtUtil.GenerateAccessToken(userPerm, secretKey);
            var newRefreshToken = JwtUtil.GenerateRefreshToken();
            return new ResponseModel(GlobalVariable.SUCCESS, new
            {
                AccessToken = newToken,
                RefreshToken = newRefreshToken
            });

        }
    }
}
