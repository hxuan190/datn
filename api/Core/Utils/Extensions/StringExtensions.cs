﻿using System.Text.RegularExpressions;

namespace Util.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Filter the list of strings via pattern
        /// </summary>
        /// <param name="sources"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static IEnumerable<string> FilterByPattern(this IEnumerable<string> sources, string pattern)
        {
            var filteredList = new List<string>();

            // Escape special characters in the pattern
            var regexPattern = Regex.Escape(pattern)
                .Replace("\\*", ".*") // Replace wildcard asterisk with regex wildcard
                .Replace("\\?", "."); // Replace wildcard question mark with regex single character wildcard

			var regexTimeout = TimeSpan.FromSeconds(10); // Set the timeout to 10 seconds
			// Create a regex pattern from the modified pattern string
			var regex = new Regex(regexPattern, RegexOptions.None, regexTimeout);
			// Iterate over the input list and check if each string matches the pattern
			foreach (var item in sources)
            {
                if (regex.IsMatch(item))
                {
                    filteredList.Add(item);
                }
            }
            return filteredList;
        }
    }
}
