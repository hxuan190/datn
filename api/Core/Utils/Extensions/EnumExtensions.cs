﻿using System.ComponentModel;
using System.Reflection;

namespace Util.Extensions
{
    public static class EnumExtensions
    {
        //tech debt should update https://stackoverflow.com/questions/37305985/enum-description-attribute-in-dotnet-core
        public static string GetDescription(this Enum value)
        {
            if (value is null)
            {
                return string.Empty;
            }
            return value.GetType()
                    .GetField(value.ToString())
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description ??
                value.ToString();
        }

        /// <summary>Return a list of description values for the given enum.</summary>
        /// <typeparam name="T">Enum type passed in.</typeparam>
        /// <param name="value">Value of the enum to get descriptions.</param>
        /// <returns>An array of description values for the given Enum value.</returns>
        /// <remarks>Designed to be used with flags Enum values, however will return a single item for non-flags Enum values.</remarks>
        /// <seealso cref="GetDescriptionsAsEnglishList{T}(T)" />
        /// <seealso cref="FlagsAttribute" />
        /// <seealso cref="GetDescription(Enum)" />
        public static IEnumerable<string> GetDescriptions<T>(this T value) where T : Enum
        {
            if (typeof(T).GetCustomAttribute<FlagsAttribute>() is null)
            {
                yield return GetDescription(value);
                yield break;
            }

            foreach (T flagsValue in Enum.GetValues(typeof(T)))
            {
                if (value.HasFlag(flagsValue)) yield return GetDescription(flagsValue);
            }
        }

        /// <summary>Build an English list of the descriptions of the given Enum values.</summary>
        /// <typeparam name="T">Type of the Enum</typeparam>
        /// <param name="value">Value to build list of descriptions from.</param>
        /// <returns>An English list of descriptions joined with commas and "and".</returns>
        /// <seealso cref="GetDescriptions{T}(T)" />
        public static string GetDescriptionsAsEnglishList<T>(this T value) where T : Enum
        {
            var descriptions = GetDescriptions(value).ToArray();

            return descriptions switch
            {
                { Length: 0 } => string.Empty,
                { Length: 1 } => descriptions[0],
                { Length: 2 } => $"{descriptions[0]} and {descriptions[1]}",
                _ => $"{string.Join(", ", descriptions[0..^1])}, and {descriptions[^1]}"
            };
        }

        public static T[] GetValues<T>() where T : struct, IConvertible
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToArray();
        }

        public static TEnum ParseEnum<TEnum>(this string value)
        {
            foreach (var enumValue in Enum.GetValues(typeof(TEnum)))
            {
                if (enumValue.ToString().Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return (TEnum)enumValue;
                }
            }

            throw new ArgumentException($"Invalid value '{value}' for enum {typeof(TEnum).Name}");

        }
    }
}
