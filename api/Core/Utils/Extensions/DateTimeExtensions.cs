﻿namespace Util.Extensions
{
    public static class DateTimeExtensions
    {
        public static double ConvertToTimestampDouble(DateTime value)
        {
            //create Timespan by subtracting the value provided from
            //the Unix Epoch
            var span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).ToUniversalTime());

            //return the total seconds (which is a UNIX timestamp)
            return (double)span.TotalSeconds;
        }
    }
}
