﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Util.Extensions
{
	public static class SecurityExtensions
	{
        private const int IterCount = 600_000;
        /// <summary>
        /// Generate strong and random password, have upper case, lower case, special characters and numbers, min 8 and max 16 characters
        /// </summary>
        /// <returns></returns>
        public static string GenerateRandomPassword()
		{
			var upperPart = GenerateRandomString(6, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			var lowerPart = GenerateRandomString(6, "abcdefghijklmnopqrstuvwxyz");
			var specialPart = GenerateRandomString(2, "!@#$%^&*");
			var numberPart = GenerateRandomString(2, "0123456789");
			return $"{upperPart}{specialPart}{numberPart}{lowerPart}";
		}
		/// <summary>
		/// Generate OTP code in 6 numeric characters
		/// </summary>
		/// <returns></returns>
        public static string GenerateOtp() => GenerateRandomString(6, "0123456789");

        /// <summary>
        /// Generate AES Raw key in 30 characters
        /// </summary>
        /// <returns></returns>
        public static string GenerateAesRawKey() => GenerateRandomString(30);

		/// <summary>
		/// Check if password is strong enough, have upper case, lower case, special characters and numbers, min 8 and max 16 characters
		/// </summary>
		/// <returns></returns>
		public static bool IsStrongPassword(string password)
		{
			if (string.IsNullOrEmpty(password))
				return false;
			// Define the regex patterns for different password criteria
			var uppercasePattern = "[A-Z]";
			var lowercasePattern = "[a-z]";
			var digitPattern = "[0-9]";
			var specialCharacterPattern = @"[!@#$%^&*()\-_=+\\|\[\]{};:',<.>/?\""]";

			// Check the length of the password (minimum length of 8 characters)
			if (password.Length < 8)
				return false;

			var regexTimeout = TimeSpan.FromSeconds(10); // Set the timeout to 10 seconds
			// Check if the password contains at least one uppercase letter
			if (!Regex.IsMatch(password, uppercasePattern, RegexOptions.None, regexTimeout))
				return false;

			// Check if the password contains at least one lowercase letter
			if (!Regex.IsMatch(password, lowercasePattern, RegexOptions.None, regexTimeout))
				return false;

			// Check if the password contains at least one digit
			if (!Regex.IsMatch(password, digitPattern, RegexOptions.None, regexTimeout))
				return false;

			// Check if the password contains at least one special character
			if (!Regex.IsMatch(password, specialCharacterPattern, RegexOptions.None, regexTimeout))
				return false;

			// If all criteria are met, the password is strong
			return true;
		}

        /// <summary>
        /// Hash password with pepper
        /// </summary>
        /// <param name="pepper"></param>
        /// <param name="password"></param>
        /// <param name="rng"></param>
        /// <returns></returns>
		public static byte[] HashPasswordV3(string pepper, string password, RandomNumberGenerator rng)
        {
            return HashPasswordV3(pepper, password, rng,
                prf: KeyDerivationPrf.HMACSHA512,
                iterCount: IterCount,
                saltSize: 128 / 8,
                numBytesRequested: 256 / 8);
        }
        /// <summary>
        /// Verify password with pepper
        /// </summary>
        /// <param name="pepper"></param>
        /// <param name="hashedPassword"></param>
        /// <param name="plainPassword"></param>
        /// <param name="iterCount"></param>
        /// <param name="prf"></param>
        /// <returns></returns>
        public static bool VerifyHashedPasswordV3(string pepper, byte[] hashedPassword, string plainPassword, out int iterCount, out KeyDerivationPrf prf)
        {
            //first => encrypt the password with SHA512
            var password = SHA512_ComputeHash(plainPassword, pepper);

            iterCount = default(int);
            prf = default(KeyDerivationPrf);

            try
            {
                // Read header information
                prf = (KeyDerivationPrf)ReadNetworkByteOrder(hashedPassword, 1);
                iterCount = (int)ReadNetworkByteOrder(hashedPassword, 5);
                int saltLength = (int)ReadNetworkByteOrder(hashedPassword, 9);

                // Read the salt: must be >= 128 bits
                if (saltLength < 128 / 8)
                {
                    return false;
                }
                byte[] salt = new byte[saltLength];
                Buffer.BlockCopy(hashedPassword, 13, salt, 0, salt.Length);

                // Read the subkey (the rest of the payload): must be >= 128 bits
                int subkeyLength = hashedPassword.Length - 13 - salt.Length;
                if (subkeyLength < 128 / 8)
                {
                    return false;
                }
                byte[] expectedSubkey = new byte[subkeyLength];
                Buffer.BlockCopy(hashedPassword, 13 + salt.Length, expectedSubkey, 0, expectedSubkey.Length);

                // Hash the incoming password and verify it
                byte[] actualSubkey = KeyDerivation.Pbkdf2(password, salt, prf, iterCount, subkeyLength);
#if NETSTANDARD2_0 || NETFRAMEWORK
            return ByteArraysEqual(actualSubkey, expectedSubkey);
#elif NETCOREAPP
                return CryptographicOperations.FixedTimeEquals(actualSubkey, expectedSubkey);
#else
#error Update target frameworks
#endif
            }
            catch
            {
                // This should never occur except in the case of a malformed payload, where
                // we might go off the end of the array. Regardless, a malformed payload
                // implies verification failed.
                return false;
            }
        }

        /// <summary>
        /// Encrypt with RSA public key
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="base64PublicKey">RSA public key in base64</param>
        /// <returns>Base64 cipher string</returns>
        public static string EncryptWithRsaPublicKey(string plainText, string base64PublicKey)
        {
            var rsa = RSA.Create();
            rsa.ImportFromPem(base64PublicKey);
            var plaintextBytes = Encoding.UTF8.GetBytes(plainText);
            var encryptedBytes = rsa.Encrypt(plaintextBytes, RSAEncryptionPadding.OaepSHA256);
            return Convert.ToBase64String(encryptedBytes);
        }
        private static byte[] HashPasswordV3(string pepper, string plainPassword, RandomNumberGenerator rng, KeyDerivationPrf prf, int iterCount, int saltSize, int numBytesRequested)
        {
            //first => encrypt the password with SHA512
            var password = SHA512_ComputeHash(plainPassword, pepper);

            // Produce a version 3 (see comment above) text hash.
            byte[] salt = new byte[saltSize];
            rng.GetBytes(salt);
            byte[] subkey = KeyDerivation.Pbkdf2(password, salt, prf, iterCount, numBytesRequested);

            var outputBytes = new byte[13 + salt.Length + subkey.Length];
            outputBytes[0] = 0x01; // format marker
            WriteNetworkByteOrder(outputBytes, 1, (uint)prf);
            WriteNetworkByteOrder(outputBytes, 5, (uint)iterCount);
            WriteNetworkByteOrder(outputBytes, 9, (uint)saltSize);
            Buffer.BlockCopy(salt, 0, outputBytes, 13, salt.Length);
            Buffer.BlockCopy(subkey, 0, outputBytes, 13 + saltSize, subkey.Length);
            return outputBytes;
        }
        private static string SHA512_ComputeHash(string text, string secretKey)
        {
            var hash = new StringBuilder();
            var secretkeyBytes = Encoding.UTF8.GetBytes(secretKey);
            var inputBytes = Encoding.UTF8.GetBytes(text);
            using (var hmac = new HMACSHA512(secretkeyBytes))
            {
                var hashValue = hmac.ComputeHash(inputBytes);
                foreach (var theByte in hashValue)
                {
                    hash.Append(theByte.ToString("x2"));
                }
            }
            return hash.ToString();
        }

#if NETSTANDARD2_0 || NETFRAMEWORK
    // Compares two byte arrays for equality. The method is specifically written so that the loop is not optimized.
    [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
    private static bool ByteArraysEqual(byte[] a, byte[] b)
    {
        if (a == null && b == null)
        {
            return true;
        }
        if (a == null || b == null || a.Length != b.Length)
        {
            return false;
        }
        var areSame = true;
        for (var i = 0; i < a.Length; i++)
        {
            areSame &= (a[i] == b[i]);
        }
        return areSame;
    }
#endif
        private static uint ReadNetworkByteOrder(byte[] buffer, int offset)
        {
            return ((uint)(buffer[offset + 0]) << 24)
                   | ((uint)(buffer[offset + 1]) << 16)
                   | ((uint)(buffer[offset + 2]) << 8)
                   | ((uint)(buffer[offset + 3]));
        }

        private static void WriteNetworkByteOrder(byte[] buffer, int offset, uint value)
        {
            buffer[offset + 0] = (byte)(value >> 24);
            buffer[offset + 1] = (byte)(value >> 16);
            buffer[offset + 2] = (byte)(value >> 8);
            buffer[offset + 3] = (byte)(value >> 0);
        }

        /// <summary>
        /// Generate a random string
        /// </summary>
        /// <param name="length"></param>
        /// <param name="seeds"></param>
        /// <returns></returns>
        private static string GenerateRandomString(int length, string seeds = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
		{
			var randomBytes = new byte[length];
			var stringBuilder = new StringBuilder(length);
			foreach (var randomByte in randomBytes)
			{
				using var rng = RandomNumberGenerator.Create();
				rng.GetBytes(randomBytes);
				var randomIndex = randomByte % seeds.Length;
				stringBuilder.Append(seeds[randomIndex]);
			}
			return stringBuilder.ToString();
		}
	}
}
