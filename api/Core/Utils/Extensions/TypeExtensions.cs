﻿using System.Reflection;

namespace Util.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class TypeExtensions
    {
        public static bool IsClosedTypeOf(this Type type, Type openGenericType)
        {
            return TypesAssignableFrom(type).Any(t =>
                t.GetTypeInfo().IsGenericType && !type.GetTypeInfo().ContainsGenericParameters &&
                t.GetGenericTypeDefinition() == openGenericType);
        }

        public static Type[] GetTypesThatClose(this Type type, Type openGenericType)
        {
            return TypesAssignableFrom(type)
                .Where(t => t.IsClosedTypeOf(openGenericType))
                .ToArray();
        }

        private static IEnumerable<Type> TypesAssignableFrom(Type candidateType)
        {
#pragma warning disable CS8603 // Possible null reference return. Getting rid of this warning with nullable reference types was too complex.
            return candidateType.GetTypeInfo().ImplementedInterfaces.Concat(
                TraverseAcross(candidateType, t => t.GetTypeInfo().BaseType));
#pragma warning restore CS8603 // Possible null reference return.
        }

        private static IEnumerable<T> TraverseAcross<T>(T first, Func<T, T> next)
            where T : class
        {
            var item = first;
            while (item != null)
            {
                yield return item;
                item = next(item);
            }
        }
    }
}
