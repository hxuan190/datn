﻿using System.Text;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Serilog;

namespace Util.Extensions
{
    public static class DistributedCacheExtensions
    {
        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="cache"></param>
        /// <param name="key">Key of cached item</param>
        /// <exception cref="Exception"></exception>
        /// <returns>The cached value associated with the specified key</returns>
        private static async Task<T> Get<T>(this IDistributedCache cache, string key)
        {
            //get serialized item from cache
            var serializedItem = await cache.GetAsync(key);
            if (serializedItem == null)
#pragma warning disable S112
                throw new Exception($"Cache was already expired for {key}");
#pragma warning restore S112
            //deserialize item
            var item = JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(serializedItem), _serializerOptions);
            return item ?? default(T)!;
        }
        /// <summary>
        /// Get a cached item. If it's not in the cache yet, then load and cache it
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="key">Cache key</param>
        /// <param name="acquire">Function to load item if it's not in the cache yet</param>
        /// <returns>Cached item</returns>
        public static async Task<T> Get<T>(this IDistributedCache cacheManager, string key, Func<T> acquire)
        {
            //use default cache time
            return await Get(cacheManager, key, acquire, 60);
        }

        /// <summary>
        /// Get a cached item. If it's not in the cache yet, then load and cache it
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="key">Cache key</param>
        /// <param name="acquire">Function to load item if it's not in the cache yet</param>
        /// <param name="minuteCache"></param>
        /// <returns>Cached item</returns>
        public static async Task<T> Get<T>(this IDistributedCache cacheManager, string key, Func<T> acquire, int minuteCache)
        {
            //use default cache time
            return await Get(cacheManager, key, minuteCache, acquire);
        }

        /// <summary>
        /// Get a cached item. If it's not in the cache yet, then load and cache it
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="key">Cache key</param>
        /// <param name="cacheTime">Cache time in minutes (0 - do not cache)</param>
        /// <param name="acquire">Function to load item if it's not in the cache yet</param>
        /// <returns>Cached item</returns>
        public static async Task<T> Get<T>(this IDistributedCache cacheManager, string key, int cacheTime, Func<T> acquire)
        {
            var logger = Log.ForContext("IDistributedCache", null);
            var cacheIsExpired = false;
            //item already is in cache, so return it
            if (cacheManager.IsSet(key))
            {
                logger.Debug("Cache found for key {Key}", key);
                try
                {
                    return await cacheManager.Get<T>(key);
                }
                catch (Exception)
                {
                    cacheIsExpired = true;
                }
            }
            logger.Debug(cacheIsExpired? 
                "Cache was expired for key {Key}" : 
                "Cache miss for key {Key}", 
                key);
            //or create it using passed function
            var result = acquire();
            //and set in cache (if cache time is defined)
            if (cacheTime > 0)
                await cacheManager.Set(key, result!, cacheTime);
            return result;
        }

        /// <summary>
        /// Adds the specified key and object to the cache
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="key">Key of cached item</param>
        /// <param name="data">Value for caching</param>
        /// <param name="cacheTime">Cache time in minutes</param>
        public static Task Set(this IDistributedCache cache, string key, object data, int cacheTime = 60)
        {
            var cacheEntryOptions = new DistributedCacheEntryOptions()
                .SetAbsoluteExpiration(TimeSpan.FromSeconds(cacheTime * 60));
            return Set(cache, key, data, cacheEntryOptions);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static Task Set<T>(this IDistributedCache cache, string key, T value, DistributedCacheEntryOptions options)
        {
            var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(value, _serializerOptions));
            return cache.SetAsync(key, bytes, options);
        }


        /// <summary>
        /// Gets a value indicating whether the value associated with the specified key is cached
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="key">Key of cached item</param>
        /// <returns>True if item already is in cache; otherwise false</returns>
        public static bool IsSet(this IDistributedCache cache, string key)
        {
            if (cache.GetType() != typeof(MemoryDistributedCache))
            {
                throw new ArgumentException("This method is only available for MemoryDistributedCache");
            }
            var memoryCache = (MemoryDistributedCache)cache;
            return memoryCache.GetKeys<string>().Contains(key);
        }

        /// <summary>
        /// Removes the value with the specified key from the cache
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="key">Key of cached item</param>
        public static Task Delete(this IDistributedCache cache, string key)
        {
            return cache.RemoveAsync(key);
        }

        /// <summary>
        /// Removes items by key pattern
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="pattern">String key pattern</param>
        public static void DeleteByPattern(this IDistributedCache cache, string pattern)
        {
            if (cache.GetType() != typeof(MemoryDistributedCache))
            {
                throw new ArgumentException("This method is only available for IMemoryCache");
            }
            var memoryCache = (MemoryDistributedCache)cache;
            var allKeys = memoryCache.GetKeys<string>();
            var keyShouldBeRemoved = allKeys.FilterByPattern(pattern);
            foreach (var key in keyShouldBeRemoved)
            {
                memoryCache.Remove(key);
            }
        }

        /// <summary>
        /// Clear all cache data
        /// </summary>
        public static void Clear(this IDistributedCache cache)
        {
            if (cache.GetType() != typeof(MemoryDistributedCache))
            {
                throw new ArgumentException("This method is only available for IMemoryCache");
            }
            var memoryCache = (MemoryDistributedCache)cache;
            foreach (var item in memoryCache.GetKeys<string>())
            {
                memoryCache.Remove(item);
            }
        }

        private static JsonSerializerSettings _serializerOptions = new()
        {
            ContractResolver = new PrivateResolver(),
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
        };
    }
}
