﻿using System.Collections;
using Microsoft.Extensions.Caching.Memory;
using System.Reflection.Emit;
using System.Reflection;
using Microsoft.Extensions.Caching.Distributed;

namespace Util.Extensions
{
    /// <summary>
    /// Extension to access the memory cache entries that not available via Microsoft public api
    /// </summary>
    /// <see cref="https://stackoverflow.com/questions/45597057/how-to-retrieve-a-list-of-memory-cache-keys-in-asp-net-core"/>
    /// <example>
    /// var cache = new MemoryCache(new MemoryCacheOptions());
    /// cache.GetOrCreate(1, ce => "one");
    /// cache.GetOrCreate("two", ce => "two");
    /// foreach (var key in cache.GetKeys())
    ///    Console.WriteLine($"Key: '{key}', Key type: '{key.GetType()}'");
    /// </example>
    public static class MemoryCacheExtensions
    {
        #region Microsoft.Extensions.Caching.Memory_6_OR_OLDER

        private static readonly Lazy<Func<MemoryCache, object>> GetEntries6 =
            new Lazy<Func<MemoryCache, object>>(() => (Func<MemoryCache, object>)Delegate.CreateDelegate(
                typeof(Func<MemoryCache, object>),
                typeof(MemoryCache).GetProperty("EntriesCollection", BindingFlags.NonPublic | BindingFlags.Instance).GetGetMethod(true),
                throwOnBindFailure: true));

        #endregion

        #region Microsoft.Extensions.Caching.Memory_7_OR_NEWER

        private static readonly Lazy<Func<MemoryCache, object>> GetCoherentState =
            new Lazy<Func<MemoryCache, object>>(() =>
                CreateGetter<MemoryCache, object>(typeof(MemoryCache)
                    .GetField("_coherentState", BindingFlags.NonPublic | BindingFlags.Instance)));

        private static readonly Lazy<Func<object, IDictionary>> GetEntries7 =
            new Lazy<Func<object, IDictionary>>(() =>
                CreateGetter<object, IDictionary>(typeof(MemoryCache)
                    .GetNestedType("CoherentState", BindingFlags.NonPublic)
                    .GetField("_entries", BindingFlags.NonPublic | BindingFlags.Instance)));

        private static Func<TParam, TReturn> CreateGetter<TParam, TReturn>(FieldInfo field)
        {
            var methodName = $"{field.ReflectedType.FullName}.get_{field.Name}";
            var method = new DynamicMethod(methodName, typeof(TReturn), new[] { typeof(TParam) }, typeof(TParam), true);
            var ilGen = method.GetILGenerator();
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldfld, field);
            ilGen.Emit(OpCodes.Ret);
            return (Func<TParam, TReturn>)method.CreateDelegate(typeof(Func<TParam, TReturn>));
        }

        #endregion
        private static MemoryCache? GetMemoryCache(this MemoryDistributedCache memoryDistributedCache)
        {
            var cacheType = typeof(MemoryDistributedCache);
            var memCacheField = cacheType.GetField("_memCache", BindingFlags.NonPublic | BindingFlags.Instance);
            if (memCacheField != null)
            {
                var memCacheValue = memCacheField.GetValue(memoryDistributedCache) as MemoryCache;
                return memCacheValue;
            }
            return null;
        }

        private static readonly Func<MemoryCache, IDictionary> GetEntries =
            Assembly.GetAssembly(typeof(MemoryCache)).GetName().Version.Major < 7
                ? (Func<MemoryCache, IDictionary>)(cache => (IDictionary)GetEntries6.Value(cache))
                : cache => GetEntries7.Value(GetCoherentState.Value(cache));

        public static ICollection GetKeys(this IMemoryCache memoryCache) =>
            GetEntries((MemoryCache)memoryCache).Keys;

        public static IEnumerable<T> GetKeys<T>(this IMemoryCache memoryCache) =>
            memoryCache.GetKeys().OfType<T>();
        public static ICollection GetKeys(this MemoryDistributedCache memoryDistributedCache) {
            var memoryCache = memoryDistributedCache.GetMemoryCache();
            return GetEntries(memoryCache!).Keys;
        }
        public static IEnumerable<T> GetKeys<T>(this MemoryDistributedCache memoryDistributedCache)
        {
            var memoryCache = memoryDistributedCache.GetMemoryCache();
            return memoryCache!.GetKeys().OfType<T>();
        }

    }
}
