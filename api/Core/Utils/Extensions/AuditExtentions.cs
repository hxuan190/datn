﻿using Audit.Core;
using System.Buffers;
using System.Reflection;
using System.Text.Json.Serialization.Metadata;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Util.Extensions
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SensitiveAttribute : Attribute
    {
    }
    public class SensitiveAuditJsonAdapter : IJsonAdapter
    {
        public string Serialize(object value)
        {
            var options = new JsonSerializerOptions
            {
                TypeInfoResolver = new DefaultJsonTypeInfoResolver
                {
                    Modifiers = { RedactSensitivePropertyModifier }
                },
                ReferenceHandler = ReferenceHandler.Preserve,
            };
            // First serialization and deserialization.
            var serialized = JsonSerializer.Serialize(value, options);
            return serialized;
        }

        public T Deserialize<T>(string json) => JsonSerializer.Deserialize<T>(json, Configuration.JsonSettings);

        public object Deserialize(string json, Type type) => JsonSerializer.Deserialize(json, type, Configuration.JsonSettings);

        public async Task SerializeAsync(
            Stream stream,
            object value,
            CancellationToken cancellationToken = default)
        {
            await JsonSerializer.SerializeAsync(stream, value, value.GetType(), Configuration.JsonSettings, cancellationToken);
        }

        public async Task<T> DeserializeAsync<T>(Stream stream, CancellationToken cancellationToken = default) => await JsonSerializer.DeserializeAsync<T>(stream, Configuration.JsonSettings, cancellationToken);

        public T ToObject<T>(object value)
        {
            if (value == null)
                return default;
            if (value is T || typeof(T).GetTypeInfo().IsAssignableFrom(value.GetType().GetTypeInfo()))
                return (T)value;
            if (!(value is JsonElement jsonElement))
                return default;
            ArrayBufferWriter<byte> arrayBufferWriter = new ArrayBufferWriter<byte>();
            using (Utf8JsonWriter writer = new Utf8JsonWriter(arrayBufferWriter))
                jsonElement.WriteTo(writer);
            return JsonSerializer.Deserialize<T>(arrayBufferWriter.WrittenSpan, Configuration.JsonSettings);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeInfo"></param>
        void RedactSensitivePropertyModifier(JsonTypeInfo typeInfo)
        {
            foreach (JsonPropertyInfo propertyInfo in typeInfo.Properties)
            {
                //only string and marked with SensitiveAttribute
                if (!propertyInfo.PropertyType.FullName.Equals("System.String"))// != typeof(string) && propertyInfo.PropertyType != typeof(String))
                    continue;
                var sensitiveAttributes = propertyInfo.AttributeProvider?.GetCustomAttributes(typeof(SensitiveAttribute), true) ?? Array.Empty<object>();
                var attribute = sensitiveAttributes.Length == 1 ? (SensitiveAttribute)sensitiveAttributes[0] : null;
                if (attribute != null)
                {
                    var getProperty = propertyInfo.Get;
                    if (getProperty is not null)
                    {
                        //override the Get of propertyInfo
                        propertyInfo.Get = (obj) =>
                        {
                            return "***REDACTED***";
                        };
                    }

                }
            }
        }

    }
}
