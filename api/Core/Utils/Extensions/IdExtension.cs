﻿namespace Util.Extensions
{
    public static class IdExtension
    {
        public static string Generate()
        {
            return Guid.NewGuid().ToString();
        } 
    }
}
