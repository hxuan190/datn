﻿using Microsoft.Extensions.Caching.Distributed;
using Util.Extensions;

namespace Util
{
    public static class CacheKeys
    {
	    public static string CACHEKEYPREFIXUPLOADTEMP => Cachekeyprefixuploadtemp;
	    private const string Cachekeyprefixuploadtemp = "UPLOADTEMP";
	    public static string FEATURES_OF_GROUP(string orgId, string groupId) => $"FEATURES_OF_GROUP_{orgId}_{groupId}";
        public static string FEATURES_OF_USER(string orgId, string email) => $"FEATURES_OF_USER_{orgId}_{email}";
        public static string EMPLOYEE_BY_ORG_ID_AND_EMAIL(string orgId, string email) => $"EMPLOYEE_BY_ORG_ID_{orgId}_AND_EMAIL_{email}";
        public static string EMPLOYEE_BY_ORG_ID_AND_EMPLOYEE_ID(string orgId, string id) => $"EMPLOYEE_BY_ORG_ID_{orgId}_AND_EMPLOYEE_ID_{id}";
    }

    public static class CacheRemoveHelpers
    {
        /// <summary>
        /// Remove all cache data related to employee
        /// </summary>
        /// <param name="distributedCache"></param>
        /// <param name="orgId"></param>
        /// <param name="employeeId"></param>
        /// <param name="email"></param>
        public static void RemoveCacheForEmployee(this IDistributedCache distributedCache, 
            string orgId, 
            string? employeeId,
            string? email)
        {
            var tasks = new List<Task>();
            if (string.IsNullOrEmpty(orgId))
                return;
            if(!string.IsNullOrEmpty(employeeId))
                tasks.Add(distributedCache.Delete(CacheKeys.EMPLOYEE_BY_ORG_ID_AND_EMPLOYEE_ID(orgId, employeeId)));
            if (!string.IsNullOrEmpty(email))
                tasks.Add(distributedCache.Delete(CacheKeys.EMPLOYEE_BY_ORG_ID_AND_EMAIL(orgId, email)));
            if(tasks.Count>0)
                Task.WhenAll(tasks);
        }
    }
}
