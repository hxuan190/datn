﻿using System.ComponentModel;

namespace Util
{
    /// <summary>
    /// All error codes in the solution
    /// </summary>
    public enum ErrorCodes
    {
        //exception for system start with 10
        [Description("SYSTEM_OPERATION_CANCELLED")]
        SYSTEM_OPERATION_CANCELLED = 10001,

        #region application
        //exception for application start with 40
        [Description("APPLICATION_GENERAL")]
        APPLICATION_GENERAL = 40100,

        [Description("APPLICATION_FORBIDDEN")]
        APPLICATION_FORBIDDEN = 40101,

        [Description("APPLICATION_FEATURE_NOT_ENABLE")]
        APPLICATION_FEATURE_NOT_ENABLE = 40102,

        #region application not found 

        [Description("APPLICATION_NOTFOUND_ENTITY")]
        APPLICATION_NOTFOUND_ENTITY = 40201,

        [Description("APPLICATION_NOTFOUND_ENTITY_ORGANISATION")]
        APPLICATION_NOTFOUND_ENTITY_ORGANISATION = 40202,

        [Description("APPLICATION_NOTFOUND_ENTITY_EMPLOYEE")]
        APPLICATION_NOTFOUND_ENTITY_EMPLOYEE = 40203,
        [Description("APPLICATION_NOTFOUND_ENTITY_SESSION")]
        APPLICATION_NOTFOUND_ENTITY_SESSION = 40204,
        [Description("APPLICATION_NOTFOUND_ENTITY_UPLOADTEMPRESULT")]
        APPLICATION_NOTFOUND_ENTITY_UPLOADTEMPRESULT = 40205,
        [Description("APPLICATION_NOTFOUND_ENTITY_GROUP")]
        APPLICATION_NOTFOUND_ENTITY_GROUP = 40206,
        
        #endregion

        #region application not valid

        [Description("APPLICATION_INVALID")]
        APPLICATION_INVALID = 40301,

        [Description("APPLICATION_INVALID_PASSWORD_NOT_STRONG")]
        APPLICATION_INVALID_PASSWORD_NOT_STRONG = 40341,

        [Description("APPLICATION_INVALID_OLD_PASSWORD_INCORRECT")]
        APPLICATION_INVALID_OLD_PASSWORD_INCORRECT = 40342,

        [Description("APPLICATION_INVALID_TOKEN_INVALID")]
        APPLICATION_INVALID_TOKEN_INVALID = 40343,

        [Description("APPLICATION_OTP_BRUTE_FORCE")]
        APPLICATION_OTP_BRUTE_FORCE = 40344,

        [Description("APPLICATION_RESET_PASSWORD_NOT_GENERATE_TOKEN")]
        APPLICATION_RESET_PASSWORD_NOT_GENERATE_TOKEN = 40345,

        [Description("APPLICATION_SESSION_CANNOT_UPDATE_DEVICE_SECRET")]
        APPLICATION_SESSION_CANNOT_UPDATE_DEVICE_SECRET = 40351,

        [Description("APPLICATION_GROUP_IS_DEFAULT")]
        APPLICATION_GROUP_IS_DEFAULT = 40361,
		#endregion

		#region application conflict

		[Description("APPLICATION_CONFLICT")]
        APPLICATION_CONFLICT = 40401,

        [Description("APPLICATION_CONFLICT_EMPLOYEE_ALREADY_EXIST")]
        APPLICATION_CONFLICT_EMPLOYEE_ALREADY_EXIST = 40411,

		#endregion

		#region application data not saved

		[Description("APPLICATION_DATA_NOT_SAVED")]
        APPLICATION_DATA_NOT_SAVED = 40501,
        [Description("APPLICATION_GROUP_DATA_NOT_SAVED")]
        APPLICATION_GROUP_DATA_NOT_SAVED = 40502,
        [Description("APPLICATION_GROUP_EMPLOYEE_NOT_REMOVED")]
        APPLICATION_GROUP_EMPLOYEE_NOT_REMOVED = 40503,
        [Description("APPLICATION_GROUP_NOT_DELETED")]
        APPLICATION_GROUP_NOT_DELETED = 40504,

        [Description("APPLICATION_EMPLOYEE_PASSWORD_NOT_SET")]
        APPLICATION_EMPLOYEE_PASSWORD_NOT_SET = 40510,
        [Description("APPLICATION_EMPLOYEE_SESSIONS_NOT_REVOKE")]
        APPLICATION_EMPLOYEE_SESSIONS_NOT_REVOKE = 40511,
        [Description("APPLICATION_EMPLOYEE_NOT_ADDED")]
        APPLICATION_EMPLOYEE_NOT_ADDED = 40512,
        [Description("APPLICATION_EMPLOYEE_NOT_UPDATED_BASIC_DETAIL")]
        APPLICATION_EMPLOYEE_NOT_UPDATED_BASIC_DETAIL = 40513,
        [Description("APPLICATION_EMPLOYEE_NOT_DELETE")]
        APPLICATION_EMPLOYEE_NOT_DELETE = 40514,
        #endregion

        #endregion

        #region web auth

        //exception for web auth start with 51

        [Description("WEB_AUTH_INVALID_MODEL")]
        WEB_AUTH_INVALID_MODEL = 51000,

        [Description("WEB_AUTH_INVALID_RECAPTCHA")]
        WEB_AUTH_INVALID_RECAPTCHA = 51001,

        [Description("WEB_AUTH_MISSING_RETURNURL")]
        WEB_AUTH_MISSING_RETURNURL = 51002,

        [Description("WEB_AUTH_MISSING_CLIENTID")]
        WEB_AUTH_MISSING_CLIENTID = 51003,

        [Description("WEB_AUTH_MISSING_POSTLOGOUTREDIRECT")]
        WEB_AUTH_MISSING_POSTLOGOUTREDIRECT = 51004,

        [Description("WEB_AUTH_MISSING_INTERACTIONCONTEXT")]
        WEB_AUTH_MISSING_INTERACTIONCONTEXT = 51005,

        [Description("WEB_AUTH_ORGANISATION_NOT_AVAILABLE")]
        WEB_AUTH_ORGANISATION_NOT_AVAILABLE = 51006,

        [Description("WEB_AUTH_ACCOUNT_WAS_LOCKED")]
        WEB_AUTH_ACCOUNT_WAS_LOCKED = 51007,

        [Description("WEB_AUTH_ACCOUNT_WAS_DISABLED")]
        WEB_AUTH_ACCOUNT_WAS_DISABLED = 51008,

        [Description("WEB_AUTH_ACCOUNT_WAS_NOT_ALLOW_LOGIN")]
        WEB_AUTH_ACCOUNT_WAS_NOT_ALLOW_LOGIN = 51009,

        [Description("WEB_AUTH_ACCOUNT_REQUIRE_MFA")]
        WEB_AUTH_ACCOUNT_REQUIRE_MFA = 51010,

        [Description("WEB_AUTH_ACCOUNT_INVALID_CREDENTIAL")]
        WEB_AUTH_ACCOUNT_INVALID_CREDENTIAL = 51011,
        #endregion

        #region web api
        //exception for web api start with 50
        [Description("WEB_API_INVALID_MODEL")]
        WEB_API_INVALID_MODEL = 50001,

        [Description("WEB_API_INTERNAL_ERROR")]
        WEB_API_INTERNAL_ERROR = 50002,

        [Description("WEB_API_TOO_MANY_REQUESTS")]
        WEB_API_TOO_MANY_REQUESTS = 50003,

        [Description("WEB_API_KEY_MISSING")]
        WEB_API_KEY_MISSING = 50004,

        [Description("WEB_API_KEY_WRONG")]
        WEB_API_KEY_WRONG = 50005,

        [Description("WEB_API_CONFLICT")]
        WEB_API_CONFLICT = 50006,

        [Description("WEB_API_UNKNOWN")]
        WEB_API_UNKNOWN = 50007,

        [Description("WEB_API_NOTFOUND_ENTITY")]
        WEB_API_NOTFOUND_ENTITY = 50012,

        #endregion
    }
}
