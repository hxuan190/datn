﻿using Util.Extensions;

namespace Util.UnitTest.Extensions
{
    public class DateTimeExtensionsTests
    {
        [Fact]
        public void ConvertToTimestampDouble_ShouldReturnCorrectTimestamp()
        {
            // Arrange
            var dateTime = new DateTime(2022, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var expectedTimestamp = 1640995200; // Unix timestamp for 2022-01-01 00:00:00

            // Act
            var result = DateTimeExtensions.ConvertToTimestampDouble(dateTime);

            // Assert
            Assert.Equal(expectedTimestamp, result, 0);
        }
    }
}
