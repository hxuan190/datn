﻿using Microsoft.Extensions.Caching.Memory;
using Util.Extensions;

namespace Util.UnitTest.Extensions
{
    public class MemoryCacheExtensionsTests
    {
        [Fact]
        public void GetKeys_ReturnsCorrectKeys()
        {
            // Arrange
            var memoryCache = new MemoryCache(new MemoryCacheOptions());
            memoryCache.GetOrCreate(1, ce => "one");
            memoryCache.GetOrCreate("two", ce => "two");

            // Act
            var intKeys = memoryCache.GetKeys<int>();
            var stringKeys = memoryCache.GetKeys<string>();
            // Assert
            Assert.Contains(1, intKeys);
            Assert.Contains("two", stringKeys);
        }

        [Fact]
        public void GetKeysGeneric_ReturnsCorrectKeys()
        {
            // Arrange
            var memoryCache = new MemoryCache(new MemoryCacheOptions());
            memoryCache.GetOrCreate(1, ce => "one");
            memoryCache.GetOrCreate("two", ce => "two");

            // Act
            var keys = memoryCache.GetKeys<string>();

            // Assert
            Assert.Contains("two", keys);
            Assert.DoesNotContain("1", keys);
        }
    }
}
