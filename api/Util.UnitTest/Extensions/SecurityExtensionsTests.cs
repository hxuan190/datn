﻿using Util.Extensions;

namespace Util.UnitTest.Extensions
{
	public class SecurityExtensionsTest
	{
		[Fact]
		public void GenerateRandomPassword_Returns_Valid_Password()
		{
			// Arrange

			// Act
			var password = SecurityExtensions.GenerateRandomPassword();

			// Assert
			Assert.NotNull(password);
			Assert.Equal(16, password.Length);
			Assert.Contains(password, char.IsUpper);
			Assert.Contains(password, char.IsLower);
			Assert.Contains(password, char.IsDigit);
			Assert.Contains(password, c => "!@#$%^&*".Contains(c));
		}

		[Fact]
		public void GenerateOtp_Returns_String_With_Six_Digits()
		{
			// Act
			var otp = SecurityExtensions.GenerateOtp();

			// Assert
			Assert.NotNull(otp);
			Assert.Equal(6, otp.Length);
			Assert.Matches("^[0-9]{6}$", otp);
		}

		[Fact]
		public void GenerateAesRawKey_Returns_NonEmpty_String()
		{
			// Act
			string key = SecurityExtensions.GenerateAesRawKey();

			// Assert
			Assert.NotEmpty(key);
		}

		[Fact]
		public void GenerateAesRawKey_Returns_Unique_Keys()
		{
			// Arrange
			string key1, key2;

			// Act
			key1 = SecurityExtensions.GenerateAesRawKey();
			key2 = SecurityExtensions.GenerateAesRawKey();

			// Assert
			Assert.NotEqual(key1, key2);
		}

		[Fact]
		public void GenerateAesRawKey_Returns_Key_With_Correct_Length()
		{
			// Arrange
			int expectedLength = 30;

			// Act
			string key = SecurityExtensions.GenerateAesRawKey();

			// Assert
			Assert.Equal(expectedLength, key.Length);
		}
		[Fact]
		public void IsStrongPassword_Returns_True_For_Strong_Password()
		{
			// Arrange
			var password = "Abcdefg1!";

			// Act
			var result = SecurityExtensions.IsStrongPassword(password);

			// Assert
			Assert.True(result);
		}

		[Fact]
		public void IsStrongPassword_Returns_False_For_Null_Password()
		{
			// Arrange
			string password = null!;

			// Act
			var result = SecurityExtensions.IsStrongPassword(password);

			// Assert
			Assert.False(result);
		}

		[Fact]
		public void IsStrongPassword_Returns_False_For_Empty_Password()
		{
			// Arrange
			var password = "";

			// Act
			var result = SecurityExtensions.IsStrongPassword(password);

			// Assert
			Assert.False(result);
		}

		[Fact]
		public void IsStrongPassword_Returns_False_For_Short_Password()
		{
			// Arrange
			var password = "Abc1!";

			// Act
			var result = SecurityExtensions.IsStrongPassword(password);

			// Assert
			Assert.False(result);
		}

		[Fact]
		public void IsStrongPassword_Returns_False_Without_Uppercase_Letter()
		{
			// Arrange
			var password = "abcdefg1!";

			// Act
			var result = SecurityExtensions.IsStrongPassword(password);

			// Assert
			Assert.False(result);
		}

		[Fact]
		public void IsStrongPassword_Returns_False_Without_Lowercase_Letter()
		{
			// Arrange
			var password = "ABCDEFG1!";

			// Act
			var result = SecurityExtensions.IsStrongPassword(password);

			// Assert
			Assert.False(result);
		}

		[Fact]
		public void IsStrongPassword_Returns_False_Without_Digit()
		{
			// Arrange
			var password = "Abcdefg!";

			// Act
			var result = SecurityExtensions.IsStrongPassword(password);

			// Assert
			Assert.False(result);
		}

		[Fact]
		public void IsStrongPassword_Returns_False_Without_Special_Character()
		{
			// Arrange
			var password = "Abcdefg1";

			// Act
			var result = SecurityExtensions.IsStrongPassword(password);

			// Assert
			Assert.False(result);
		}
	}
}
