﻿using System.ComponentModel;
using Util.Extensions;

namespace Util.UnitTest.Extensions
{
    public class EnumExtensionsTests
    {
        private enum TestEnum
        {
            [Description("First Value")]
            FirstValue = 1,

            [Description("Second Value")]
            SecondValue = 2,

            [Description("Third Value")]
            ThirdValue = 4
        }

        [Fact]
        public void GetDescription_ShouldReturnCorrectDescription()
        {
            Assert.Equal("First Value", TestEnum.FirstValue.GetDescription());
            Assert.Equal("Second Value", TestEnum.SecondValue.GetDescription());
            Assert.Equal("Third Value", TestEnum.ThirdValue.GetDescription());
        }

        [Fact]
        public void GetDescriptions_ShouldReturnCorrectDescriptions()
        {
            var descriptions = TestEnum.FirstValue.GetDescriptions();
            Assert.Contains("First Value", descriptions);

            descriptions = TestEnum.SecondValue.GetDescriptions();
            Assert.Contains("Second Value", descriptions);
        }

        [Fact]
        public void GetDescriptionsAsEnglishList_ShouldReturnCorrectDescriptions()
        {
            var descriptions = TestEnum.FirstValue.GetDescriptionsAsEnglishList();
            Assert.Equal("First Value", descriptions);

            descriptions = (TestEnum.SecondValue).GetDescriptionsAsEnglishList();
            Assert.Equal("Second Value", descriptions);
        }

        [Fact]
        public void ParseEnum_ShouldReturnCorrectEnumValue()
        {
            Assert.Equal(TestEnum.FirstValue, "FirstValue".ParseEnum<TestEnum>());
            Assert.Equal(TestEnum.SecondValue, "SecondValue".ParseEnum<TestEnum>());
            Assert.Equal(TestEnum.ThirdValue, "ThirdValue".ParseEnum<TestEnum>());
        }

        [Fact]
        public void ParseEnum_ShouldThrowExceptionForInvalidValue()
        {
            Assert.Throws<ArgumentException>(() => "InvalidValue".ParseEnum<TestEnum>());
        }
    }
}
