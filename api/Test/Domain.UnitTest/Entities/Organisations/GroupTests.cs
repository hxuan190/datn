﻿using Domain.Entities.Organisations;

namespace Domain.UnitTest.Entities.Organisations
{
    public class GroupTests
    {
        [Fact]
        public void Group_Initialise_PropertiesAreSetCorrectly()
        {
            // Arrange
            var id = "testId";
            var name = "testName";
            var description = "testDescription";
            var isDefault = false;
            var organisationId = "testOrganisationId"; 

            // Act
            var group = new Group(id, name, description, isDefault, organisationId, AppPage.Dashboard);

            // Assert
            Assert.Equal(id, group.Id);
            Assert.Equal(name, group.Name);
            Assert.Equal(description, group.Description);
            Assert.Equal(isDefault, group.IsDefault);
            Assert.Equal(organisationId, group.OrganisationId);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenIsDefaultIsTrue()
        {
            // Arrange
            var group = new Group("1", "Test Group", "Test Description", true, "1", AppPage.Dashboard);

            // Act
            var result = group.Update("New Name", "New Description");

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenNameIsNullOrWhiteSpace()
        {
            // Arrange
            var group = new Group("1", "Test Group", "Test Description", false, "1", AppPage.Dashboard);

            // Act
            var result = group.Update("", "New Description");

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void Update_ShouldUpdateNameAndDescription_WhenValidParametersArePassed()
        {
            // Arrange
            var group = new Group("1", "Test Group", "Test Description", false, "1", AppPage.Dashboard);

            // Act
            var result = group.Update("New Name", "New Description");

            // Assert
            Assert.True(result);
            Assert.Equal("New Name", group.Name);
            Assert.Equal("New Description", group.Description);
        }
    }
}
