using Application.Commands;
using Application.Commands.Employees;
using Application.Queries;
using Application.Queries.Employees;
using Application.Queries.GridViews;
using AutoMapper;
using Domain.Entities.Organisations;
using Kensa.API.Features.Misc.DTOs;
using Kensa.API.Features.Security.Employees;
using Kensa.API.Features.Security.Employees.DTOs;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Util;

namespace Kensa.API.UnitTest.Features.Security.Employees
{
	public class EmployeeControllerTests
	{
		private IQueryExecutor _queryExecutor;
		private ICommandDispatcher _commandDispatcher;
		private readonly IMapper _mapper;

		public EmployeeControllerTests()
		{
			var employeeMapperProfile = new EmployeeMappingProfile();
			var miscMapperProfile = new MiscMappingProfile();

			var configuration = new MapperConfiguration(cfg =>
			{
				cfg.AddProfile(employeeMapperProfile);
				cfg.AddProfile(miscMapperProfile);
			});
			_mapper = new Mapper(configuration);
		}

		[Fact]
		public async Task GetActiveSessionsByEmployeeId_Return_Array_Of_EmployeeActiveSessionDto()
		{
			//arrange
			var fakeGetEmployeeSessionsByEmployeeIdResult = new List<GetEmployeeSessionsByEmployeeIdResult>()
			{
				new("fakeSessionId", new DateTime(2024,1,1), new DateTime(2024,1,2),null,"fakeUserAgent","fakeDevice","::1","australia","::1","australia" )
			};
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			mockQueryExecutor
				.Setup(x => x.QueryAsync(
					It.IsAny<GetEmployeeSessionsByEmployeeId>()
					, It.IsAny<CancellationToken>()
					))
				.ReturnsAsync(fakeGetEmployeeSessionsByEmployeeIdResult.ToArray());
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.GetActiveSessionsByEmployeeId("testId");
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<EmployeeActiveSessionDto[]>(data);
			var employeeActiveSessionDtos = (EmployeeActiveSessionDto[])data;
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult.Count, employeeActiveSessionDtos.Length);
			var employeeActiveSessionDto = employeeActiveSessionDtos[0];
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].SessionId, employeeActiveSessionDto.SessionId);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].Created, employeeActiveSessionDto.Created);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].Renewed, employeeActiveSessionDto.Renewed);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].Expires, employeeActiveSessionDto.Expires);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].UserAgent, employeeActiveSessionDto.UserAgent);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].Device, employeeActiveSessionDto.Device);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].InitialIpAddress, employeeActiveSessionDto.InitialIpAddress);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].InitialPlace, employeeActiveSessionDto.InitialPlace);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].LastUsedIpAddress, employeeActiveSessionDto.LastUsedIpAddress);
			Assert.Equal(fakeGetEmployeeSessionsByEmployeeIdResult[0].LastUsedPlace, employeeActiveSessionDto.LastUsedPlace);
		}

		[Fact]
		public async Task RevokeSessionsOfEmployee_Return_Ok_If_Command_Success()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
									It.IsAny<RevokeSessionsOfEmployee>()
														, It.IsAny<CancellationToken>()
														))
				.ReturnsAsync(new CommandHandlerResult());
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.RevokeSessionsOfEmployee("testId", new RevokeSessionsRequestDto(["fakeSessionId"]));
			//assert
			Assert.IsType<OkResult>(result);
		}
		[Fact]
		public async Task RevokeSessionsOfEmployee_Return_BadRequest_If_Command_Failed()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<RevokeSessionsOfEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult("fakeError"));
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.RevokeSessionsOfEmployee("testId", new RevokeSessionsRequestDto(["fakeSessionId"]));
			//assert
			Assert.IsType<BadRequestObjectResult>(result);
			Assert.Equal(ErrorCodes.APPLICATION_EMPLOYEE_SESSIONS_NOT_REVOKE, ((BadRequestObjectResult)result).Value);
		}

		[Fact]
		public async Task ForceSetPasswordForEmployee_Return_Ok_If_Command_Success()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<SetPasswordForEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult());
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.ForceSetPasswordForEmployee("testEmployeeId", new ForceSetPasswordRequestDto("fakePassword"));
			//assert
			Assert.IsType<OkResult>(result);
		}
		[Fact]
		public async Task ForceSetPasswordForEmployee_Return_BadRequest_If_Command_Failed()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<SetPasswordForEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult("fakeError"));
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.ForceSetPasswordForEmployee("testEmployeeId", new ForceSetPasswordRequestDto("fakePassword"));
			//assert
			Assert.IsType<BadRequestObjectResult>(result);
			Assert.Equal(ErrorCodes.APPLICATION_EMPLOYEE_PASSWORD_NOT_SET, ((BadRequestObjectResult)result).Value);
		}
		[Fact]
		public async Task GetBasicDetailOfEmployee_Return_GetEmployeeBasicDetailResultDto()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;
			var fakeEmployee = new Employee("fakeId", "fakeOrganisationId",
				"fakeEmail",
				"fakeFirstName",
				"fakeLastName");
			mockQueryExecutor
				.Setup(x => x.QueryAsync(
					It.IsAny<GetEmployeeById>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(fakeEmployee);
			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.GetBasicDetailOfEmployee("fakeId");
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<GetEmployeeBasicDetailResultDto>(data);
			var basicResult = (GetEmployeeBasicDetailResultDto)data;
			Assert.Equal("fakeEmail", basicResult.Email);
			Assert.Equal("fakeFirstName", basicResult.FirstName);
			Assert.Equal("fakeLastName", basicResult.LastName);
			Assert.Null(basicResult.Avatar);
		}

		[Fact]
		public async Task AddNewEmployee_Return_Ok_If_Command_Success()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<AddNewEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new AddNewEmployeeCommandResult("fakeId"));
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.AddNewEmployee(
				new AddNewEmployeeRequestDto("fakeEmail",
					"fakeFirstName",
					"fakeLastName",
					null,
					[new AddNewEmployeeMetadataRequestDto("fakeMetadataId", "fakeValue")]));
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<string>(data);
			var newEmployeeId = (string)data;
			Assert.Equal("fakeId", newEmployeeId);

		}
		[Fact]
		public async Task AddNewEmployee_Return_BadRequest_If_Command_Failed()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<AddNewEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult("fakeError"));
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.AddNewEmployee(
				new AddNewEmployeeRequestDto("fakeEmail",
					"fakeFirstName",
					"fakeLastName",
					null,
					[new AddNewEmployeeMetadataRequestDto("fakeMetadataId", "fakeValue")]));
			//assert
			Assert.IsType<BadRequestObjectResult>(result);
			Assert.Equal(ErrorCodes.APPLICATION_EMPLOYEE_NOT_ADDED, ((BadRequestObjectResult)result).Value);
		}

		[Fact]
		public async Task UpdateBasicDetail_Return_Ok_If_Command_Success()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<UpdateBasicDetailEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult());
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.UpdateBasicDetail("testEmployeeId", new UpdateEmployeeBasicDetailRequestDto("fakeFirstName", "fakeLastName"));
			//assert
			Assert.IsType<OkResult>(result);
		}
		[Fact]
		public async Task UpdateBasicDetail_Return_BadRequest_If_Command_Failed()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<UpdateBasicDetailEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult("fakeError"));
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.UpdateBasicDetail("testEmployeeId", new UpdateEmployeeBasicDetailRequestDto("fakeFirstName", "fakeLastName"));
			//assert
			Assert.IsType<BadRequestObjectResult>(result);
			Assert.Equal(ErrorCodes.APPLICATION_EMPLOYEE_NOT_UPDATED_BASIC_DETAIL, ((BadRequestObjectResult)result).Value);
		}


		[Fact]
		public async Task Delete_Return_Ok_If_Command_Success()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<SoftDeleteEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult());
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.Delete("testEmployeeId");
			//assert
			Assert.IsType<OkResult>(result);
		}
		[Fact]
		public async Task Delete_Return_BadRequest_If_Command_Failed()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher.Setup(x => x.DispatchAsync(
					It.IsAny<SoftDeleteEmployee>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult("fakeError"));
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.Delete("testEmployeeId");
			//assert
			Assert.IsType<BadRequestObjectResult>(result);
			Assert.Equal(ErrorCodes.APPLICATION_EMPLOYEE_NOT_DELETE, ((BadRequestObjectResult)result).Value);
		}
		[Fact]
		public async Task SearchEmployeeByKeyword_Returns_SearchEmployeeResultDto()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;
			var fakeResult = new SearchEmployeeResult(10, new SimpleEmployeeResponse[]
			{
				new("fakeId","fakeFirstName","fakeLastName","fakeAvatar","fakeEmail")
			});
			mockQueryExecutor.Setup(x => x.QueryAsync(
					It.IsAny<SearchEmployees>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(fakeResult);
			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.SearchEmployeeByKeyword(
				new SearchEmployeeRequestDto("testKeyword", 10, 20));
			//assert
			Assert.IsType<OkObjectResult>(result);
			Assert.IsType<SearchEmployeeResultDto>(((OkObjectResult)result).Value);
			var searchEmployeeResultDto = (SearchEmployeeResultDto)((OkObjectResult)result).Value!;
			Assert.Equal(10, searchEmployeeResultDto.Total);
			Assert.Equal(1, searchEmployeeResultDto.Employees.Length);
			Assert.Equal("fakeId", searchEmployeeResultDto.Employees[0].Id);
			Assert.Equal("fakeFirstName", searchEmployeeResultDto.Employees[0].FirstName);
			Assert.Equal("fakeLastName", searchEmployeeResultDto.Employees[0].LastName);
			Assert.Equal("fakeAvatar", searchEmployeeResultDto.Employees[0].Avatar);
			Assert.Equal("fakeEmail", searchEmployeeResultDto.Employees[0].Email);
		}

		[Fact]
		public async Task GetAvailableColumnsForEmployeeGrid_Return_Array_Of_GridViewColumnDto()
		{
			//arrange
			var fakeAvailableViewColumns = new List<AvailableViewColumn>()
			{
				new ("fakeSessionId","fakeColumnName", true, true, true )
			};
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			mockQueryExecutor
				.Setup(x => x.QueryAsync(
					It.IsAny<GetAvailableColumns>()
					, It.IsAny<CancellationToken>()
					))
				.ReturnsAsync(fakeAvailableViewColumns.ToArray());
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.GetAvailableColumnsForEmployeeGrid();
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<GridViewColumnDto[]>(data);
			var gridviewColumnDtos = (GridViewColumnDto[])data;
			Assert.Equal(fakeAvailableViewColumns.Count, gridviewColumnDtos.Length);

			var gridviewColumnDto = gridviewColumnDtos[0];
			Assert.Equal(fakeAvailableViewColumns[0].Id, gridviewColumnDto.Id);
			Assert.Equal(fakeAvailableViewColumns[0].Name, gridviewColumnDto.Name);
			Assert.Equal(fakeAvailableViewColumns[0].IsDefaultColumn, gridviewColumnDto.IsDefaultColumn);
			Assert.Equal(fakeAvailableViewColumns[0].AllowSort, gridviewColumnDto.AllowSort);
			Assert.Equal(fakeAvailableViewColumns[0].AllowFilter, gridviewColumnDto.AllowFilter);
		}
		[Fact]
		public async Task GetOptionsOfColumnToFilter_Return_GridViewColumnOptionDto()
		{
			//arrange
			var fakeColumnOptions = new List<ColumnOptionResult>()
			{
				new ColumnOptionResult("fakeValue","fakeDisplay")
			};
			var fakeGetOptionsOfColumnResult = new GetOptionsOfColumnResult(fakeColumnOptions.Count, fakeColumnOptions.ToArray());
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			mockQueryExecutor
				.Setup(x => x.QueryAsync(
					It.IsAny<GetOptionsOfColumn>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(fakeGetOptionsOfColumnResult);
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.GetOptionsOfColumnToFilter("fakeColumnId", new GridViewGetOptionsOfColumnDto());
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<GridViewColumnOptionDto>(data);
			var gridViewColumnOptionDto = (GridViewColumnOptionDto)data;
			Assert.Equal(1, gridViewColumnOptionDto.Total);

			var gridviewColumnOptionDto = gridViewColumnOptionDto.Data[0];
			Assert.Equal("fakeValue", gridviewColumnOptionDto.Value);
			Assert.Equal("fakeDisplay", gridviewColumnOptionDto.Display);
		}
		[Fact]
		public async Task GetGridViewData_Return_DynamicData()
		{
			//arrange
			var fakeData = new List<Dictionary<string, object>>()
			{
				new Dictionary<string, object>()
				{
					{ "id", "fakeId" },
					{ "firstName", "fakeFirstName" }
				}
			};
			var fakeGetOptionsOfColumnResult = new GetGridViewDataResult(fakeData.Count, fakeData.ToArray());

			var mockQueryExecutor = new Mock<IQueryExecutor>();
			mockQueryExecutor
				.Setup(x => x.QueryAsync(
					It.IsAny<GetGridViewData>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(fakeGetOptionsOfColumnResult);
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.GetGridViewData(new GetGridViewDataRequestDto(20, 0, [
				new GridViewDataColumnRequestDto("fakeColumnId", ["fakeSelectedOption"], true, "fakeSortDirection")
			]));
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<GetGridViewDataResult>(data);
			var getGridViewData = (GetGridViewDataResult)data;
			Assert.Equal(1, getGridViewData.Total);

			var gridViewDataDict = getGridViewData.Data[0];
			Assert.Equal("fakeId", gridViewDataDict["id"]);
			Assert.Equal("fakeFirstName", gridViewDataDict["firstName"]);

		}

		[Fact]
		public async Task GetAllViews_Return_GridViews()
		{
			//arrange

			var fakeGridViewsResult = new List<GetGridViewResult>
			{
				new GetGridViewResult("fakeGridViewId1","fakeGridView1","fakeGridViewStructure1", ResourceType.Employee,true),
				new GetGridViewResult("fakeGridViewId2","fakeGridView2","fakeGridViewStructure2", ResourceType.Employee, false),
			};
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			mockQueryExecutor
				.Setup(x => x.QueryAsync(
					It.IsAny<GetGridViews>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(fakeGridViewsResult.ToArray());
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new EmployeeController(_queryExecutor,
				_commandDispatcher,
				_mapper);
			//act
			var result = await controller.GetAllViews();
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<GetGridViewResultDto[]>(data);
			var gridviewsResult = (GetGridViewResultDto[])data;
			Assert.Equal(2, gridviewsResult.Length);

			var firstGridView = gridviewsResult.FirstOrDefault(x => x.Id == "fakeGridViewId1");
			Assert.NotNull(firstGridView);
			Assert.Equal("fakeGridView1", firstGridView.Name);
			Assert.Equal("fakeGridViewStructure1", firstGridView.GridStructure);
			Assert.True(firstGridView.Own);

			var secondGridView = gridviewsResult.FirstOrDefault(x => x.Id == "fakeGridViewId2");
			Assert.NotNull(secondGridView);
			Assert.Equal("fakeGridView2", secondGridView.Name);
			Assert.Equal("fakeGridViewStructure2", secondGridView.GridStructure);
			Assert.False(secondGridView.Own);
		}
	}
}