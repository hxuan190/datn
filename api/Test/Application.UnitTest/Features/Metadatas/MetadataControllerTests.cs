using Application.Commands;
using Application.Commands.Metadata;
using Application.Queries;
using Application.Queries.Metadata;
using AutoMapper;
using Domain.Entities.Organisations;
using Kensa.API.Features.Metadata;
using Kensa.API.Features.Metadata.DTOs;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace Kensa.API.UnitTest.Features.Metadatas
{
    public class MetadataControllerTests
    {
	    private IQueryExecutor _queryExecutor;
	    private ICommandDispatcher _commandDispatcher;
	    private readonly IMapper _mapper;

	    public MetadataControllerTests()
	    {
		    var metadataMapperProfile = new MetadataMappingProfile();
		    
			var configuration = new MapperConfiguration(cfg =>
			{
				cfg.AddProfile(metadataMapperProfile);
			});
		    _mapper = new Mapper(configuration);
		}

		[Fact]
		public async Task GetAvailableMetadata_Return_Array_Of_AvailableMetadataCategoryOfResourceDto()
		{
			//arrange
			var fakeAvailableMetadata = new List<AvailableMetadataCategoryOfResource>()
			{
				new("fakeCategoryId","fakeCategoryName",1,
				[
					new AvailableMetadataOfResource("fakeMetadataId1","fakeMetadataName1","fakeMetadataDesc1",
					[
						new AvailableMetadataPredefinedValueOfResource("fakeId1","fakeValue1")
					], 1,MetadataDataType.Text,true, false, true,true)
				])
			};
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			mockQueryExecutor
				.Setup(x => x.QueryAsync(
					It.IsAny<GetMetadataOfResource>()
					, It.IsAny<CancellationToken>()
					))
				.ReturnsAsync(fakeAvailableMetadata.ToArray());
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new MetadataController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.GetAvailableMetadata(new GetAvailableMetadataRequestDto(ResourceType.Employee));
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<AvailableMetadataCategoryOfResourceDto[]>(data);
			var availableMetadataCategoryOfResourceDtos = (AvailableMetadataCategoryOfResourceDto[])data;
			Assert.Equal(fakeAvailableMetadata.Count, availableMetadataCategoryOfResourceDtos.Length);
			Assert.Equal(fakeAvailableMetadata[0].Id, availableMetadataCategoryOfResourceDtos[0].Id);
			Assert.Equal(fakeAvailableMetadata[0].Name, availableMetadataCategoryOfResourceDtos[0].Name);
			Assert.Equal(fakeAvailableMetadata[0].DisplayOrder, availableMetadataCategoryOfResourceDtos[0].DisplayOrder);
			Assert.Equal(fakeAvailableMetadata[0].Metadata.Length, availableMetadataCategoryOfResourceDtos[0].Metadata.Length);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].Id, availableMetadataCategoryOfResourceDtos[0].Metadata[0].Id);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].Name, availableMetadataCategoryOfResourceDtos[0].Metadata[0].Name);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].Description, availableMetadataCategoryOfResourceDtos[0].Metadata[0].Description);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].DisplayOrder, availableMetadataCategoryOfResourceDtos[0].Metadata[0].DisplayOrder);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].DataType, availableMetadataCategoryOfResourceDtos[0].Metadata[0].DataType);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].Required, availableMetadataCategoryOfResourceDtos[0].Metadata[0].Required);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].AllowFilter, availableMetadataCategoryOfResourceDtos[0].Metadata[0].AllowFilter);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].AllowFreeText, availableMetadataCategoryOfResourceDtos[0].Metadata[0].AllowFreeText);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].AllowSort, availableMetadataCategoryOfResourceDtos[0].Metadata[0].AllowSort);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].PredefinedValues.Length, availableMetadataCategoryOfResourceDtos[0].Metadata[0].PredefinedValues.Length);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].PredefinedValues[0].Id, availableMetadataCategoryOfResourceDtos[0].Metadata[0].PredefinedValues[0].Id);
			Assert.Equal(fakeAvailableMetadata[0].Metadata[0].PredefinedValues[0].Value, availableMetadataCategoryOfResourceDtos[0].Metadata[0].PredefinedValues[0].Value);
		}

		[Fact]
		public async Task GetMetadataWithValueForResource_Return_Array_Of_GetMetadataWithValueOfResourceResultDto()
		{
			//arrange
			var fakeGetMetadataWithValueOfResourceResult = new List<GetMetadataWithValueOfResourceResult>()
			{
				new GetMetadataWithValueOfResourceResult("fakeMetadataId1","fakeMetadataValue1"),
				new GetMetadataWithValueOfResourceResult("fakeMetadataId2",ResourceTypeColumnOptions.CLASSIFIED)
			};
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			mockQueryExecutor
				.Setup(x => x.QueryAsync(
					It.IsAny<GetMetadataWithValueOfResource>()
					, It.IsAny<CancellationToken>()
					))
				.ReturnsAsync(fakeGetMetadataWithValueOfResourceResult.ToArray());
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new MetadataController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.GetMetadataWithValueForResource(ResourceType.Employee,"fakeId");
			//assert
			Assert.IsType<OkObjectResult>(result);
			var data = ((OkObjectResult)result).Value;
			Assert.IsType<GetMetadataWithValueOfResourceResultDto[]>(data);
			var getMetadataWithValueOfResourceResultDto = (GetMetadataWithValueOfResourceResultDto[])data;
			Assert.Equal(fakeGetMetadataWithValueOfResourceResult.Count, getMetadataWithValueOfResourceResultDto.Length);
			Assert.Equal(fakeGetMetadataWithValueOfResourceResult[0].MetadataId, getMetadataWithValueOfResourceResultDto[0].MetadataId);
			Assert.Equal(fakeGetMetadataWithValueOfResourceResult[0].Value, getMetadataWithValueOfResourceResultDto[0].Value);
			Assert.Equal(fakeGetMetadataWithValueOfResourceResult[1].MetadataId, getMetadataWithValueOfResourceResultDto[1].MetadataId);
			Assert.Equal(fakeGetMetadataWithValueOfResourceResult[1].Value, getMetadataWithValueOfResourceResultDto[1].Value);
			}

		[Fact]
		public async Task SaveMetadataForResource_Return_Ok_If_Command_Success()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher
				.Setup(x => x.DispatchAsync(
					It.IsAny<SaveMetadataWithValueForResource>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult());
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new MetadataController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.SaveMetadataForResource(
				ResourceType.Employee,
				"fakeEmployeeId",
				new SaveMetadataForResourceRequestDto([
				new SaveMetadataWithValueRequestDto(SaveMetadataAction.Add,"fakeMetadataId","a")])
				);
			//assert
			Assert.IsType<OkResult>(result);
	    }
		[Fact]
		public async Task SaveMetadataForResource_Return_BadRequest_If_Command_Failed()
		{
			//arrange
			var mockQueryExecutor = new Mock<IQueryExecutor>();
			_queryExecutor = mockQueryExecutor.Object;

			var mockCommandDispatcher = new Mock<ICommandDispatcher>();
			mockCommandDispatcher
				.Setup(x => x.DispatchAsync(
					It.IsAny<SaveMetadataWithValueForResource>()
					, It.IsAny<CancellationToken>()
				))
				.ReturnsAsync(new CommandHandlerResult("Error_while_saving"));
			_commandDispatcher = mockCommandDispatcher.Object;

			var controller = new MetadataController(_queryExecutor, _commandDispatcher, _mapper);
			//act
			var result = await controller.SaveMetadataForResource(
				ResourceType.Employee,
				"fakeEmployeeId",
				new SaveMetadataForResourceRequestDto([])
			);
			//assert
			Assert.IsType<BadRequestResult>(result);
		}

	}
}