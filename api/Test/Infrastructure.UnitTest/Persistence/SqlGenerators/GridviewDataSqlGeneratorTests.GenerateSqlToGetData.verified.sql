--1. Declare variables
DECLARE @organisationId NVARCHAR(50); 
SET @organisationId = 'fakeOrganisationId';
DECLARE @searchKeyword NVARCHAR(MAX); 
SET @searchKeyword = 'fakeKeyword';
DECLARE @take integer; 
SET @take = 20;
DECLARE @skip integer; 
SET @skip = 0;
DECLARE @resourceType integer; 
SET @resourceType = 0;

--2. ResourceValues CTE
WITH ResourceValues as (
	SELECT
		msv.ResourceId,
		msv.MetadataId,
		msv.Value as ValueId,
		CASE 
			WHEN m.AllowFreeText = 1 THEN msv.Value
			ELSE mpv.Value
		END AS Value
	FROM
		MetadataResourceValues msv
	LEFT JOIN
		Metadatas m ON msv.MetadataId = m.Id
	LEFT JOIN
		MetadataPredefinedValues mpv ON msv.Value = mpv.Id AND m.AllowFreeText = 0
    Where 
		msv.OrganisationId =  @organisationId
		AND msv.ResourceType = @resourceType
		--metadata columns selected		
		AND msv.MetadataId IN (
			'metadata1','metadata2','metadata3'
		)
),

--3. Pivot table ResourceValues
PivotResourceValues AS (
	SELECT
        ResourceId
		,MAX(CASE WHEN mv.MetadataId = 'metadata1' THEN mv.Value END) as t1, MAX(CASE WHEN mv.MetadataId = 'metadata1' THEN mv.ValueId END) as t1Id
		,MAX(CASE WHEN mv.MetadataId = 'metadata2' THEN mv.Value END) as t2, MAX(CASE WHEN mv.MetadataId = 'metadata2' THEN mv.ValueId END) as t2Id
		,MAX(CASE WHEN mv.MetadataId = 'metadata3' THEN mv.Value END) as t3, MAX(CASE WHEN mv.MetadataId = 'metadata3' THEN mv.ValueId END) as t3Id

	FROM
        ResourceValues mv
    GROUP BY
        ResourceId
),

--4. filter the pivot resource values
FilteredPivotResourceValues as (
	Select * 
	--search metadata
	,(CASE WHEN (
	
				t1 LIKE '%'+@searchKeyword+'%' OR
				t2 LIKE '%'+@searchKeyword+'%' OR
				t3 LIKE '%'+@searchKeyword+'%'
			) 
			Then 1 
			ELSE 0 
		END) as MatchSearchMetadata

	from PivotResourceValues
	
    --filter metadata
	WHERE
		
		(t1Id = 'm1_1' OR t1Id = 'm1_2') AND
		(t2Id = 'm2_1')

),

-- 5. Filtered main table
FilteredMainTable as (
	Select Id
	--Default columns
	
	, Email
	, FirstName
	, LastName
	
	--search default columns
	, (CASE WHEN(
			
				Email LIKE '%'+@searchKeyword+'%'  OR
				FirstName LIKE '%'+@searchKeyword+'%'  OR
				LastName LIKE '%'+@searchKeyword+'%' 
		)
		THEN 1
		ELSE 0
	END ) AS MatchSearchMain

	FROM fakeTable
	WHERE OrganisationId = @organisationId
    --filter default column 
	AND (FirstName = 'alpha' OR FirstName = 'beta')AND (LastName = 'gamma')
)

-- 6. Final select statement
SELECT 
	Id
	
	, Email
	, FirstName
	, LastName
	, t1 , t2 , t3
FROM 
	FilteredMainTable fe 
LEFT JOIN 
	FilteredPivotResourceValues fprv on fe.Id = fprv.ResourceId
Where ResourceId is not NULL
	And MatchSearchMain + MatchSearchMetadata > 0
--order
ORDER BY fe.FirstName desc 
--paging
OFFSET @skip ROWS 
FETCH NEXT @take ROWS ONLY;
