﻿using Domain.Entities.Organisations;
using Infrastructure.Persistence.SqlGenerators;

namespace Infrastructure.UnitTest.Persistence.SqlGenerators
{
	[UsesVerify]
	public class GridviewDataSqlGeneratorTests
	{
		[Fact]
		public async Task GenerateSqlToCount()
		{
			var skip = 0;
			var take = 20;
			var fakeOrganisationId = "fakeOrganisationId";
			var fakeKeyword = "fakeKeyword";
			var columnData = new[]
			{
				new GridViewColumnQueryData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL,
					 "",
					new string[] { }),
				new GridViewColumnQueryData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_FIRST_NAME,
					"",
					new string[] { "alpha","beta"},
					true,"desc"),
				new GridViewColumnQueryData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_LAST_NAME,
					"",
					new string[] { "gamma"}),
				new GridViewColumnQueryData("metadata1",
					"t1",
					new string[] { "m1_1","m1_2"}),
				new GridViewColumnQueryData("metadata2",
					 "t2",
					new string[] { "m2_1"}),
				new GridViewColumnQueryData("metadata3",
					"t3",
					new string[] { }),
			};

			var sqlQueries = GridviewDataSqlGenerator.Generate(
				fakeOrganisationId,
				"fakeTable",
				skip,
				take,
				columnData,
				fakeKeyword
			);
			await Verify(sqlQueries.Item2, "sql");
		}
		[Fact]
		public async Task GenerateSqlToGetData()
		{
			var skip = 0;
			var take = 20;
			var fakeOrganisationId = "fakeOrganisationId";
			var fakeKeyword = "fakeKeyword";
			var columnData = new[]
			{
				new GridViewColumnQueryData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL,
					"",
					new string[] { }),
				new GridViewColumnQueryData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_FIRST_NAME,
					"",
					new string[] { "alpha","beta"},
					true,"desc"),
				new GridViewColumnQueryData(ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_LAST_NAME,
					"",
					new string[] { "gamma"}),
				new GridViewColumnQueryData("metadata1",
					"t1",
					new string[] { "m1_1","m1_2"}),
				new GridViewColumnQueryData("metadata2",
					"t2",
					new string[] { "m2_1"}),
				new GridViewColumnQueryData("metadata3",
					"t3",
					new string[] { }),
			};

			var sqlQueries = GridviewDataSqlGenerator.Generate(
				fakeOrganisationId,
				"fakeTable",
				skip,
				take,
				columnData,
				fakeKeyword
			);
			await Verify(sqlQueries.Item1, "sql");
		}
	}
}
