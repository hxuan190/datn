using Domain.Entities.Notifications;
using Infrastructure.Services.Emails;
using Infrastructure.Services.Emails.EmailTemplates;
using Microsoft.Extensions.Configuration;
using RazorLight;
using Util.Extensions;

namespace Infrastructure.UnitTest.Services.Emails
{
	[UsesVerify]
	public class EmailContentGeneratorTests
	{
		private readonly EmailContentGenerator _emailContentGenerator;

		public EmailContentGeneratorTests()
		{
			var inMemorySettings = new Dictionary<string, string> {
				{"Email:UnsubscribeUrl", "https://fakeUnsubscribeUrl"},
				{"Email:AppName", "FAKEAPP"},
				{"Email:AppUrl", "FAKEURL"},
				{"Email:AppLogoUrl", "FAKELOGOURL"},
				{"Email:SupportEmail", "fakeSupportEmail"},
			};
			var configuration = new ConfigurationBuilder()
				.AddInMemoryCollection(inMemorySettings!)
				.Build();
			var engine = new RazorLightEngineBuilder()
				.UseEmbeddedResourcesProject(typeof(EmailTemplates))
				.UseMemoryCachingProvider()
				.Build();
			_emailContentGenerator = new EmailContentGenerator(engine, configuration);
		}

		[Fact]
		public async Task RequestOtpForPasswordReset()
		{
			var newNotificationMessage =
				new EmployeeRequestOtpForPasswordResetNotificationMessage("Kensa", "John", "123456");
			var emailContent = await _emailContentGenerator.GenerateEmailContent(newNotificationMessage);
			await Verify(emailContent.Body, "html");
		}
		[Fact]
		public async Task RequestOtpForPasswordReset_Returns_Correct_Subject()
		{
			var newNotificationMessage =
				new EmployeeRequestOtpForPasswordResetNotificationMessage("Kensa", "John", "123456");
			var emailContent = await _emailContentGenerator.GenerateEmailContent(newNotificationMessage);
			Assert.Equal(EmailTemplates.ResetPassword.GetDescription(), emailContent.Subject);
		}

		[Fact]
		public async Task LockedOut()
		{
			var newNotificationMessage =
				new LockedOutNotificationMessage("Kensa", "John");
			var emailContent = await _emailContentGenerator.GenerateEmailContent(newNotificationMessage);
			await Verify(emailContent.Body, "html");
		}
		[Fact]
		public async Task LockedOut_Returns_Correct_Subject()
		{
			var newNotificationMessage =
				new LockedOutNotificationMessage("Kensa", "John");
			var emailContent = await _emailContentGenerator.GenerateEmailContent(newNotificationMessage);
			Assert.Equal(EmailTemplates.LockedOutAccount.GetDescription(), emailContent.Subject);
		}
		[Fact]
		public async Task PasswordIsForceSet()
		{
			var newNotificationMessage =
				new EmployeePasswordIsForceSetMessage("Kensa",
					"John",
					"123456");
			var emailContent = await _emailContentGenerator.GenerateEmailContent(newNotificationMessage);
			await Verify(emailContent.Body, "html");
		}
		[Fact]
		public async Task PasswordIsForceSet_Returns_Correct_Subject()
		{
			var newNotificationMessage =
				new EmployeePasswordIsForceSetMessage("Kensa",
					"John",
					"123456");
			var emailContent = await _emailContentGenerator.GenerateEmailContent(newNotificationMessage);
			Assert.Equal(EmailTemplates.PasswordIsForceSet.GetDescription(), emailContent.Subject);
		}
		[Fact]
		public async Task EmployeeCreated()
		{
			var newNotificationMessage =
				new EmployeeCreatedMessage("Kensa",
					"John",
					"123456");
			var emailContent = await _emailContentGenerator.GenerateEmailContent(newNotificationMessage);
			await Verify(emailContent.Body, "html");
		}
		[Fact]
		public async Task EmployeeCreated_Returns_Correct_Subject()
		{
			var newNotificationMessage =
				new EmployeeCreatedMessage("Kensa",
					"John",
					"123456");
			var emailContent = await _emailContentGenerator.GenerateEmailContent(newNotificationMessage);
			Assert.Equal(EmailTemplates.EmployeeCreated.GetDescription(), emailContent.Subject);
		}
	}
}