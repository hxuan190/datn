﻿using Domain;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Util.Extensions;

namespace Application.UnitTest.TestHelpers
{
    internal class InMemoryKensaDbContext : KensaDbContext
    {
        public InMemoryKensaDbContext(IUserProvider userProvider) :
            base(new DbContextOptionsBuilder<KensaDbContext>().UseInMemoryDatabase(IdExtension.Generate()).Options,
            userProvider)
        {
        }
    }
}
