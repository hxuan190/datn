﻿using Application.Commands.Employees;
using Application.Services;
using Application.UnitTest.TestHelpers.Serilog;
using Domain;
using Domain.Entities.FeatureFlags;
using Domain.Entities.Organisations;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Serilog;

namespace Application.UnitTest.Commands.Employees
{
    public class SetPasswordForEmployeeTests
	{
        private readonly Mock<IKensaDbContext> _mockDbContext;
        private readonly Mock<IMediator> _mockMediator;

        public SetPasswordForEmployeeTests()
        {
	        _mockDbContext = new Mock<IKensaDbContext>();
            _mockMediator = new Mock<IMediator>();
        }
        [Fact]
        public void Command_Invalid_If_EmployeeId_Empty_Or_Password_Not_Strong()
        {
            var command = new SetPasswordForEmployee();
            Assert.False(command.IsValid());
            command = new SetPasswordForEmployee("","Abc123###");
            Assert.False(command.IsValid()); 
            command = new SetPasswordForEmployee(null!, "Abc123###");
            Assert.False(command.IsValid());
            command = new SetPasswordForEmployee("fakeEmployeeId", null!);
            Assert.False(command.IsValid());
            command = new SetPasswordForEmployee("fakeEmployeeId", "");
            Assert.False(command.IsValid());
            //not long enough
            command = new SetPasswordForEmployee("fakeEmployeeId", "Abc12!!");
            Assert.False(command.IsValid());
            //missing upper case
            command = new SetPasswordForEmployee("fakeEmployeeId", "abc123###");
            Assert.False(command.IsValid());
            //missing lower case
            command = new SetPasswordForEmployee("fakeEmployeeId", "ABC123###");
            Assert.False(command.IsValid());
			//missing number
			command = new SetPasswordForEmployee("fakeEmployeeId", "abcABC###");
            Assert.False(command.IsValid());
	        //missing special characters
            command = new SetPasswordForEmployee("fakeEmployeeId", "abcABC123");
            Assert.False(command.IsValid());
            command = new SetPasswordForEmployee("fakeEmployeeId", "abcABC123#");
            Assert.True(command.IsValid());
		}
        [Fact]
        public async Task AuthoriseAsync_Returns_False_If_Authorized_False()
        {
            var command = new SetPasswordForEmployee("fakeEmployeeId1", "abcABC123#");
            var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			var userManager = new Mock<UserManager<Employee>>(
				new Mock<IUserStore<Employee>>().Object,
				new Mock<IOptions<IdentityOptions>>().Object,
				new Mock<IPasswordHasher<Employee>>().Object,
				new IUserValidator<Employee>[0],
				new IPasswordValidator<Employee>[0],
				new Mock<ILookupNormalizer>().Object,
				new Mock<IdentityErrorDescriber>().Object,
				new Mock<IServiceProvider>().Object,
				new Mock<ILogger<UserManager<Employee>>>().Object);
			var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
                .CreateLogger();
            var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
            mockAuthorizationServiceFalse.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => false);
            var handler = new SetPasswordForEmployeeHandler(
	            userManager.Object,
				_mockDbContext.Object,
	            passwordHasher.Object,
				serilogLogger,
				_mockMediator.Object,
                mockAuthorizationServiceFalse.Object);
            var authoriseResult = await handler.AuthoriseAsync(command, null!);
            Assert.False(authoriseResult);
        }
    }
}
