﻿using Application.Commands.Employees;
using Application.Services;
using Application.UnitTest.TestHelpers.Serilog;
using Domain;
using Domain.Entities.FeatureFlags;
using Domain.Entities.Organisations;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Moq;
using Serilog;

namespace Application.UnitTest.Commands.Employees
{
    public class AddNewEmployeeTests
	{
        private readonly Mock<IKensaDbContext> _mockDbContext;
        private readonly Mock<IMediator> _mockMediator;

        public AddNewEmployeeTests()
        {
	        _mockDbContext = new Mock<IKensaDbContext>();
            _mockMediator = new Mock<IMediator>();
        }
        [Fact]
        public void Command_Invalid_If_Email_Is_Invalid_Or_Missing_FirstName_Or_LastName_Or_Duplicate_Metadata()
        {
            var command = new AddNewEmployee();
            Assert.False(command.IsValid());
            command = new AddNewEmployee("","fakeFirstName","fakeLastName", new[] { "fakeGroupId" }, []);
            Assert.False(command.IsValid());
            command = new AddNewEmployee("aaa", "fakeFirstName", "fakeLastName", new[] { "fakeGroupId" }, []);
            Assert.False(command.IsValid());
            command = new AddNewEmployee("@aaa", "fakeFirstName", "fakeLastName", new[] { "fakeGroupId" }, []);
            Assert.False(command.IsValid());
            command = new AddNewEmployee("aaa@aaa", "", "fakeLastName", new[] { "fakeGroupId" }, []);
            Assert.False(command.IsValid());
            command = new AddNewEmployee("aaa@aaa", "fakeFirstName", "", new[] { "fakeGroupId" }, []);
            Assert.False(command.IsValid());
            command = new AddNewEmployee("aaa@aaa", "fakeFirstName", "", new[] { "fakeGroupId" }, [new EmployeeMetadataRequest("fakeMetadataId1","val1"), new EmployeeMetadataRequest("fakeMetadataId1", "val2")]);
            Assert.False(command.IsValid());
		}
        [Fact]
        public async Task AuthoriseAsync_Returns_False_If_Authorized_False()
        {
			var command = new AddNewEmployee("aaa@aaa", "fakeFirstName", "", new[] { "fakeGroupId" }, []);
            var passwordHasher = new Mock<IPasswordHasher<Employee>>();
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
                .CreateLogger();
            var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
            mockAuthorizationServiceFalse.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => false);
            var handler = new AddNewEmployeeHandler(
                _mockDbContext.Object,
                passwordHasher.Object,
                serilogLogger,
                mockAuthorizationServiceFalse.Object,
				_mockMediator.Object);
            var authoriseResult = await handler.AuthoriseAsync(command, null!);
            Assert.False(authoriseResult);
        }
    }
}
