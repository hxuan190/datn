﻿using Application.Commands.Groups;
using Application.Services;
using Application.UnitTest.TestHelpers.Serilog;
using Domain;
using Domain.Entities.FeatureFlags;
using Domain.Entities.Organisations;
using Domain.Events.Groups;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog;
using Serilog.Events;

namespace Application.UnitTest.Commands.Groups
{
	public class AddNewGroupHandlerTests
	{
		private readonly Mock<IKensaDbContext> _mockDbContext;
		private readonly Mock<IMediator> _mockMediator;
		private readonly Mock<IUser> _mockUser;
		private readonly Mock<IAuthorizationService> _mockAuthorizationService;
		public AddNewGroupHandlerTests()
		{
			var groups = new Mock<DbSet<Group>>();
			groups.Setup(x => x.Add(It.IsAny<Group>())).Verifiable();
			_mockDbContext = new Mock<IKensaDbContext>();
			_mockDbContext
				.Setup(x => x.Groups)
				.Returns(groups.Object);
			_mockMediator = new Mock<IMediator>();
			_mockUser = new Mock<IUser>();
			_mockAuthorizationService = new Mock<IAuthorizationService>();
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => true);
		}
		[Fact]
		public async Task Command_Invalid_If_Name_Empty()
		{
			var command = new AddNewGroup();
			Assert.False(command.IsValid());
			command = new AddNewGroup("", 0, "", []);
			Assert.False(command.IsValid());
			command = new AddNewGroup(null!, 0, "", []);
			Assert.False(command.IsValid());
		}
		[Fact]
		public async Task AuthoriseAsync_Returns_False_If_UserIsNull()
		{
			var command = new AddNewGroup("Test Group", 0, "Test Description", []);
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
				.CreateLogger();
			var handler = new AddNewGroupHandler(_mockDbContext.Object, serilogLogger,
				_mockAuthorizationService.Object, _mockMediator.Object);
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						null!, It.IsAny<Feature>()))
				.ReturnsAsync(() => false);
			var authoriseResult = await handler.AuthoriseAsync(command, null!);
			Assert.False(authoriseResult);
		}
		[Fact]
		public async Task AuthoriseAsync_Returns_False_If_Authorized_False()
		{
			var command = new AddNewGroup("Test Group", 0, "Test Description", []);
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
				.CreateLogger();
			var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
			mockAuthorizationServiceFalse.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => false);
			var handler = new AddNewGroupHandler(_mockDbContext.Object,
				serilogLogger,
				mockAuthorizationServiceFalse.Object, _mockMediator.Object);
			var authoriseResult = await handler.AuthoriseAsync(command, null!);
			Assert.False(authoriseResult);
		}
		[Fact]
		public async Task HandleAsync_ShouldAddNewGroup()
		{
			// Arrange
			var command = new AddNewGroup("Test Group", 0, "Test Description", []);
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var handler = new AddNewGroupHandler(_mockDbContext.Object, serilogLogger, _mockAuthorizationService.Object, _mockMediator.Object);

			// Act
			var result = await handler.HandleAsync(command, _mockUser.Object, CancellationToken.None);

			// Assert
			//log
			Assert.NotNull(evt);
			Assert.Equal(LogEventLevel.Information, evt.Level);
			Assert.Equal("Add new group {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
			//result
			Assert.NotNull(result);
			Assert.Null(result.Error);
			Assert.IsType<AddNewGroupCommandResult>(result);
			var addNewGroupCommandResult = (AddNewGroupCommandResult)result;
			Assert.NotNull(addNewGroupCommandResult.GroupId);
			//method calling
			_mockDbContext.Verify(db => db.Groups.Add(It.IsAny<Group>()), Times.Once);
			_mockDbContext.Verify(db => db.SaveChangesAsync(CancellationToken.None), Times.Once);
			_mockMediator.Verify(m => m.Publish(It.Is<GroupAddedEvent>(
				x => x.Id == addNewGroupCommandResult.GroupId
				   && x.Name == "Test Group"
				   && x.Description == "Test Description"
				   )
				, CancellationToken.None), Times.Once);
		}
	}
}
