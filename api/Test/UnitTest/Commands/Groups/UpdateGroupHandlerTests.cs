﻿using Application.Commands.Groups;
using Application.Services;
using Application.UnitTest.TestHelpers.Serilog;
using Domain;
using Domain.Entities.FeatureFlags;
using Domain.Entities.Organisations;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog;

namespace Application.UnitTest.Commands.Groups
{
    public class UpdateGroupHandlerTests
    {
        private readonly Mock<IKensaDbContext> _mockDbContext;
        private readonly Mock<IMediator> _mockMediator;
        private readonly Mock<IUser> _mockUser;
        private readonly Mock<IAuthorizationService> _mockAuthorizationService;
        public UpdateGroupHandlerTests()
        {
            var groups = new Mock<DbSet<Group>>();
            _mockDbContext = new Mock<IKensaDbContext>();
            _mockDbContext
                .Setup(x => x.Groups)
                .Returns(groups.Object);
            _mockMediator = new Mock<IMediator>();
            _mockUser = new Mock<IUser>();
            _mockAuthorizationService = new Mock<IAuthorizationService>();
            _mockAuthorizationService.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => true);
        }
        [Fact]
        public void Command_Invalid_If_Name_Empty()
        {
            var command = new UpdateGroup();
            Assert.False(command.IsValid());
            command = new UpdateGroup("", "");
            Assert.False(command.IsValid());
            command = new UpdateGroup(null!, "");
            Assert.False(command.IsValid());
        }
        [Fact]
        public async Task AuthoriseAsync_Returns_False_If_UserIsNull()
        {
            var command = new UpdateGroup("Test Group", "Test Description");
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
                .CreateLogger();
            var handler = new UpdateGroupHandler(_mockDbContext.Object, serilogLogger,
                _mockAuthorizationService.Object, _mockMediator.Object);
            _mockAuthorizationService.Setup(
                    x => x.IsAuthorizedForFeature(
                        null!, It.IsAny<Feature>()))
                .ReturnsAsync(() => false);
            var authoriseResult = await handler.AuthoriseAsync(command, null!);
            Assert.False(authoriseResult);
        }
        [Fact]
        public async Task AuthoriseAsync_Returns_False_If_Authorized_False()
        {
            var command = new UpdateGroup("Test Group", "Test Description");
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
                .CreateLogger();
            var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
            mockAuthorizationServiceFalse.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => false);
            var handler = new UpdateGroupHandler(_mockDbContext.Object,
                serilogLogger,
                mockAuthorizationServiceFalse.Object, _mockMediator.Object);
            var authoriseResult = await handler.AuthoriseAsync(command, null!);
            Assert.False(authoriseResult);
        }
    }
}
