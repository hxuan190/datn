﻿using Application.Commands.Groups;
using Application.Services;
using Application.UnitTest.TestHelpers.Serilog;
using Domain;
using Domain.Entities.FeatureFlags;
using Domain.Entities.Organisations;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog; 
namespace Application.UnitTest.Commands.Groups
{
    public class RemoveEmployeesFromAGroupTests
    {
        private readonly Mock<IKensaDbContext> _mockDbContext;
        private readonly Mock<IMediator> _mockMediator;
        private readonly Mock<IAuthorizationService> _mockAuthorizationService;

        public RemoveEmployeesFromAGroupTests()
        {
            var groupEmployee = new Mock<DbSet<GroupEmployee>>();
            groupEmployee.Setup(x => x.Add(It.IsAny<GroupEmployee>())).Verifiable();
            _mockDbContext = new Mock<IKensaDbContext>();
            _mockDbContext
                .Setup(x => x.GroupEmployees)
                .Returns(groupEmployee.Object);
            _mockMediator = new Mock<IMediator>();
            _mockAuthorizationService = new Mock<IAuthorizationService>();
            _mockAuthorizationService.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => true);
        }
        [Fact]
        public void Command_Invalid_If_EmployeeIds_Empty()
        {
	        var command = new RemoveEmployeesFromGroup([], "fakeGroupId");
	        Assert.False(command.IsValid());
	        command = new RemoveEmployeesFromGroup();
	        Assert.False(command.IsValid());
		}
		[Fact]
        public void Command_Invalid_If_GroupId_Null_Empty()
        {
	        var command = new RemoveEmployeesFromGroup( ["1", "2"], null!);
	        Assert.False(command.IsValid());
			command = new RemoveEmployeesFromGroup(["1", "2"], "");
			Assert.False(command.IsValid());
        }
		[Fact]
        public async Task AuthoriseAsync_Returns_False_If_Authorized_False()
        {
            var command = new RemoveEmployeesFromGroup(["1", "2"], "1");
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
                .CreateLogger();
            var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
            mockAuthorizationServiceFalse.Setup(
                    x => x.IsAuthorizedForFeature(
                        It.IsAny<IUser>(), It.IsAny<Feature>()))
                .ReturnsAsync(() => false);
            var handler = new RemoveEmployeesFromAGroupHandler(_mockDbContext.Object,
                serilogLogger,
                mockAuthorizationServiceFalse.Object, _mockMediator.Object);
            var authoriseResult = await handler.AuthoriseAsync(command, null!);
            Assert.False(authoriseResult);
        }

        [Fact]
        public async Task AuthoriseAsync_Returns_True_If_Authorized_True()
        {
            var command = new RemoveEmployeesFromGroup(new string[] { "1", "2" }, "1");
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => _ = e))
                .CreateLogger();
            var handler = new RemoveEmployeesFromAGroupHandler(_mockDbContext.Object,
                               serilogLogger,
                                              _mockAuthorizationService.Object, _mockMediator.Object);
            var authoriseResult = await handler.AuthoriseAsync(command, null!);
            Assert.True(authoriseResult);
        }

        
    }
}
