﻿using Application.Services;
using Application.UnitTest.TestHelpers;
using Domain;
using Infrastructure.Persistence;
using Moq;
using Serilog;
using Serilog.Events;
using Application.UnitTest.TestHelpers.Serilog;
using Domain.Entities.FeatureFlags;
using Application.Queries.Employees;

namespace Application.UnitTest.Queries.Employees
{
	public class SearchEmployeeTests
	{
		private readonly IKensaDbContext _kensaDbContext;
		private readonly SearchEmployees _query;
		private readonly IUser _fakeUser;
		private readonly Mock<IAuthorizationService> _mockAuthorizationService;

		public SearchEmployeeTests()
		{
			var mockUserProvider = new Mock<IUserProvider>();
			_kensaDbContext = new InMemoryKensaDbContext(mockUserProvider.Object);
			_query = new SearchEmployees("keyword");
			_fakeUser = new FakeUser();

			_mockAuthorizationService = new Mock<IAuthorizationService>();
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => true);
		}

		[Fact]
		public async Task AuthoriseAsync_AuthorizedFail_ReturnsFailed()
		{
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
			mockAuthorizationServiceFalse.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => false);

			var executor = new SearchEmployeesExecutor(serilogLogger,
				_kensaDbContext,
				mockAuthorizationServiceFalse.Object);
			var result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.False(result);
		}

		[Fact]
		public async Task AuthoriseAsync_AuthorizedSuccess_ReturnsTrue()
		{
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var executor = new SearchEmployeesExecutor(serilogLogger,
				_kensaDbContext,
				_mockAuthorizationService.Object);
			var result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.True(result);
		}
	}
}
