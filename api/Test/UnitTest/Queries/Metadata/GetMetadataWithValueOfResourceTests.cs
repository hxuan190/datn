﻿using Application.UnitTest.TestHelpers.Serilog;
using Application.UnitTest.TestHelpers;
using Domain;
using Infrastructure.Persistence;
using Moq;
using Serilog.Events;
using Serilog;
using Application.Services;
using Application.Queries.Metadata;
using Domain.Entities.Organisations;
using Domain.Entities.FeatureFlags;

namespace Application.UnitTest.Queries.Metadata
{
	public class GetMetadataWithValueOfResourceTests
	{
		private readonly IKensaDbContext _kensaDbContext;
		private GetMetadataWithValueOfResource _query;
		private readonly IUser _fakeUser;
		public GetMetadataWithValueOfResourceTests()
		{
			var mockUserProvider = new Mock<IUserProvider>();
			_kensaDbContext = new InMemoryKensaDbContext(mockUserProvider.Object);
			_fakeUser = new FakeUser();
		}

		[Fact]
		public async Task AuthoriseAsync_AuthorizedFail_ReturnsFailed()
		{
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
			mockAuthorizationServiceFalse.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => false);
			var mockAclService = new Mock<IAclService>();
			var executor = new GetMetadataWithValueOfResourceExecutor(serilogLogger,
				_kensaDbContext,
				mockAuthorizationServiceFalse.Object,
				mockAclService.Object);
			_query = new GetMetadataWithValueOfResource(ResourceType.Employee,"fakeEmployeeId");
			var result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.False(result);
			_query = new GetMetadataWithValueOfResource(ResourceType.Job, "fakeJobId");
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.False(result);
			_query = new GetMetadataWithValueOfResource(ResourceType.Site, "fakeSiteId");
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.False(result);
			_query = new GetMetadataWithValueOfResource((ResourceType) 111111, "fakeResourceId");
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.False(result);
		}
		[Fact]
		public async Task AuthoriseAsync_AuthorizedSuccess_ReturnsTrue()
		{
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var mockAuthorizationService = new Mock<IAuthorizationService>();
			mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(true);
			var mockAclService = new Mock<IAclService>();
			var executor = new GetMetadataWithValueOfResourceExecutor(serilogLogger,
				_kensaDbContext,
				mockAuthorizationService.Object,
				mockAclService.Object);
			_query = new GetMetadataWithValueOfResource(ResourceType.Employee, "fakeEmployeeId");
			var result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.True(result);
			_query = new GetMetadataWithValueOfResource(ResourceType.Job, "fakeJobId");
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.True(result);
			_query = new GetMetadataWithValueOfResource(ResourceType.Site, "fakeSiteId");
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.True(result);
		}
	}
}
