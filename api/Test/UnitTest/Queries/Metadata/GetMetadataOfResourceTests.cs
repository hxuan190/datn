﻿using Application.UnitTest.TestHelpers.Serilog;
using Application.UnitTest.TestHelpers;
using Domain;
using Infrastructure.Persistence;
using Moq;
using Serilog.Events;
using Serilog;
using Application.Services;
using Application.Queries.Metadata;
using Domain.Entities.Organisations;

namespace Application.UnitTest.Queries.Metadata
{
	public class GetMetadataOfResourceTests
	{
		private readonly IKensaDbContext _kensaDbContext;
		private GetMetadataOfResource _query;
		private readonly IUser _fakeUser;
		public GetMetadataOfResourceTests()
		{
			var mockUserProvider = new Mock<IUserProvider>();
			_kensaDbContext = new InMemoryKensaDbContext(mockUserProvider.Object);
			_fakeUser = new FakeUser();
		}

		[Fact]
		public async Task AuthoriseAsync_Return_True()
		{
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
			
			var executor = new GetMetadataOfResourceExecutor(serilogLogger,
				_kensaDbContext,
				mockAuthorizationServiceFalse.Object);
			_query = new GetMetadataOfResource(ResourceType.Employee);
			var result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.True(result);
			
		}
	}
}
