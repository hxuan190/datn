﻿using Application.UnitTest.TestHelpers.Serilog;
using Moq;
using Serilog.Events;
using Serilog;
using Application.Queries.GridViews;
using Application.Services;
using Domain;
using Infrastructure.Persistence;
using Domain.Entities.FeatureFlags;
using Application.UnitTest.TestHelpers;
using Domain.Entities.Organisations;

namespace Application.UnitTest.Queries.GridViews
{
	public class GetGridViewsTests
	{
		private readonly IKensaDbContext _kensaDbContext;
		private GetGridViews _query;
		private readonly IUser _fakeUser;
		private readonly Mock<IAuthorizationService> _mockAuthorizationService;

		public GetGridViewsTests()
		{
			var mockUserProvider = new Mock<IUserProvider>();
			_kensaDbContext = new InMemoryKensaDbContext(mockUserProvider.Object);
			_fakeUser = new FakeUser();
			_mockAuthorizationService = new Mock<IAuthorizationService>();
			_mockAuthorizationService.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => true);
		}

		[Fact]
		public async Task AuthoriseAsync_AuthorizedFail_ReturnsFailed()
		{
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var mockAuthorizationServiceFalse = new Mock<IAuthorizationService>();
			mockAuthorizationServiceFalse.Setup(
					x => x.IsAuthorizedForFeature(
						It.IsAny<IUser>(), It.IsAny<Feature>()))
				.ReturnsAsync(() => false);
			var executor = new GetGridViewsExecutor(serilogLogger,
				_kensaDbContext,
				mockAuthorizationServiceFalse.Object);
			_query = new GetGridViews(ResourceType.Employee);
			var result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.False(result);
			_query = new GetGridViews(ResourceType.Site);
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.False(result);
			_query = new GetGridViews(ResourceType.Job);
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.False(result);
		}

		[Fact]
		public async Task AuthoriseAsync_AuthorizedSuccess_ReturnsTrue()
		{
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var executor = new GetGridViewsExecutor(serilogLogger,
				_kensaDbContext,
				_mockAuthorizationService.Object);
			_query = new GetGridViews(ResourceType.Employee);
			var result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.True(result);
			_query = new GetGridViews(ResourceType.Site);
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.True(result);
			_query = new GetGridViews(ResourceType.Job);
			result = await executor.AuthoriseAsync(_query, _fakeUser);
			Assert.True(result);
		}
	}


}
