﻿using Application.IntegrationTest.TestHelpers;
using Application.Services;
using Domain;
using Moq;
using Domain.Entities.Organisations;
using Serilog.Events;
using Application.IntegrationTest.TestHelpers.Serilog;
using Serilog;

namespace Application.IntegrationTest.Services
{
    public class AclServiceTests
    {
        private readonly AclService _aclService;
        private readonly List<LogEvent> _evts = [];
        public AclServiceTests()
        {
            IUser fakeUser = new FakeUser();
            var userProviderMock = new Mock<IUserProvider>();
            userProviderMock.Setup(x => x.User).Returns(fakeUser);
            var context = new InMemoryKensaDbContext(userProviderMock.Object);
            context.Organisations.Add(new Organisation("fakeOrganisationId",
                "fakeEmployeeId",
                "fakeOrganisationName",
                "fakeAddress",
                null,
                null,
                null,
                null,
                null
            ));
            //employees
            context.Employees.Add(new Employee("fakeEmployeeId1", "fakeOrganisationId", "fakeEmail@fakecompany.com", "fakeFirstName", "fakeLastName"));
            context.Employees.Add(new Employee("fakeEmployeeId2", "fakeOrganisationId", "fakeEmail@fakecompany.com2", "fakeFirstName", "fakeLastName"));
            //groups
            context.Groups.Add(new Group("fakeGroupId1", "fakeGroupName1", "fakeGroupDesc1", false, "fakeOrganisationId", 0));
            context.Groups.Add(new Group("fakeGroupId2", "fakeGroupName2", "fakeGroupDesc2", false, "fakeOrganisationId", 0));
            context.Groups.Add(new Group("fakeGroupId3", "fakeGroupName3", "fakeGroupDesc3", false, "fakeOrganisationId", 0));
            context.Groups.Add(new Group("fakeGroupId4", "fakeGroupName4", "fakeGroupDesc4", false, "fakeOrganisationId", 0));
            context.Groups.Add(new Group("fakeGroupId5", "fakeGroupName5", "fakeGroupDescz35", false, "fakeOrganisationId", 0));
            //group employees
            context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId1", "fakeEmployeeId1"));
            context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId2", "fakeEmployeeId1"));
            context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId3", "fakeEmployeeId1"));
            context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId3", "fakeEmployeeId2"));
            context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId4", "fakeEmployeeId2"));
            context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId5", "fakeEmployeeId1"));
            context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId5", "fakeEmployeeId2"));

            //acl 
            //acl - employee - bank account
            // (group 1: allow all; group 2: deny all; employee 1: force allow to view e1 and e2)
            context.Acls.Add(new Acl("fakeAclId1", "fakeOrganisationId", "fakeGroupId1", SubjectType.Group, AclAccess.Allow, ResourceType.Employee, "BANK_ACCOUNT"));
            context.Acls.Add(new Acl("fakeAclId2", "fakeOrganisationId", "fakeGroupId2", SubjectType.Group, AclAccess.Deny, ResourceType.Employee, "BANK_ACCOUNT"));
            context.Acls.Add(new Acl("fakeAclId3", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Allow, ResourceType.Employee, "BANK_ACCOUNT", "e1"));
            context.Acls.Add(new Acl("fakeAclId4", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Allow, ResourceType.Employee, "BANK_ACCOUNT", "e2"));
            // (group 4: allow all; employee 2: force deny to view e1)
            context.Acls.Add(new Acl("fakeAclId5", "fakeOrganisationId", "fakeGroupId4", SubjectType.Group, AclAccess.Allow, ResourceType.Employee, "BANK_ACCOUNT"));
            context.Acls.Add(new Acl("fakeAclId6", "fakeOrganisationId", "fakeEmployeeId2", SubjectType.Employee, AclAccess.Deny, ResourceType.Employee, "BANK_ACCOUNT", "e1"));

            //acl - employee - driver license
            //(group 2: allow all;, group 3: deny all; employee 1: force allow to view e1)
            context.Acls.Add(new Acl("fakeAclId7", "fakeOrganisationId", "fakeGroupId2", SubjectType.Group, AclAccess.Allow, ResourceType.Employee, "DRIVER_LICENSE"));
            context.Acls.Add(new Acl("fakeAclId8", "fakeOrganisationId", "fakeGroupId3", SubjectType.Group, AclAccess.Deny, ResourceType.Employee, "DRIVER_LICENSE"));
            context.Acls.Add(new Acl("fakeAclId9", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Allow, ResourceType.Employee, "DRIVER_LICENSE", "e1"));
            //(group 5: deny all; employee 2: force allow to view e2)
            context.Acls.Add(new Acl("fakeAclId10", "fakeOrganisationId", "fakeGroupId5", SubjectType.Group, AclAccess.Deny, ResourceType.Employee, "DRIVER_LICENSE"));
            context.Acls.Add(new Acl("fakeAclId11", "fakeOrganisationId", "fakeEmployeeId2", SubjectType.Employee, AclAccess.Allow, ResourceType.Employee, "DRIVER_LICENSE", "e2"));

            //acl - site - contract
            //(group 1: deny all;  group 2: allow all; employee 1: force allow to view s1, s3)
            context.Acls.Add(new Acl("fakeAclId12", "fakeOrganisationId", "fakeGroupId1", SubjectType.Group, AclAccess.Deny, ResourceType.Site, "CONTRACT"));
            context.Acls.Add(new Acl("fakeAclId13", "fakeOrganisationId", "fakeGroupId2", SubjectType.Group, AclAccess.Allow, ResourceType.Site, "CONTRACT"));
            context.Acls.Add(new Acl("fakeAclId14", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Allow, ResourceType.Site, "CONTRACT", "s1"));
            context.Acls.Add(new Acl("fakeAclId15", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Allow, ResourceType.Site, "CONTRACT", "s3"));
            //(group 4: deny all, group 5: allow all, force deny s1; employee 2: force allow to view s2 )
            context.Acls.Add(new Acl("fakeAclId16", "fakeOrganisationId", "fakeGroupId4", SubjectType.Group, AclAccess.Deny, ResourceType.Site, "CONTRACT"));
            context.Acls.Add(new Acl("fakeAclId17", "fakeOrganisationId", "fakeGroupId5", SubjectType.Group, AclAccess.Allow, ResourceType.Site, "CONTRACT"));
            context.Acls.Add(new Acl("fakeAclId18", "fakeOrganisationId", "fakeGroupId5", SubjectType.Group, AclAccess.Deny, ResourceType.Site, "CONTRACT", "s1"));
            context.Acls.Add(new Acl("fakeAclId19", "fakeOrganisationId", "fakeEmployeeId2", SubjectType.Employee, AclAccess.Allow, ResourceType.Site, "CONTRACT", "s2"));

            //acl - site - price
            //(group 2: deny all, force allow s1;  group 3: deny all; employee 1: allow all, force deny to view s1, s3)
            context.Acls.Add(new Acl("fakeAclId20", "fakeOrganisationId", "fakeGroupId2", SubjectType.Group, AclAccess.Deny, ResourceType.Site, "PRICE"));
            context.Acls.Add(new Acl("fakeAclId21", "fakeOrganisationId", "fakeGroupId2", SubjectType.Group, AclAccess.Allow, ResourceType.Site, "PRICE", "s1"));
            context.Acls.Add(new Acl("fakeAclId22", "fakeOrganisationId", "fakeGroupId3", SubjectType.Group, AclAccess.Deny, ResourceType.Site, "PRICE"));
            context.Acls.Add(new Acl("fakeAclId23", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Allow, ResourceType.Site, "PRICE"));
            context.Acls.Add(new Acl("fakeAclId24", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Deny, ResourceType.Site, "PRICE", "s1"));
            context.Acls.Add(new Acl("fakeAclId25", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Deny, ResourceType.Site, "PRICE", "s3"));

            //acl - site - diagram
            //(employee1: force deny s1 ; employee3: force deny s2)
            context.Acls.Add(new Acl("fakeAclId26", "fakeOrganisationId", "fakeEmployeeId1", SubjectType.Employee, AclAccess.Deny, ResourceType.Site, "DIAGRAM", "s1"));
            context.Acls.Add(new Acl("fakeAclId27", "fakeOrganisationId", "fakeEmployeeId2", SubjectType.Employee, AclAccess.Deny, ResourceType.Site, "DIAGRAM", "s2"));
            context.SaveChanges();

            var serilogLogger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(
                    _evts.Add))
                .CreateLogger();
            _aclService = new AclService(context, serilogLogger);
        }

        public static IEnumerable<object[]> TestAclServiceData()
        {

            #region employee - bank account
            //(group 1: allow all; group 2: deny all; employee 1: force allow to view e1 and e2)
            yield return
            [
                "fakeGroupId1",
                SubjectType.Group,
                new[] { "BANK_ACCOUNT" },
                ResourceType.Employee,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { [] } //no opposite employee ids
			];
            yield return
            [
                "fakeGroupId2",
                SubjectType.Group,
                new[] { "BANK_ACCOUNT" },
                ResourceType.Employee,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { [] } //no opposite employee ids
			];
            yield return
            [
                "fakeEmployeeId1",
                SubjectType.Employee,
                new[] { "BANK_ACCOUNT" },
                ResourceType.Employee,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { ["e1", "e2"] } //no opposite employee ids
			];
            //(group 4: allow all; employee 2: force deny to view e1)
            yield return
            [
                "fakeGroupId4",
                SubjectType.Group,
                new[] { "BANK_ACCOUNT" },
                ResourceType.Employee,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { [] } //no opposite employee ids
			];
            yield return
            [
                "fakeEmployeeId2",
                SubjectType.Employee,
                new[] { "BANK_ACCOUNT" },
                ResourceType.Employee,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { ["e1"] } //no opposite employee ids
			];

            #endregion

            #region employee - driver license
            //(group 2: allow all;, group 3: deny all; employee 1: force allow to view e1)
            yield return
            [
                "fakeGroupId2",
                SubjectType.Group,
                new[] { "DRIVER_LICENSE" },
                ResourceType.Employee,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { [] }
            ];
            yield return
            [
                "fakeGroupId3",
                SubjectType.Group,
                new[] { "DRIVER_LICENSE" },
                ResourceType.Employee,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { [] }
            ];

            yield return
            [
                "fakeEmployeeId1",
                SubjectType.Employee,
                new[] { "DRIVER_LICENSE" },
                ResourceType.Employee,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { ["e1"] }
            ];
            #endregion

            #region site - contract
            //(group 1: deny all;  group 2: allow all; employee 1: force allow to view s1, s3)
            yield return
            [
                "fakeGroupId1",
                SubjectType.Group,
                new[] { "CONTRACT" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { [] }
            ];
            yield return
            [
                "fakeGroupId2",
                SubjectType.Group,
                new[] { "CONTRACT" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { [] }
            ];

            yield return
            [
                "fakeEmployeeId1",
                SubjectType.Employee,
                new[] { "CONTRACT" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { ["s1", "s3"] }
            ];

            //(group 4: deny all, group 5: allow all, force deny s1; employee 2: force allow to view s2 )
            yield return
            [
                "fakeGroupId4",
                SubjectType.Group,
                new[] { "CONTRACT" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { [] }
            ];
            yield return
            [
                "fakeGroupId5",
                SubjectType.Group,
                new[] { "CONTRACT" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { ["s1"] }
            ];

            yield return
            [
                "fakeEmployeeId2",
                SubjectType.Employee,
                new[] { "CONTRACT" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { ["s2"] }
            ];
            #endregion

            #region acl - site - price
            //(group 2: deny all, force allow s1; group 3: deny all; employee 1: allow all, force deny to view s1, s3)
            yield return
            [
                "fakeGroupId2",
                SubjectType.Group,
                new[] { "PRICE" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { ["s1"] }
            ];
            yield return
            [
                "fakeGroupId3",
                SubjectType.Group,
                new[] { "PRICE" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Deny },
                new string[][] { [] }
            ];

            yield return
            [
                "fakeEmployeeId1",
                SubjectType.Employee,
                new[] { "PRICE" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { ["s1", "s3"] }
            ];
            #endregion

            #region diagram
            //(employee1: force deny s1 ; employee3: force deny s2)
            yield return
            [
                "fakeEmployeeId1",
                SubjectType.Employee,
                new[] { "DIAGRAM" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { ["s1"] }
            ];

            yield return
            [
                "fakeEmployeeId2",
                SubjectType.Employee,
                new[] { "DIAGRAM" },
                ResourceType.Site,
				//expected
				1,
                new[] { AclAccess.Allow },
                new string[][] { ["s2"] }
            ];
            #endregion

            #region multiple resource properties
            yield return
            [
                "fakeEmployeeId2",
                SubjectType.Employee,
                new[] { "DIAGRAM", "FAKE" },
                ResourceType.Site,
				//expected
				2,
                new[] { AclAccess.Allow, AclAccess.Allow },
                new string[][] { ["s2"], [] }
            ];
            #endregion
        }

        [Theory]
        [MemberData(nameof(TestAclServiceData))]
        public async Task AclService_GetAclOfSubjectAgainstResourceProperties_Returns_Correct_Data(
            string subjectId,
            SubjectType subjectType,
            string[] resourcePropertyIds,
            ResourceType resourceType,
            int expectedTotalAcl,
            AclAccess[] expectedAccessDirections,
            string[][] expectedResourceIdsInOppositeDirections
            )
        {
            var orgId = "fakeOrganisationId";
            var result = await _aclService.GetAclOfSubjectAgainstResourceProperties(orgId,
                subjectId,
                subjectType,
                resourceType,
                resourcePropertyIds, default);
            Assert.Equal(expectedTotalAcl, result.Count);
            var aclAccessDirections = result.Select(x => x.AccessDirection).ToArray();
            Assert.Equal(expectedAccessDirections, aclAccessDirections);
            var resourceIdsInOppositeDirections = result.Select(x => x.ResourceIdsInOppositeDirection).ToArray();
            Assert.Equal(expectedResourceIdsInOppositeDirections, resourceIdsInOppositeDirections);
        }

        [Fact]
        public async Task AclService_GetAclOfSubjectAgainstResourceProperties_Returns_Empty_Array_If_Not_Pass_ResourcePropertyIds()
        {
            var result = await _aclService.GetAclOfSubjectAgainstResourceProperties("fakeOrganisationId",
                "fakeEmployeeId1",
                SubjectType.Employee,
                ResourceType.Employee,
                [], default);
            Assert.Empty(result);
        }
    }
}
