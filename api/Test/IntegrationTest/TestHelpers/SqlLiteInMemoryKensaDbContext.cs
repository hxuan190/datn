﻿using Domain;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;

namespace Application.IntegrationTest.TestHelpers
{
	internal class SqlLiteInMemoryKensaDbContext : KensaDbContext
	{
		public SqlLiteInMemoryKensaDbContext(IUserProvider userProvider, SqliteConnection connection) :
			base(new DbContextOptionsBuilder<KensaDbContext>()
					.UseSqlite(connection)
					.Options,
				userProvider)
		{
		}
	}
}