﻿using Domain;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Util.Extensions;

namespace Application.IntegrationTest.TestHelpers
{
    internal class InMemoryKensaDbContext : KensaDbContext
    {
        public InMemoryKensaDbContext(IUserProvider userProvider) :
            base(new DbContextOptionsBuilder<KensaDbContext>().UseInMemoryDatabase(IdExtension.Generate()).Options,
            userProvider)
        {
        }

        public override Task ExecuteDeleteAsync<T>(Expression<Func<T, bool>> predicate,
            bool ignoreQueryFilters = false,
            string? tag = "",
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var query = Set<T>().Where(predicate);
            if (ignoreQueryFilters)
            {
                query = query.IgnoreQueryFilters();
            }
            if (!string.IsNullOrEmpty(tag))
            {
                query = query.TagWith(tag);
            }
            //using normal here instead of ExecuteDeleteAsync because In-Memory database does not support it.
            Set<T>().RemoveRange(query);
            return SaveChangesAsync(cancellationToken);
        }

        public override Task ExecuteUpdateMultipleFields(IQueryable query,
	        IReadOnlyDictionary<string, object?> fieldValues,
	        CancellationToken cancellationToken = default)
        {
	        query.ExecuteUpdateManual(fieldValues);
			return SaveChangesAsync(cancellationToken);
		}

	}
}
