﻿using Serilog.Events;

namespace Application.IntegrationTest.TestHelpers.Serilog
{
    internal static class Extensions
    {
        public static object LiteralValue(this LogEventPropertyValue @this)
        {
            return ((ScalarValue)@this).Value!;
        }
    }
}
