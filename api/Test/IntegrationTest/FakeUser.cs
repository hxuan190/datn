﻿using Domain;
using Domain.Entities.Organisations;

namespace Application.IntegrationTest
{
    internal class FakeUser : IUser
    {
        public string Email => "fakeEmail";
        public string OrganisationId => "fakeOrganisationId";
        public string Id => null!;
        public bool IsImpersonating => false;
        public string? ImpersonatorEmail => null!;

        public string? IpAddress {get;set;}

        public int? Timezone => null!;

        public string? SessionId => null!;
        public void SetIpAddress(string? ipaddress)
        {

        }
    }

    public static class FakeData
    {
        public static Organisation FakeOrganisation =
            new Organisation("fakeOrganisationId", "fakeOwnerId", "fakeName", "fakeAddress1",null, null, null, null, null, null, null, null, null);
    }
    
}
