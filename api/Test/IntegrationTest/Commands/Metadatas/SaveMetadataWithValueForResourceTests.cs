﻿using Application.Commands.Metadata;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Services;
using Audit.Core;
using Audit.Core.Providers;
using Domain;
using Domain.Entities.Organisations;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog;
using Serilog.Events;
using Serilog.Core;
using Util;
using Domain.Events.Metadata;

namespace Application.IntegrationTest.Commands.Metadatas
{
	public class SaveMetadataWithValueForResourceTests
	{
		private KensaDbContext context;
		private SaveMetadataWithValueForResourceHandler _handler;
		private readonly List<LogEvent> _evts = new();
		private readonly IUser _fakeUser = new FakeUser();
		private readonly Logger serilogLogger;
		private readonly Mock<IMediator> _mediatorMock;
		private readonly Mock<IAuthorizationService> _authorizationServiceMock;
		private readonly Mock<IAclService> _aclServiceMock;
		private readonly string fakeOrganisationId = "fakeOrganisationId";
		private readonly string fakeEmployeeId = "fakeEmployeeId";
		public SaveMetadataWithValueForResourceTests()
		{
			Configuration.Setup().UseInMemoryProvider();
			var userProvider = new Mock<IUserProvider>();
			userProvider.Setup(x => x.User).Returns(_fakeUser);
			context = new InMemoryKensaDbContext(userProvider.Object);
			serilogLogger = new LoggerConfiguration()
				.MinimumLevel.Debug()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(
					_evts.Add))
				.CreateLogger();
			_mediatorMock = new Mock<IMediator>();
			_authorizationServiceMock = new Mock<IAuthorizationService>();
			_aclServiceMock = new Mock<IAclService>();
		}

		private async Task AddFakeData()
		{
			//org
			context.Organisations.Add(new Organisation(fakeOrganisationId, "fakeOwnerId", "fakeOrg", "fakeAddress",
				"fakeAddress2", "fakeCity", "fakeState", "fakeCountry", "fakeZipCode"));
			//employee
			context.Employees.Add(new Employee(fakeEmployeeId, fakeOrganisationId, "fakeEmail", "fakeFirstName", "fakeLastName"));
			//metadata
			context.MetadataCategories.Add(new MetadataCategory("fakeMetadataCategory1", fakeOrganisationId, "fakeMetadataCategory1", 1));
			context.MetadataCategories.Add(new MetadataCategory("fakeMetadataCategory2", fakeOrganisationId, "fakeMetadataCategory2", 2));
			context.Metadatas.Add(new Metadata("fakeMetadata1", fakeOrganisationId, "fakeMetadata1", MetadataDataType.Number, ResourceType.Employee, true, "fakeMetadataCategory1", 1));
			context.Metadatas.Add(new Metadata("fakeMetadata2", fakeOrganisationId, "fakeMetadata2", MetadataDataType.Text, ResourceType.Employee, false, "fakeMetadataCategory1", 2));
			context.Metadatas.Add(new Metadata("fakeMetadata3", fakeOrganisationId, "fakeMetadata3", MetadataDataType.Number, ResourceType.Employee, true, "fakeMetadataCategory2", 1));
			context.Metadatas.Add(new Metadata("fakeMetadata4", fakeOrganisationId, "fakeMetadata4", MetadataDataType.Text, ResourceType.Employee, true, "", 1, true));
			context.MetadataPredefinedValues.Add(new MetadataPredefinedValue("fakeMetadataPredefinedValue1", fakeOrganisationId, "fakeMetadata2", "value1"));
			context.MetadataPredefinedValues.Add(new MetadataPredefinedValue("fakeMetadataPredefinedValue2", fakeOrganisationId, "fakeMetadata2", "value2"));
			//metadata resource
			context.MetadataResourceValues.Add(new MetadataResourceValue(fakeOrganisationId, ResourceType.Employee,
				fakeEmployeeId, "fakeMetadata1", "fakeValue1"));
			context.MetadataResourceValues.Add(new MetadataResourceValue(fakeOrganisationId, ResourceType.Employee,
				fakeEmployeeId, "fakeMetadata3", "fakeValue3"));
			await context.SaveChangesAsync();
		}

		[Theory]
		[InlineData(ResourceType.Employee, ErrorCodes.APPLICATION_NOTFOUND_ENTITY_EMPLOYEE, "Not found entity when handling Application.Commands.Metadata.SaveMetadataWithValueForResource. Detail: Not found employee to save metadata values")]
		public async Task HandleAsync_Should_Throw_NotFoundEntityException_If_Save_For_Not_Exist_Resource(
			ResourceType resourceType, ErrorCodes errorCode, string errorDescription)
		{
			// Arrange
			_handler = new SaveMetadataWithValueForResourceHandler(context,
				serilogLogger,
				_authorizationServiceMock.Object,
				_aclServiceMock.Object,
				_mediatorMock.Object);
			var command = new SaveMetadataWithValueForResource("not_exist_resource_id",
				resourceType,
				[
					new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "fakeMetadata4", "")
				]);
			// Act
			var ex = await Assert.ThrowsAsync<NotFoundEntityException<SaveMetadataWithValueForResource>>(
				() => _handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			// Assert
			Assert.Equal(errorCode, ex.ErrorCode);
			Assert.Equal(errorDescription, ex.Message);
		}
		[Theory]
		[InlineData(ResourceType.Employee, 
			ErrorCodes.APPLICATION_INVALID,
			"Command Application.Commands.Metadata.SaveMetadataWithValueForResource is invalid to handle. Detail: Required metadata must have value")]
		public async Task HandleAsync_Should_Throw_InvalidException_If_Save_RequiredMetadata_Without_Value(
			ResourceType resourceType, ErrorCodes errorCode, string errorDescription)
		{
			// Arrange
			await AddFakeData();
			_handler = new SaveMetadataWithValueForResourceHandler(context,
				serilogLogger,
				_authorizationServiceMock.Object,
				_aclServiceMock.Object,
				_mediatorMock.Object);
			var command = new SaveMetadataWithValueForResource(fakeEmployeeId,
				resourceType,
				[
					new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "fakeMetadata4", "")
				]);
			// Act
			var ex = await Assert.ThrowsAsync<InvalidException<SaveMetadataWithValueForResource>>(
				() => _handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			// Assert
			Assert.Equal(errorCode, ex.ErrorCode);
			Assert.Equal(errorDescription, ex.Message);
			var checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Warning, MessageTemplate.Text: "There was an attempt to save required metadata without value" });
			Assert.NotNull(checkLog);
		}
		[Theory]
		[InlineData(ResourceType.Employee,
			ErrorCodes.APPLICATION_INVALID,
			"Command Application.Commands.Metadata.SaveMetadataWithValueForResource is invalid to handle. Detail: Metadata is not allowed to have free text but input wrong predefined value")]
		public async Task HandleAsync_Should_Throw_InvalidException_If_Use_Wrong_PredefinedValue(
			ResourceType resourceType, ErrorCodes errorCode, string errorDescription)
		{
			// Arrange
			await AddFakeData();
			_handler = new SaveMetadataWithValueForResourceHandler(context,
				serilogLogger,
				_authorizationServiceMock.Object,
				_aclServiceMock.Object,
				_mediatorMock.Object);
			var command = new SaveMetadataWithValueForResource(fakeEmployeeId,
				resourceType,
				[
					new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "fakeMetadata2", "wrong_defined_value")
				]);
			// Act
			var ex = await Assert.ThrowsAsync<InvalidException<SaveMetadataWithValueForResource>>(
				() => _handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			// Assert
			Assert.Equal(errorCode, ex.ErrorCode);
			Assert.Equal(errorDescription, ex.Message);
			var checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Warning, MessageTemplate.Text: "Metadata {MetadataId} is not allowed to have free text but input wrong predefined value" });
			Assert.NotNull(checkLog);
		}
		[Fact]
		public async Task HandleAsync_Should_AddResourceValues()
		{
			await AddFakeData();
			// Arrange
			_handler = new SaveMetadataWithValueForResourceHandler(context,
				serilogLogger,
				_authorizationServiceMock.Object,
				_aclServiceMock.Object,
				_mediatorMock.Object);
			var command = new SaveMetadataWithValueForResource(fakeEmployeeId,
				ResourceType.Employee,
				[
					new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "fakeMetadata2", "fakeMetadataPredefinedValue1"),
					new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "fakeMetadata4", "fakeValue4"),
					new SaveMetadataWithValueRequest(SaveMetadataAction.Add, "notExist", "notexistvalue")
				]);
			// Act
			await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
			var metadataResourceValue2 = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "fakeMetadata2");
			Assert.NotNull(metadataResourceValue2);
			Assert.Equal("fakeMetadataPredefinedValue1", metadataResourceValue2.Value);
			var metadataResourceValue4 = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "fakeMetadata4");
			Assert.NotNull(metadataResourceValue4);
			Assert.Equal("fakeValue4", metadataResourceValue4.Value);
			var metadataResourceNotExist = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "notExist");
			Assert.Null(metadataResourceNotExist);

			//mediator
			_mediatorMock.Verify(x => x.Publish(
				It.Is<ResourceMetadataValueSavedEvent>(
					m =>
						m.ResourceOrganisationId == fakeOrganisationId
						&& m.ResourceType == ResourceType.Employee
						&& m.ResourceId == fakeEmployeeId
						&& m.ResourceMetadataValueSavedRecords.Length == 2
						&& m.ResourceMetadataValueSavedRecords.FirstOrDefault
						(l => l.MetadataId == "fakeMetadata2"
							  && l.Action == SaveMetadataActionType.Add
							  && l.NewValue == "fakeMetadataPredefinedValue1") != null
						&& m.ResourceMetadataValueSavedRecords.FirstOrDefault
						(l => l.MetadataId == "fakeMetadata4"
							  && l.Action == SaveMetadataActionType.Add
							  && l.NewValue == "fakeValue4") != null
				)
				, CancellationToken.None), Times.Once);
			//logs
			var checkLog = _evts.FirstOrDefault(x =>
								x is { Level: LogEventLevel.Warning, MessageTemplate.Text: "Metadata {MetadataId} is not found or not allowed to add" });
			Assert.NotNull(checkLog);
			checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Debug, MessageTemplate.Text: "Adding {Count} metadata values for {ResourceId} type {ResourceType}" });
			Assert.NotNull(checkLog);
			checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Debug, MessageTemplate.Text: "Saving data for {ResourceId} type {ResourceType}" });
			Assert.NotNull(checkLog);
			checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Information, MessageTemplate.Text: "Save metadata values for employee {Outcome} in {Elapsed:0.0} ms" });
			Assert.NotNull(checkLog);
			//audit
			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
				x.EventType == "SaveMetadataWithValueForResource"
				&& (string)x.CustomFields["SubType"] == "SaveData"
			);
			Assert.NotNull(checkAuditEvent);
		}

		[Fact]
		public async Task HandleAsync_Should_UpdateResourceValues()
		{
			await AddFakeData();
			// Arrange
			_handler = new SaveMetadataWithValueForResourceHandler(context,
				serilogLogger,
				_authorizationServiceMock.Object,
				_aclServiceMock.Object,
				_mediatorMock.Object);
			var command = new SaveMetadataWithValueForResource(fakeEmployeeId,
				ResourceType.Employee,
				[
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadata1", "newFakeValue1"),
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "fakeMetadata3", "newFakeValue3"),
					new SaveMetadataWithValueRequest(SaveMetadataAction.Update, "notExist", "notexistvalue")
				]);
			// Act
			await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
			var metadataResourceValue1 = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "fakeMetadata1");
			Assert.NotNull(metadataResourceValue1);
			Assert.Equal("newFakeValue1", metadataResourceValue1.Value);
			var metadataResourceValue3 = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "fakeMetadata3");
			Assert.NotNull(metadataResourceValue3);
			Assert.Equal("newFakeValue3", metadataResourceValue3.Value);
			var metadataResourceNotExist = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "notExist");
			Assert.Null(metadataResourceNotExist);

			//mediator
			_mediatorMock.Verify(x => x.Publish(
				It.Is<ResourceMetadataValueSavedEvent>(
					m =>
						m.ResourceOrganisationId == fakeOrganisationId
						&& m.ResourceType == ResourceType.Employee
						&& m.ResourceId == fakeEmployeeId
						&& m.ResourceMetadataValueSavedRecords.Length == 2
						&& m.ResourceMetadataValueSavedRecords.FirstOrDefault
						(l => l.MetadataId == "fakeMetadata1"
							  && l.Action == SaveMetadataActionType.Update
							  && l.NewValue == "newFakeValue1") != null
						&& m.ResourceMetadataValueSavedRecords.FirstOrDefault
						(l => l.MetadataId == "fakeMetadata3"
							  && l.Action == SaveMetadataActionType.Update
							  && l.NewValue == "newFakeValue3") != null
				)
				, CancellationToken.None), Times.Once);
			//logs
			var checkLog = _evts.FirstOrDefault(x =>
								x is { Level: LogEventLevel.Warning, MessageTemplate.Text: "Metadata {MetadataId} is not found or not allowed to update" });
			Assert.NotNull(checkLog);
			checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Debug, MessageTemplate.Text: "Update {Count} metadata values for {ResourceId} type {ResourceType}" });
			Assert.NotNull(checkLog);
			checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Debug, MessageTemplate.Text: "Saving data for {ResourceId} type {ResourceType}" });
			Assert.NotNull(checkLog);
			checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Information, MessageTemplate.Text: "Save metadata values for employee {Outcome} in {Elapsed:0.0} ms" });
			Assert.NotNull(checkLog);
			//audit
			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
				x.EventType == "SaveMetadataWithValueForResource"
				&& (string)x.CustomFields["SubType"] == "SaveData"
			);
			Assert.NotNull(checkAuditEvent);
		}

		[Fact]
		public async Task HandleAsync_Should_RemoveResourceValues()
		{
			await AddFakeData();
			// Arrange
			_handler = new SaveMetadataWithValueForResourceHandler(context,
				serilogLogger,
				_authorizationServiceMock.Object,
				_aclServiceMock.Object,
				_mediatorMock.Object);
			var command = new SaveMetadataWithValueForResource(fakeEmployeeId,
				ResourceType.Employee,
				[
					new SaveMetadataWithValueRequest(SaveMetadataAction.Remove, "fakeMetadata1", ""),
					new SaveMetadataWithValueRequest(SaveMetadataAction.Remove, "fakeMetadata3", ""),
					new SaveMetadataWithValueRequest(SaveMetadataAction.Remove, "notExist", "")
				]);
			// Act
			await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
			var metadataResourceValue1 = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "fakeMetadata1");
			Assert.Null(metadataResourceValue1);
			var metadataResourceValue3 = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "fakeMetadata3");
			Assert.Null(metadataResourceValue3);
			var metadataResourceNotExist = await context.MetadataResourceValues
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.ResourceId == fakeEmployeeId && x.MetadataId == "notExist");
			Assert.Null(metadataResourceNotExist);

			//mediator
			_mediatorMock.Verify(x => x.Publish(
				It.Is<ResourceMetadataValueSavedEvent>(
					m =>
						m.ResourceOrganisationId == fakeOrganisationId
						&& m.ResourceType == ResourceType.Employee
						&& m.ResourceId == fakeEmployeeId
						&& m.ResourceMetadataValueSavedRecords.Length == 2
						&& m.ResourceMetadataValueSavedRecords.FirstOrDefault
						(l => l.MetadataId == "fakeMetadata1"
							  && l.Action == SaveMetadataActionType.Remove
							  && l.NewValue == "") != null
						&& m.ResourceMetadataValueSavedRecords.FirstOrDefault
						(l => l.MetadataId == "fakeMetadata3"
							  && l.Action == SaveMetadataActionType.Remove
							  && l.NewValue == "") != null
				)
				, CancellationToken.None), Times.Once);
			//logs
			var checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Debug, MessageTemplate.Text: "Remove {Count} metadata values for {ResourceId} type {ResourceType}" });
			Assert.NotNull(checkLog);
			checkLog = _evts.FirstOrDefault(x =>
				x is { Level: LogEventLevel.Information, MessageTemplate.Text: "Save metadata values for employee {Outcome} in {Elapsed:0.0} ms" });
			Assert.NotNull(checkLog);
			//audit
			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
				x.EventType == "SaveMetadataWithValueForResource"
				&& (string)x.CustomFields["SubType"] == "SaveData"
			);
			Assert.NotNull(checkAuditEvent);
		}
	}

}
