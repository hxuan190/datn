﻿using Application.Commands.Groups;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.Services;
using Domain;
using Domain.Entities.Organisations;
using Domain.Events.Groups;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog;
using Audit.Core;
using Audit.Core.Providers;
using Util;
using Application.IntegrationTest.TestHelpers.Serilog;
using Serilog.Events;

namespace Application.IntegrationTest.Commands.Groups
{
	public class RemoveEmployeesFromAGroupTests
	{
		private readonly KensaDbContext _context;
		private readonly RemoveEmployeesFromAGroupHandler _handler;
		private readonly IUser _fakeUser = new FakeUser();
		private readonly Mock<IMediator> _mediatorMock;
		private readonly List<LogEvent> _evts = new();

		public RemoveEmployeesFromAGroupTests()
		{
			Configuration.Setup().UseInMemoryProvider();
			var userProvider = new Mock<IUserProvider>();
			userProvider.Setup(x => x.User).Returns(_fakeUser);
			_context = new InMemoryKensaDbContext(userProvider.Object);
			_context.Organisations.Add(new Organisation("fakeOrganisationId", "fakeEmployeeId", "fakeOrganisationName", "fakeAddress", null, null, null, null, null));
			_context.Groups.Add(new Group("fakeGroupId", "Test Group", "Test Description", false, "fakeOrganisationId"));
			_context.Employees.Add(new Employee("fakeEmployeeId1", "fakeOrganisationId", "fakeEmail1", "fakeFirstName1", "fakeLastName1"));
			_context.Employees.Add(new Employee("fakeEmployeeId2", "fakeOrganisationId", "fakeEmail2", "fakeFirstName2", "fakeLastName2"));
			_context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId", "fakeEmployeeId1"));
			_context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId", "fakeEmployeeId2"));
			_context.SaveChanges();

			var serilogLogger = new LoggerConfiguration()
				.MinimumLevel.Debug()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(
					_evts.Add))

				.CreateLogger();
			_mediatorMock = new Mock<IMediator>();
			Mock<IAuthorizationService> authorizationServiceMock = new();
			_handler = new RemoveEmployeesFromAGroupHandler(_context,
				serilogLogger,
				authorizationServiceMock.Object, _mediatorMock.Object);
		}

		[Fact]
		public async Task HandleAsync_ShouldThrowNotFoundException_IfGroupNotExist()
		{
			// Arrange
			var command = new RemoveEmployeesFromGroup(["1"], "not_exist_group_id");
			// Act
			var exception = await Assert.ThrowsAsync<NotFoundEntityException<RemoveEmployeesFromGroup>>(() =>
				_handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_GROUP, exception.ErrorCode);
			Assert.Equal("Not found entity when handling Application.Commands.Groups.RemoveEmployeesFromGroup. Detail: Not found group to remove employees", exception.Message);
		}

		[Fact]
		public async Task HandleAsync_UpdateEmployeeCredentialChanged_AfterProcess()
		{
			// Arrange
			var command = new RemoveEmployeesFromGroup(["fakeEmployeeId1", "fakeEmployeeId2"], "fakeGroupId");
			// Act
			await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			//Assert
			var employee1 = await _context.Employees
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.Id == "fakeEmployeeId1");
			Assert.NotNull(employee1);
			Assert.NotNull(employee1.LastCredentialUpdatedTime);
			var employee2 = await _context.Employees
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.Id == "fakeEmployeeId2");
			Assert.NotNull(employee2);
			Assert.NotNull(employee2.LastCredentialUpdatedTime);
		}

		[Fact]
		public async Task HandleAsync_RemoveLinkBetweenEmployeeAndGroup_AfterProcess()
		{
			// Arrange
			var command = new RemoveEmployeesFromGroup(["fakeEmployeeId1", "fakeEmployeeId2"], "fakeGroupId");
			// Act
			await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			//Assert
			var groupEmployee1 = await _context.GroupEmployees
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.EmployeeId == "fakeEmployeeId1" && x.GroupId == "fakeGroupId");
			Assert.Null(groupEmployee1);
			var groupEmployee2 = await _context.GroupEmployees
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.EmployeeId == "fakeEmployeeId2" && x.GroupId == "fakeGroupId");
			Assert.Null(groupEmployee2);
		}

		[Fact]
		public async Task HandleAsync_ShouldPublishEvent_WriteLog_And_Audit()
		{
			// Arrange
			var command = new RemoveEmployeesFromGroup(["fakeEmployeeId1", "fakeEmployeeId2"], "fakeGroupId");
			// Act
			await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
			_mediatorMock.Verify(x => x.Publish(
				It.Is<EmployeeRemovedFromGroupEvent>(
					m =>
						m.GroupId == "fakeGroupId"
						 && m.EmployeeRemovedEntities.FirstOrDefault
						  (l => l.OrganisationId == "fakeOrganisationId"
						 							&& l.EmployeeId == "fakeEmployeeId1"
						 							&& l.EmployeeEmail == "fakeEmail1") != null
						 && m.EmployeeRemovedEntities.FirstOrDefault
						 (l => l.OrganisationId == "fakeOrganisationId"
						 && l.EmployeeId == "fakeEmployeeId2"
						 && l.EmployeeEmail == "fakeEmail2") != null
						 )
				, CancellationToken.None), Times.Once);

			//audit
			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
				x.EventType == "RemoveEmployeesFromGroup"
				&& (string)x.CustomFields["SubType"] == "SaveData"
			);
			Assert.NotNull(checkAuditEvent);
			Assert.NotEmpty(_evts);
			var evt = _evts.FirstOrDefault(x => x.Level == LogEventLevel.Information);
			Assert.NotNull(evt);
			Assert.Equal("Remove employee(s) from a group {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
		}
	}
}
