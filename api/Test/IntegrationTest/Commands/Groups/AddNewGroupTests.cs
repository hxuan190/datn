﻿using Application.Commands.Groups;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Services;
using Audit.Core;
using Audit.Core.Providers;
using Domain;
using Domain.Entities.Organisations;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog;
using Serilog.Events;
using Util;

namespace Application.IntegrationTest.Commands.Groups
{
	public class AddNewGroupTests
	{
		private readonly KensaDbContext _context;
		private readonly IUserProvider _userProvider;
		private readonly IUser _fakeUser = new FakeUser();
		private readonly Mock<IMediator> _mediatorMock;
		private readonly AddNewGroupHandler _handler;
		private readonly List<LogEvent> _evts = new();

		public AddNewGroupTests()
		{
			Configuration.Setup().UseInMemoryProvider();
			_mediatorMock = new Mock<IMediator>();
			var userProviderMock = new Mock<IUserProvider>();
			userProviderMock.Setup(x => x.User).Returns(_fakeUser);
			_userProvider = userProviderMock.Object;

			_context = new InMemoryKensaDbContext(_userProvider);
			_context.Organisations.Add(new Organisation("fakeOrganisationId",
			"fakeEmployeeId",
			"fakeOrganisationName",
			"fakeAddress",
			null,
			null,
			null,
			null,
			null
			));
			_context.Employees.Add(new Employee("1", "fakeOrganisationId", "fakeEmail1@fakecompany.com", "fakeFirstName", "fakeLastName", ""));
			_context.Employees.Add(new Employee("2", "fakeOrganisationId", "fakeEmail2@fakecompany.com2", "fakeFirstName", "fakeLastName", ""));
			_context.SaveChanges();

			var serilogLogger = new LoggerConfiguration()
					  .MinimumLevel.Debug()
					  .WriteTo.Sink(new DelegateSink.DelegatingSink(
						  _evts.Add))

					  .CreateLogger();

			Mock<IAuthorizationService> authorizationServiceMock = new();
			_handler = new AddNewGroupHandler(
					_context,
					serilogLogger, authorizationServiceMock.Object, _mediatorMock.Object);
		}

		[Fact]
		public async Task HandleAsync_ShouldAddNewGroupToDatabase()
		{
			// Arrange
			var command = new AddNewGroup("Test Group", 0, "Test Description", []);
			var fakeUser = new FakeUser();

			// Act
			var result = await _handler.HandleAsync(command, fakeUser, CancellationToken.None);
			var addNewGroupResult = (AddNewGroupCommandResult)result;
			// Assert
			var group = _context.Groups
				.IgnoreQueryFilters()
				.FirstOrDefault(g => g.Id == addNewGroupResult.GroupId);
			Assert.NotNull(group);
			Assert.Equal("Test Group", group.Name);
			Assert.Equal("Test Description", group.Description);
			Assert.Equal(fakeUser.OrganisationId, group.OrganisationId);
			Assert.False(group.IsDefault);
			Assert.False(group.IsDeleted);
			//audit write record
			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
				 x.EventType == "AddNewGroup"
				 && (string)x.CustomFields["SubType"] == "SaveData"
			);
			Assert.NotNull(checkAuditEvent);
		}


		[Fact]
		public async Task HandleAsync_ShouldAddEmployeeToGroup()
		{
			// Arrange
			var command = new AddNewGroup("Test Group", 0, "Test Description", ["1", "2"]);
			var fakeUser = new FakeUser();

			// Act
			var result = await _handler.HandleAsync(command, fakeUser, CancellationToken.None);
			var addNewGroupResult = (AddNewGroupCommandResult)result;
			// Assert
			var group = _context.Groups
				.IgnoreQueryFilters()
				.FirstOrDefault(g => g.Id == addNewGroupResult.GroupId);
			var ge = _context.GroupEmployees.IgnoreQueryFilters()
				.Where(g => g.GroupId == addNewGroupResult.GroupId)
				.ToList();
			Assert.NotNull(group);
			Assert.NotNull(ge);
			Assert.Equal(2, ge.Count);
			Assert.True(ge.Exists(x => x.EmployeeId == "1"));
			Assert.True(ge.Exists(x => x.EmployeeId == "2"));
			Assert.Equal("Test Group", group.Name);
			Assert.Equal("Test Description", group.Description);
			Assert.Equal(fakeUser.OrganisationId, group.OrganisationId);
			Assert.False(group.IsDefault);
			Assert.False(group.IsDeleted);
			//audit write record
			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
							x.EventType == "AddNewGroup"
											&& (string)x.CustomFields["SubType"] == "SaveData"
													   );
			Assert.NotNull(checkAuditEvent);
			// check employee added to group
			var groupEmployee = _context.GroupEmployees
				.IgnoreQueryFilters()
				.Where(g => g.GroupId == addNewGroupResult.GroupId)
				.ToList();
			Assert.Equal(2, groupEmployee.Count);
		}

		[Fact]
		public async Task HandleAsync_ShouldThrowException_IfEmployeeDoesNotExist()
		{
			// Arrange
			var command = new AddNewGroup("Test Group", 0, "Test Description", ["1", "2", "3"]);
			var fakeUser = new FakeUser();

			// Act
			var ex = await Assert.ThrowsAsync<InvalidException<AddNewGroup>>(async () =>
			{
				await _handler.HandleAsync(command, fakeUser, CancellationToken.None);
			});

			// Assert
			Assert.Equal(ErrorCodes.APPLICATION_INVALID, ex.ErrorCode);
			Assert.Equal("Command Application.Commands.Groups.AddNewGroup is invalid to handle. Detail: There was invalid employeeId while adding along with the group", ex.Message);
		}

	}

}
