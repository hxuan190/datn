﻿using Application.Commands.Groups;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Services;
using Audit.Core;
using Audit.Core.Providers;
using Domain;
using Domain.Entities.Organisations;
using Domain.Events.Groups;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog;
using Serilog.Events;
using Util;

namespace Application.IntegrationTest.Commands.Groups
{
    public class DeleteGroupTests
    {
        private readonly KensaDbContext _context;
        private readonly IUserProvider _userProvider;
        private readonly IUser _fakeUser = new FakeUser();
        private readonly Mock<IMediator> _mediatorMock;
        private readonly DeleteGroupHandler _handler;
        private readonly List<LogEvent> _evts = new();
        public DeleteGroupTests()
        {
            Configuration.Setup().UseInMemoryProvider();
            _mediatorMock = new Mock<IMediator>();
            var userProviderMock = new Mock<IUserProvider>();
            userProviderMock.Setup(x => x.User).Returns(_fakeUser);
            _userProvider = userProviderMock.Object;
            
            _context = new InMemoryKensaDbContext(_userProvider);
            _context.Organisations.Add(new Organisation("fakeOrganisationId",
            "fakeEmployeeId",
            "fakeOrganisationName",
            "fakeAddress",
            null,
            null,
            null,
            null,
            null
            ));
            _context.Employees.Add(new Employee("fakeEmployeeId1", "fakeOrganisationId", "fakeEmail1@fakecompany.com", "fakeFirstName", "fakeLastName", ""));
            _context.Employees.Add(new Employee("fakeEmployeeId2", "fakeOrganisationId", "fakeEmail2@fakecompany.com2", "fakeFirstName", "fakeLastName", ""));
            _context.Groups.Add(new Group("fakeGroupId1", "fakeGroupName1", "fakeGroupDesc1", false, "fakeOrganisationId"));
            _context.Groups.Add(new Group("fakeGroupId2", "fakeGroupName2", "fakeGroupDesc2", false, "fakeOrganisationId"));
			_context.Groups.Add(new Group("fakeGroupId3", "fakeGroupName3", "fakeGroupDesc3", true, "fakeOrganisationId"));
            _context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId1", "fakeEmployeeId1"));
            _context.GroupEmployees.Add(new GroupEmployee("fakeOrganisationId", "fakeGroupId1", "fakeEmployeeId2"));
            _context.SaveChanges();

            var serilogLogger = new LoggerConfiguration()
                      .MinimumLevel.Debug()
                      .WriteTo.Sink(new DelegateSink.DelegatingSink(
                          _evts.Add))

                      .CreateLogger();

            Mock<IAuthorizationService> authorizationServiceMock = new();
            _handler = new DeleteGroupHandler(
                    _context,
                    serilogLogger,
                    authorizationServiceMock.Object,
                   _mediatorMock.Object);
        }

        // Write a test to remove a group and remove all employees from the group succesfully
        [Fact]
        public async Task HandleAsync_RemoveGroupAndRemoveAllEmployeesFromGroup()
        {
            // Arrange
            var command = new DeleteGroup("fakeGroupId1");
            // Act
            await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
            var group = await _context.Groups.FirstOrDefaultAsync(x => x.Id == "fakeGroupId1");
            Assert.Null(group);

            var groupEmployees = await _context.GroupEmployees.Where(x => x.GroupId == "fakeGroupId1").ToListAsync();
            Assert.Empty(groupEmployees);

            //audit write record
            var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
            var checkAuditEvent = auditEvents.FirstOrDefault(x =>
                 x.EventType == "DeleteGroup"
                 && (string)x.CustomFields["SubType"] == "SaveData"
            );
            Assert.NotNull(checkAuditEvent);
        }

        [Fact]
        public async Task HandleAsync_ShouldThrowNotFoundException_IfGroupNotExist()
        {
            // Arrange
            var command = new DeleteGroup("not_exist_group_id");
            // Act
            var exception = await Assert.ThrowsAsync<NotFoundEntityException<DeleteGroup>>(() =>
                _handler.HandleAsync(command, _fakeUser, CancellationToken.None));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_GROUP, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Commands.Groups.DeleteGroup. Detail: Not found group", exception.Message);
        }
        [Fact]
        public async Task HandleAsync_ShouldThrowInvalidException_IfGroupIsDefault()
        {
	        // Arrange
	        var command = new DeleteGroup("fakeGroupId3");
	        // Act
	        var exception = await Assert.ThrowsAsync<InvalidException<DeleteGroup>>(() =>
		        _handler.HandleAsync(command, _fakeUser, CancellationToken.None));
	        Assert.Equal(ErrorCodes.APPLICATION_GROUP_IS_DEFAULT, exception.ErrorCode);
	        Assert.Equal("Command Application.Commands.Groups.DeleteGroup is invalid to handle. Detail: Cannot delete default group", exception.Message);
        }

		[Fact]
        public async Task HandleAsync_UpdateEmployeeCredentialChanged_AfterProcess()
        {
            // Arrange
            var command = new DeleteGroup("fakeGroupId1");
            // Act
            await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
            //Assert
            var employee1 = await _context.Employees
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.Id == "fakeEmployeeId1");
            Assert.NotNull(employee1);
            Assert.NotNull(employee1.LastCredentialUpdatedTime);
            var employee2 = await _context.Employees
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.Id == "fakeEmployeeId2");
            Assert.NotNull(employee2);
            Assert.NotNull(employee2.LastCredentialUpdatedTime);
        }

        [Fact]
        public async Task HandleAsync_RemoveLinkBetweenEmployeeAndGroup_AfterProcess()
        {
            // Arrange
            var command = new DeleteGroup("fakeGroupId1");
            // Act
            await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
            //Assert
            var groupEmployee1 = await _context.GroupEmployees
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.EmployeeId == "fakeEmployeeId1" && x.GroupId == "fakeGroupId");
            Assert.Null(groupEmployee1);
            var groupEmployee2 = await _context.GroupEmployees
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.EmployeeId == "fakeEmployeeId2" && x.GroupId == "fakeGroupId");
            Assert.Null(groupEmployee2);
        }

        [Fact]
        public async Task HandleAsync_ShouldPublishEvent_WriteLog_And_Audit()
        {
            // Arrange
            var command = new DeleteGroup("fakeGroupId1");
            // Act
            await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
            // Assert
            _mediatorMock.Verify(x => x.Publish(
                It.Is<GroupDeletedEvent>(m => m.GroupId == "fakeGroupId1"), CancellationToken.None), Times.Once);

            //audit
            var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
            var checkAuditEvent = auditEvents.FirstOrDefault(x =>
                x.EventType == "DeleteGroup"
                && (string)x.CustomFields["SubType"] == "SaveData"
            );
            Assert.NotNull(checkAuditEvent);
            Assert.NotEmpty(_evts);
            var evt = _evts.FirstOrDefault(x => x.Level == LogEventLevel.Information);
            Assert.NotNull(evt);
            Assert.Equal("Delete group {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
    }

}
