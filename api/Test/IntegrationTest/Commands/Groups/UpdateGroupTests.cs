﻿using Application.Commands.Groups;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.Services;
using Audit.Core;
using Audit.Core.Providers;
using Domain;
using Domain.Entities.Organisations;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Util;
using Util.Extensions;

namespace Application.IntegrationTest.Commands.Groups
{
    public class UpdateGroupTests
    {
        private readonly KensaDbContext _context;
        private readonly IUserProvider _userProvider;
        private readonly IUser _fakeUser = new FakeUser();
        private readonly UpdateGroupHandler _handler;
        public UpdateGroupTests()
        {
            Configuration.Setup().UseInMemoryProvider();
            var userProviderMock = new Mock<IUserProvider>();
            userProviderMock.Setup(x => x.User).Returns(_fakeUser);
            _userProvider = userProviderMock.Object;
            _context = new InMemoryKensaDbContext(_userProvider);

            _context.Groups.Add(new Group("fakeGroupId1", "fakeGroupName1", "fakeGroupDesc1", false, "fakeOrganisationId"));
            _context.Groups.Add(new Group("fakeGroupId2", "fakeGroupName2", "fakeGroupDesc2", false, "fakeOrganisationId"));
            _context.Groups.Add(new Group("fakeGroupId3", "fakeGroupName3", "group default", true, "fakeOrganisationId"));
            _context.SaveChanges();

            _handler = new UpdateGroupHandler(
                _context,
                new Mock<Serilog.ILogger>().Object,
                new Mock<IAuthorizationService>().Object,
                new Mock<IMediator>().Object);
        }

        [Fact]
        public async Task HandleAsync_ShouldThrowNotFoundException_IfGroupNotExist()
        {
            // Arrange
            var command = new UpdateGroup("not_exist_group_id", "");
            // Act
            var exception = await Assert.ThrowsAsync<NotFoundEntityException<UpdateGroup>>(() =>
                _handler.HandleAsync(command, _fakeUser, CancellationToken.None));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_GROUP, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Commands.Groups.UpdateGroup. Detail: Not found group", exception.Message);
        }

        [Fact]
        public async Task HandleAsync_ShouldUpdateGroupFromDatabase()
        {
            // Arrange 
            var command = new UpdateGroup("fakeGroupId1", "Test Group Name", "Test Description");
            // Act
            await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
            // Assert
            var group = _context.Groups
                .IgnoreQueryFilters()
                .FirstOrDefault(g => g.Id == command.Id);

            Assert.NotNull(group);
            Assert.True(command.Id == group.Id);
            Assert.Equal("Test Group Name", group.Name);
            Assert.Equal("Test Description", group.Description);
            Assert.Equal(_fakeUser.OrganisationId, group.OrganisationId);
            //audit write record
            var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
            var checkAuditEvent = auditEvents.FirstOrDefault(x =>
                 x.EventType == "UpdateGroup"
                 && (string)x.CustomFields["SubType"] == "SaveData"
            );
            Assert.NotNull(checkAuditEvent);
        }

        [Fact]
        public async Task HandleAsync_DoNotUpdateGroup_IfGroupIsDefault()
        {
            // Arrange
            var command = new UpdateGroup("fakeGroupId3", "Update group default", "Group Description");
            // Act
            var result = await _handler.HandleAsync(command, _fakeUser, CancellationToken.None);
            // Assert
            Assert.False(result.Success);
            Assert.Equal(ErrorCodes.APPLICATION_GROUP_DATA_NOT_SAVED.GetDescription(), result.Error);
        }
    }
}
