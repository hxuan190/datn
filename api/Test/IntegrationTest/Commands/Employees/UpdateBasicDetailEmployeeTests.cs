﻿using Application.Commands.Employees;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.Services;
using Audit.Core;
using Audit.Core.Providers;
using Domain;
using Domain.Entities.Organisations;
using Domain.Events.Employees;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Util;

namespace Application.IntegrationTest.Commands.Employees
{
    public class UpdateBasicDetailEmployeeTests
	{
        private readonly KensaDbContext _context;
		private readonly IUser _fakeUser = new FakeUser();
        private readonly Mock<IMediator> _mediatorMock;
        public UpdateBasicDetailEmployeeTests()
        {
	        Configuration.Setup().UseInMemoryProvider();
			_mediatorMock = new Mock<IMediator>();
			var userProviderMock = new Mock<IUserProvider>();
	        userProviderMock.Setup(x => x.User).Returns(_fakeUser);
	        _context = new InMemoryKensaDbContext(userProviderMock.Object);
			_context.Organisations.Add(new Organisation("fakeOrganisationId",
				"fakeEmployeeId",
				"fakeOrganisationName",
				"fakeAddress",
				null,
				null,
				null,
				null,
				null
				));
			_context.Employees.Add(new Employee("fakeEmployeeId1", "fakeOrganisationId", "fakeEmail@fakecompany.com", "fakeFirstName", "fakeLastName"));
			_context.SaveChanges();
		}
		
		[Fact]
		public async Task HandleAsync_ThrowNotFoundEntity_IfNotExistEmployee()
		{
			// Arrange
			var handler = new UpdateBasicDetailEmployeeHandler(
				_context,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				new Mock<IMediator>().Object);

			var command = new UpdateBasicDetailEmployee(
				"fakeEmployeeId2", "fakeFirstNameUpdate", "fakeLastNameUpdate");
			// Act & Assert
			var exception = await Assert.ThrowsAsync<NotFoundEntityException<UpdateBasicDetailEmployee>>(() =>
				handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_EMPLOYEE, exception.ErrorCode);
			Assert.Equal("Not found entity when handling Application.Commands.Employees.UpdateBasicDetailEmployee. Detail: Employee not found to update basic detail", exception.Message);
		}
		
		[Fact]
		public async Task HandleAsync_UpdateEmployeeDetail_AndCredentialDate()
		{
			// Arrange
			var handler = new UpdateBasicDetailEmployeeHandler(
				_context,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				new Mock<IMediator>().Object);

			var command = new UpdateBasicDetailEmployee(
				"fakeEmployeeId1", "fakeFirstNameUpdate", "fakeLastNameUpdate");
			// Act
			await handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// & Assert
			var checkEmployee = await _context.Employees
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.Id == "fakeEmployeeId1");
			Assert.NotNull(checkEmployee); 
			Assert.Equal("fakeFirstNameUpdate", checkEmployee.FirstName);
			Assert.Equal("fakeLastNameUpdate", checkEmployee.LastName);
			Assert.NotNull(checkEmployee.LastCredentialUpdatedTime);
		}


		[Fact]
		public async Task HandleAsync_PublishEvent_And_Audit()
		{
			var handler = new UpdateBasicDetailEmployeeHandler(
				_context,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				_mediatorMock.Object);

			var command = new UpdateBasicDetailEmployee(
				"fakeEmployeeId1", "fakeFirstNameUpdate", "fakeLastNameUpdate");
			// Act
			await handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
			_mediatorMock.Verify(x =>
				x.Publish(It.Is<EmployeeUpdatedBasicDetailEvent>(
					 m =>
							m.EmployeeId == "fakeEmployeeId1"
							  && m.EmployeeOrganisationId == "fakeOrganisationId"
							  && m.EmployeeEmail == "fakeEmail@fakecompany.com"
				), CancellationToken.None), Times.Once);

			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
				x.EventType == "UpdateBasicDetailEmployee"
				&& (string)x.CustomFields["SubType"] == "SaveData"
			);
			Assert.NotNull(checkAuditEvent);
		}
	}
}
