﻿using Application.Commands.Employees;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.Services;
using Audit.Core;
using Audit.Core.Providers;
using Domain;
using Domain.Entities.Duende;
using Domain.Entities.Organisations;
using Domain.Events.Employees;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Serilog;
using Util;

namespace Application.IntegrationTest.Commands.Employees
{
    public class SoftDeleteEmployeeTests
	{
        private readonly KensaDbContext _context;
        private readonly SoftDeleteEmployeeHandler _handler;
        private readonly IUser _fakeUser = new FakeUser();
        private readonly Mock<IMediator> _mediatorMock;
        public SoftDeleteEmployeeTests()
        {
	        Configuration.Setup().UseInMemoryProvider();
			var userProvider = new Mock<IUserProvider>();
            userProvider.Setup(x => x.User).Returns(_fakeUser);
            _context = new InMemoryKensaDbContext(userProvider.Object);
            var serverSideSessionAudits = new ServerSideSessionWithAudit[]
            {
                new()
                {
                    SessionId = "fakeSessionId1",
                    SubjectId = "fakeEmployeeId1",
                    InitialIpAddress = "fakeInitialIpAddress1",
                    UserAgent = "fakeInitialUserAgent1",
                    Renewed = new DateTime(2021, 1, 2),
                    Expires = new DateTime(2021, 1, 10),
                    OrganisationId = "fakeOrganisationId",
                    Created = new DateTime(2021, 1, 1),
                    Key = "fakeKey1",
                    Scheme = "fakeScheme1",
                    Data = "fakeData1"
                },
                new()
                {
                    SessionId = "fakeSessionId2",
                    SubjectId = "fakeEmployeeId1",
                    InitialIpAddress = "fakeInitialIpAddress2",
                    UserAgent = "fakeInitialUserAgent2",
                    Renewed = new DateTime(2021, 1, 3),
                    Expires = new DateTime(2021, 1, 11),
                    OrganisationId = "fakeOrganisationId2",
                    Created = new DateTime(2021, 1, 2),
                    Key = "fakeKey2",
                    Scheme = "fakeScheme2",
                    Data = "fakeData2"
                }
            };
            _context.ServerSideSessions.AddRange(serverSideSessionAudits);
            _context.Employees.Add(new Employee("fakeEmployeeId1", "fakeOrganisationId", "fakeEmail", "fakeFirstName", "fakeLastName", ""));
            _context.SaveChanges();
            Mock<ILogger> logger = new();
            _mediatorMock = new Mock<IMediator>();
            Mock <IAuthorizationService> authorizationServiceMock = new();
            _handler = new SoftDeleteEmployeeHandler(_context,
                logger.Object,
                authorizationServiceMock.Object,
                _mediatorMock.Object);
        }
        [Fact]
        public async Task HandleAsync_ThrowNotFoundEntity_IfNullEmployee()
        {
            // Arrange
            var command = new SoftDeleteEmployee(
                "notExistEmployeeId");
            // Act & Assert
            var exception = await Assert.ThrowsAsync<NotFoundEntityException<SoftDeleteEmployee>>(() =>
                                               _handler.HandleAsync(command, _fakeUser, CancellationToken.None));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_EMPLOYEE, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Commands.Employees.SoftDeleteEmployee. Detail: Not found employee to soft-delete", exception.Message);

            //exist employee but in wrong org
            command = new SoftDeleteEmployee(
                "fakeEmployeeId2");
            // Act & Assert
            exception = await Assert.ThrowsAsync<NotFoundEntityException<SoftDeleteEmployee>>(() =>
                _handler.HandleAsync(command, _fakeUser, CancellationToken.None));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_EMPLOYEE, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Commands.Employees.SoftDeleteEmployee. Detail: Not found employee to soft-delete", exception.Message);
        }

        [Fact]
        public async Task HandleAsync_ShouldRemoveSessionsOfEmployee()
        {
            // Arrange
            var removeSessionId = "fakeSessionId1";
            var command = new SoftDeleteEmployee(
                "fakeEmployeeId1");
            var fakeUser = new FakeUser();
            // Act
            var result = await _handler.HandleAsync(command, fakeUser, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            var remainSessions = await _context.ServerSideSessions
                .IgnoreQueryFilters()
                .Where(x => x.SessionId == removeSessionId)
                .ToArrayAsync();
            Assert.Empty(remainSessions);
		}

        [Fact]
        public async Task HandleAsync_ShouldUpdateLastCredentialOfEmployee()
        {
            // Arrange
            var command = new SoftDeleteEmployee(
				"fakeEmployeeId1");
			var fakeUser = new FakeUser();
            // Act
            var result = await _handler.HandleAsync(command, fakeUser, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            var employee = await _context.Employees
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.Id == "fakeEmployeeId1");
            Assert.True(employee!.IsDeleted);
            Assert.NotNull(employee!.LastCredentialUpdatedTime);
        }

        [Fact]
        public async Task HandleAsync_ShouldPublishEvent_And_Audit()
        {
			// Arrange
			var command = new SoftDeleteEmployee(
				"fakeEmployeeId1");
			var fakeUser = new FakeUser();
            // Act
            var result = await _handler.HandleAsync(command, fakeUser, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            _mediatorMock.Verify(x => x.Publish(It.Is<EmployeeSoftDeletedEvent>(
                    m => m.EmployeeId == "fakeEmployeeId1"
                              && m.EmployeeOrganisationId == "fakeOrganisationId" 
                              && m.EmployeeEmail == "fakeEmail"
                )
                , CancellationToken.None), Times.Once);
            //assert audit
            var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
            var checkAuditEvent = auditEvents.FirstOrDefault(x =>
					x.EventType == "SoftDeleteEmployee"
					&& (string)x.CustomFields["SubType"] == "SaveData"
			);
            Assert.NotNull(checkAuditEvent);
		}
    }

}
