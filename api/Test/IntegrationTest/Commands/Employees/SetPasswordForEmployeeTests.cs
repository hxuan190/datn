﻿using Application.Commands.Employees;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Services;
using Audit.Core;
using Audit.Core.Providers;
using Domain;
using Domain.Entities.Duende;
using Domain.Entities.Organisations;
using Domain.Events.Employees;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Serilog;
using Serilog.Events;
using Util;

namespace Application.IntegrationTest.Commands.Employees
{
    public class SetPasswordForEmployeeTests
	{
        private readonly KensaDbContext _context;
        private readonly IUserProvider _userProvider;
		private readonly IUser _fakeUser = new FakeUser();
        private readonly Mock<IMediator> _mediatorMock;
        public SetPasswordForEmployeeTests()
        {
	        Configuration.Setup().UseInMemoryProvider();
			_mediatorMock = new Mock<IMediator>();
			var userProviderMock = new Mock<IUserProvider>();
	        userProviderMock.Setup(x => x.User).Returns(_fakeUser);
	        _userProvider = userProviderMock.Object;
			_context = new InMemoryKensaDbContext(_userProvider);
			_context.Organisations.Add(new Organisation("fakeOrganisationId",
				"fakeEmployeeId",
				"fakeOrganisationName",
				"fakeAddress",
				null,
				null,
				null,
				null,
				null
				));
			_context.Employees.Add(new Employee("fakeEmployeeId1", "fakeOrganisationId", "fakeEmail", "fakeFirstName", "fakeLastName", ""));
			_context.Employees.Add(new Employee("fakeEmployeeId2", "fakeOrganisationId2", "fakeEmail", "fakeFirstName", "fakeLastName", ""));
			var serverSideSessionAudits = new ServerSideSessionWithAudit[]
			{
				new()
				{
					SessionId = "fakeSessionId1",
					SubjectId = "fakeEmployeeId1",
					InitialIpAddress = "fakeInitialIpAddress1",
					UserAgent = "fakeInitialUserAgent1",
					Renewed = new DateTime(2021, 1, 2),
					Expires = new DateTime(2021, 1, 10),
					OrganisationId = "fakeOrganisationId",
					Created = new DateTime(2021, 1, 1),
					Key = "fakeKey1",
					Scheme = "fakeScheme1",
					Data = "fakeData1"
				},
				new()
				{
					SessionId = "fakeSessionId2",
					SubjectId = "fakeEmployeeId1",
					InitialIpAddress = "fakeInitialIpAddress2",
					UserAgent = "fakeInitialUserAgent2",
					Renewed = new DateTime(2021, 1, 3),
					Expires = new DateTime(2021, 1, 11),
					OrganisationId = "fakeOrganisationId2",
					Created = new DateTime(2021, 1, 2),
					Key = "fakeKey2",
					Scheme = "fakeScheme2",
					Data = "fakeData2"
				}
			};
			_context.ServerSideSessions.AddRange(serverSideSessionAudits);
			_context.SaveChanges();
		}
		[Fact]
        public async Task HandleAsync_ThrowNotFoundEntity_IfRevokeSessionOfNullEmployee()
        {
            // Arrange
            var passwordHasher = new Mock<IPasswordHasher<Employee>>();
            var userManager = new Mock<UserManager<Employee>>(
	            new Mock<IUserStore<Employee>>().Object,
	            new Mock<IOptions<IdentityOptions>>().Object,
	            passwordHasher.Object,
	            new IUserValidator<Employee>[0],
	            new IPasswordValidator<Employee>[0],
	            new Mock<ILookupNormalizer>().Object,
	            new Mock<IdentityErrorDescriber>().Object,
	            new Mock<IServiceProvider>().Object,
	            new Mock<ILogger<UserManager<Employee>>>().Object);

            var handler = new SetPasswordForEmployeeHandler(
	            userManager.Object,
	            _context,
	            passwordHasher.Object,
	            new Mock<Serilog.ILogger>().Object,
	            new Mock<IMediator>().Object,
	            new Mock<IAuthorizationService>().Object);

            var command = new SetPasswordForEmployee(
                "notExistEmployeeId",
                "fakePassword");
            // Act & Assert
            var exception = await Assert.ThrowsAsync<NotFoundEntityException<SetPasswordForEmployee>>(() =>
	            handler.HandleAsync(command, _fakeUser, CancellationToken.None));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_EMPLOYEE, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Commands.Employees.SetPasswordForEmployee. Detail: Not found employee to set password", exception.Message);

            //exist employee but in wrong org
            command = new SetPasswordForEmployee(
                "fakeEmployeeId2",
                "fakePassword");
			// Act & Assert
			exception = await Assert.ThrowsAsync<NotFoundEntityException<SetPasswordForEmployee>>(() =>
				handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_EMPLOYEE, exception.ErrorCode);
			Assert.Equal("Not found entity when handling Application.Commands.Employees.SetPasswordForEmployee. Detail: Not found employee to set password", exception.Message);

		}
		[Fact]
		public async Task HandleAsync_ThrowInvalid_IfPasswordIsNotStrong()
		{
			// Arrange
			List<LogEvent> evts = new List<LogEvent>();
			var serilogLogger = new LoggerConfiguration()
				.MinimumLevel.Debug()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(
					evts.Add))
				
				.CreateLogger();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			var passwordValidator = new Mock<IPasswordValidator<Employee>>();
			passwordValidator.Setup(x => x.ValidateAsync(It.IsAny<UserManager<Employee>>(), It.IsAny<Employee>(), It.IsAny<string>()))
				.ReturnsAsync(IdentityResult.Failed(new IdentityError { Code = "PasswordNotStrong", Description = "Password is not strong" }));

			//passwordValidators.
			IPasswordValidator<Employee>[] passwordValidators =
				new[] { passwordValidator.Object };
			
			var userManager = new Mock<UserManager<Employee>>(
				new Mock<IUserStore<Employee>>().Object,
				new Mock<IOptions<IdentityOptions>>().Object,
				passwordHasher.Object,
				new IUserValidator<Employee>[0],
				passwordValidators,
				new Mock<ILookupNormalizer>().Object,
				new Mock<IdentityErrorDescriber>().Object,
				new Mock<IServiceProvider>().Object,
				new Mock<ILogger<UserManager<Employee>>>().Object);

			var handler = new SetPasswordForEmployeeHandler(
				userManager.Object,
				_context,
				passwordHasher.Object,
				serilogLogger,
				new Mock<IMediator>().Object,
				new Mock<IAuthorizationService>().Object);

			var command = new SetPasswordForEmployee(
				"fakeEmployeeId1",
				"fakePassword");
			// Act & Assert
			var exception = await Assert.ThrowsAsync<InvalidException<SetPasswordForEmployee>>(() =>
				handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			Assert.Equal(ErrorCodes.APPLICATION_INVALID_OLD_PASSWORD_INCORRECT, exception.ErrorCode);
			Assert.Equal("Command Application.Commands.Employees.SetPasswordForEmployee is invalid to handle. Detail: New password is invalid to set", exception.Message);
			Assert.NotEmpty(evts);
			var evt = evts.FirstOrDefault(x => x.Level == LogEventLevel.Debug);
			Assert.NotNull(evt);
			Assert.Equal("User password validation failed: {errors}.",evt.MessageTemplate.Text);
		}
		[Fact]
		public async Task HandleAsync_UpdateEmployee_IfPasswordIsStrong()
		{
			// Arrange
			List<LogEvent> evts = new List<LogEvent>();
			var serilogLogger = new LoggerConfiguration()
				.MinimumLevel.Debug()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(
					evts.Add))
				.CreateLogger();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			passwordHasher.Setup(x=> x.HashPassword(It.IsAny<Employee>(), It.IsAny<string>()))
				.Returns("fakeHashedPassword");
			var passwordValidator = new Mock<IPasswordValidator<Employee>>();
			passwordValidator.Setup(x => 
					x.ValidateAsync(It.IsAny<UserManager<Employee>>(), 
						It.IsAny<Employee>(), It.IsAny<string>()))
				.ReturnsAsync(IdentityResult.Success);
			IPasswordValidator<Employee>[] passwordValidators =
				new[] { passwordValidator.Object };
			var userManager = new Mock<UserManager<Employee>>(
				new Mock<IUserStore<Employee>>().Object,
				new Mock<IOptions<IdentityOptions>>().Object,
				passwordHasher.Object,
				new IUserValidator<Employee>[0],
				passwordValidators,
				new Mock<ILookupNormalizer>().Object,
				new Mock<IdentityErrorDescriber>().Object,
				new Mock<IServiceProvider>().Object,
				new Mock<ILogger<UserManager<Employee>>>().Object);

			var handler = new SetPasswordForEmployeeHandler(
				userManager.Object,
				_context,
				passwordHasher.Object,
				serilogLogger,
				new Mock<IMediator>().Object,
				new Mock<IAuthorizationService>().Object);

			var command = new SetPasswordForEmployee(
				"fakeEmployeeId1",
				"fakePassword");
			// Act
			await handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
			var employee = await _context.Employees
				.IgnoreQueryFilters()
				.FirstOrDefaultAsync(x => x.Id == "fakeEmployeeId1");
			Assert.NotNull(employee);
			Assert.Equal("fakeHashedPassword",employee.PasswordHash);
			Assert.NotNull(employee.LastCredentialUpdatedTime);
			Assert.NotNull(employee.SecurityStamp); 
			Assert.NotNull(employee.ConcurrencyStamp);
		}
		[Fact]
		public async Task HandleAsync_RevokeAllSessions()
		{
			// Arrange
			List<LogEvent> evts = new List<LogEvent>();
			var serilogLogger = new LoggerConfiguration()
				.MinimumLevel.Debug()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(
					evts.Add))
				.CreateLogger();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			passwordHasher.Setup(x => x.HashPassword(It.IsAny<Employee>(), It.IsAny<string>()))
				.Returns("fakeHashedPassword");
			var passwordValidator = new Mock<IPasswordValidator<Employee>>();
			passwordValidator.Setup(x =>
					x.ValidateAsync(It.IsAny<UserManager<Employee>>(),
						It.IsAny<Employee>(), It.IsAny<string>()))
				.ReturnsAsync(IdentityResult.Success);
			IPasswordValidator<Employee>[] passwordValidators =
				new[] { passwordValidator.Object };
			var userManager = new Mock<UserManager<Employee>>(
				new Mock<IUserStore<Employee>>().Object,
				new Mock<IOptions<IdentityOptions>>().Object,
				passwordHasher.Object,
				new IUserValidator<Employee>[0],
				passwordValidators,
				new Mock<ILookupNormalizer>().Object,
				new Mock<IdentityErrorDescriber>().Object,
				new Mock<IServiceProvider>().Object,
				new Mock<ILogger<UserManager<Employee>>>().Object);

			var handler = new SetPasswordForEmployeeHandler(
				userManager.Object,
				_context,
				passwordHasher.Object,
				serilogLogger,
				new Mock<IMediator>().Object,
				new Mock<IAuthorizationService>().Object);

			var command = new SetPasswordForEmployee(
				"fakeEmployeeId1",
				"fakePassword");
			// Act
			await handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
			var sessions = await _context.ServerSideSessions
				.IgnoreQueryFilters()
				.Where(x => x.SubjectId == "fakeEmployeeId1")
				.ToListAsync();
			Assert.Empty(sessions);
		}
		[Fact]
		public async Task HandleAsync_PublishEvent_And_Audit()
		{
			// Arrange
			List<LogEvent> evts = new List<LogEvent>();
			var serilogLogger = new LoggerConfiguration()
				.MinimumLevel.Debug()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(
					evts.Add))
				.CreateLogger();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			passwordHasher.Setup(x => x.HashPassword(It.IsAny<Employee>(), It.IsAny<string>()))
				.Returns("fakeHashedPassword");
			var passwordValidator = new Mock<IPasswordValidator<Employee>>();
			passwordValidator.Setup(x =>
					x.ValidateAsync(It.IsAny<UserManager<Employee>>(),
						It.IsAny<Employee>(), It.IsAny<string>()))
				.ReturnsAsync(IdentityResult.Success);
			IPasswordValidator<Employee>[] passwordValidators =
				new[] { passwordValidator.Object };
			var userManager = new Mock<UserManager<Employee>>(
				new Mock<IUserStore<Employee>>().Object,
				new Mock<IOptions<IdentityOptions>>().Object,
				passwordHasher.Object,
				new IUserValidator<Employee>[0],
				passwordValidators,
				new Mock<ILookupNormalizer>().Object,
				new Mock<IdentityErrorDescriber>().Object,
				new Mock<IServiceProvider>().Object,
				new Mock<ILogger<UserManager<Employee>>>().Object);

			var handler = new SetPasswordForEmployeeHandler(
				userManager.Object,
				_context,
				passwordHasher.Object,
				serilogLogger,
				_mediatorMock.Object,
				new Mock<IAuthorizationService>().Object);

			var command = new SetPasswordForEmployee(
				"fakeEmployeeId1",
				"fakePassword");
			// Act
			await handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// Assert
			_mediatorMock.Verify(x => 
				x.Publish(It.Is<EmployeePasswordIsForceSetEvent>(
					 m =>  m.EmployeeId == "fakeEmployeeId1"
							  && m.EmployeeOrganisationId == "fakeOrganisationId"
							  && m.EmployeeOrganisationName == "fakeOrganisationName"
							  && m.EmployeeEmail == "fakeEmail"
							  && m.EmployeeFirstName == "fakeFirstName"
							  && m.NewPassword == "fakePassword"
				)
				, CancellationToken.None), Times.Once);
			//audit write record
			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
					x.EventType == "SetPasswordForEmployee"
					&& (string)x.CustomFields["SubType"] == "SaveData"
			);
			Assert.NotNull(checkAuditEvent);
		}
	}
}
