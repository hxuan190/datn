﻿using Application.Commands.Employees;
using Application.Exceptions;
using Application.IntegrationTest.TestHelpers;
using Application.Services;
using Audit.Core;
using Audit.Core.Providers;
using Domain;
using Domain.Entities.Organisations;
using Domain.Events.Employees;
using Infrastructure.Persistence;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using Util;

namespace Application.IntegrationTest.Commands.Employees
{
    public class AddNewEmployeeTests
	{
        private readonly KensaDbContext _context;
        private readonly IUserProvider _userProvider;
		private readonly IUser _fakeUser = new FakeUser();
        private readonly Mock<IMediator> _mediatorMock;
        public AddNewEmployeeTests()
        {
	        Configuration.Setup().UseInMemoryProvider();
			_mediatorMock = new Mock<IMediator>();
			var userProviderMock = new Mock<IUserProvider>();
	        userProviderMock.Setup(x => x.User).Returns(_fakeUser);
	        _userProvider = userProviderMock.Object;
			_context = new InMemoryKensaDbContext(_userProvider);
			_context.Organisations.Add(new Organisation("fakeOrganisationId",
				"fakeEmployeeId",
				"fakeOrganisationName",
				"fakeAddress",
				null,
				null,
				null,
				null,
				null
				));
			_context.Employees.Add(new Employee("fakeEmployeeId1", "fakeOrganisationId", "fakeEmail@fakecompany.com", "fakeFirstName", "fakeLastName", ""));
			_context.Employees.Add(new Employee("fakeEmployeeId2", "fakeOrganisationId2", "fakeEmail@fakecompany.com2", "fakeFirstName", "fakeLastName", ""));
			_context.Groups.Add(new Group("fakeGroupId1", "fakeGroupName1", "fakeGroupDesc1", false, "fakeOrganisationId", 0));
			_context.SaveChanges();
		}

        private async Task AddFakeDataForMetadata()
        {
			//metadata
			var fakeOrganisationId = "fakeOrganisationId";
			_context.MetadataCategories.Add(new MetadataCategory("fakeMetadataCategory1", fakeOrganisationId, "fakeMetadataCategory1", 1));
			_context.MetadataCategories.Add(new MetadataCategory("fakeMetadataCategory2", fakeOrganisationId, "fakeMetadataCategory2", 2));
			_context.Metadatas.Add(new Metadata("fakeMetadata1", fakeOrganisationId, "fakeMetadata1", MetadataDataType.Number, ResourceType.Employee, true, "fakeMetadataCategory1", 1));
			_context.Metadatas.Add(new Metadata("fakeMetadata2", fakeOrganisationId, "fakeMetadata2", MetadataDataType.Text, ResourceType.Employee, false, "fakeMetadataCategory1", 2));
	        _context.Metadatas.Add(new Metadata("fakeMetadata3", fakeOrganisationId, "fakeMetadata3", MetadataDataType.Number, ResourceType.Employee, true, "fakeMetadataCategory2", 1));
	        _context.Metadatas.Add(new Metadata("fakeMetadata4", fakeOrganisationId, "fakeMetadata4", MetadataDataType.Text, ResourceType.Employee, true, "", 1, true));
	        _context.MetadataPredefinedValues.Add(new MetadataPredefinedValue("fakeMetadataPredefinedValue1", fakeOrganisationId, "fakeMetadata2", "value1"));
	        _context.MetadataPredefinedValues.Add(new MetadataPredefinedValue("fakeMetadataPredefinedValue2", fakeOrganisationId, "fakeMetadata2", "value2"));
	        await _context.SaveChangesAsync();
        }
		[Fact]
        public async Task HandleAsync_ThrowConflictEntity_IfExistEmployee()
        {
            // Arrange
            var passwordHasher = new Mock<IPasswordHasher<Employee>>();
            var handler = new AddNewEmployeeHandler(
	            _context,
	            passwordHasher.Object,
	            new Mock<Serilog.ILogger>().Object,
	            new Mock<IAuthorizationService>().Object,
				new Mock<IMediator>().Object);

			var command = new AddNewEmployee("fakeEmail@fakecompany.com", "fakeFirstName", "", new[] { "fakeGroupId" }, []);

			// Act & Assert
			var exception = await Assert.ThrowsAsync<ConflictEntityException<AddNewEmployee>>(() =>
	            handler.HandleAsync(command, _fakeUser, CancellationToken.None));
            Assert.Equal(ErrorCodes.APPLICATION_CONFLICT_EMPLOYEE_ALREADY_EXIST, exception.ErrorCode);
            Assert.Equal("Conflict entity when handling Application.Commands.Employees.AddNewEmployee. Detail: Employee already exists while adding new employee", exception.Message);
		}
		[Fact]
		public async Task HandleAsync_ThrowNotFoundEntity_IfNotExistGroup()
		{
			// Arrange
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			var handler = new AddNewEmployeeHandler(
				_context,
				passwordHasher.Object,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				new Mock<IMediator>().Object);

			var command = new AddNewEmployee("fakeEmail2@fakecompany.com", "fakeFirstName", "", new[] { "fakeGroupId" }, []);
			// Act & Assert
			var exception = await Assert.ThrowsAsync<NotFoundEntityException<AddNewEmployee>>(() =>
				handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_GROUP, exception.ErrorCode);
			Assert.Equal("Not found entity when handling Application.Commands.Employees.AddNewEmployee. Detail: Group not found while adding new employee", exception.Message);
		}
		[Fact]
		public async Task HandleAsync_ThrowNotFoundEntity_IfNotExistOrg()
		{
			// Arrange
			var o = _context.Organisations.FirstOrDefault();
			_context.Organisations.Remove(o);
			await _context.SaveChangesAsync();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			var handler = new AddNewEmployeeHandler(
				_context,
				passwordHasher.Object,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				new Mock<IMediator>().Object);

			var command = new AddNewEmployee("fakeEmail2@fakecompany.com", "fakeFirstName", "fakeLastName", Array.Empty<string>(), []);
			// Act & Assert
			var exception = await Assert.ThrowsAsync<NotFoundEntityException<AddNewEmployee>>(() =>
				handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_ORGANISATION, exception.ErrorCode);
			Assert.Equal("Not found entity when handling Application.Commands.Employees.AddNewEmployee. Detail: Organisation not found while adding new employee", exception.Message);
		}
		[Fact]
		public async Task HandleAsync_ThrowInvalidException_IfMissingRequiredMetadata()
		{
			// Arrange
			await AddFakeDataForMetadata();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			var handler = new AddNewEmployeeHandler(
				_context,
				passwordHasher.Object,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				new Mock<IMediator>().Object);

			var command = new AddNewEmployee("fakeEmail2@fakecompany.com", "fakeFirstName", "fakeLastName", Array.Empty<string>(), []);
			// Act & Assert
			var exception = await Assert.ThrowsAsync<InvalidException<AddNewEmployee>>(() =>
				handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			Assert.Equal(ErrorCodes.APPLICATION_INVALID, exception.ErrorCode);
			Assert.Equal("Command Application.Commands.Employees.AddNewEmployee is invalid to handle. Detail: Missing required metadata when adding new employee", exception.Message);
		}
		[Fact]
		public async Task HandleAsync_ThrowInvalidException_IfPredefinedMetadataValueIsWrong()
		{
			// Arrange
			await AddFakeDataForMetadata();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			var handler = new AddNewEmployeeHandler(
				_context,
				passwordHasher.Object,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				new Mock<IMediator>().Object);

			var command = new AddNewEmployee("fakeEmail2@fakecompany.com",
				"fakeFirstName", "fakeLastName",
				Array.Empty<string>(), 
				[new EmployeeMetadataRequest("fakeMetadata4", "val4"),
					new EmployeeMetadataRequest("fakeMetadata2", "val_not_exist")]);
			// Act & Assert
			var exception = await Assert.ThrowsAsync<InvalidException<AddNewEmployee>>(() =>
				handler.HandleAsync(command, _fakeUser, CancellationToken.None));
			Assert.Equal(ErrorCodes.APPLICATION_INVALID, exception.ErrorCode);
			Assert.Equal("Command Application.Commands.Employees.AddNewEmployee is invalid to handle. Detail: Metadata is not allowed to have free text but input wrong predefined value", exception.Message);
		}
		[Fact]
		public async Task HandleAsync_Add_New_Employee_And_Groups_And_Metadata()
		{
			// Arrange
			await AddFakeDataForMetadata();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			passwordHasher.Setup(x=>x.HashPassword(It.IsAny<Employee>(), It.IsAny<string>()))
				.Returns("fakeHashedPassword");
			var handler = new AddNewEmployeeHandler(
				_context,
				passwordHasher.Object,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				new Mock<IMediator>().Object);

			var command = new AddNewEmployee("fakeEmail2@fakecompany.com",
				"fakeFirstName", 
				"fakeLastName", 
				new[] { "fakeGroupId1" },
				[new EmployeeMetadataRequest("fakeMetadata4", "val4"),
				new EmployeeMetadataRequest("fakeMetadata2", "fakeMetadataPredefinedValue1")]);
			// Act
			await handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			// & Assert
			var checkEmployee = await _context.Employees
				.FirstOrDefaultAsync(x => x.Email == "fakeEmail2@fakecompany.com");
			Assert.NotNull(checkEmployee);
			//assert first name ,last name, password hash, security stamp, concurrency stamp, 
			Assert.Equal("fakeFirstName", checkEmployee.FirstName);
			Assert.Equal("fakeLastName", checkEmployee.LastName);
			Assert.Equal("fakeHashedPassword", checkEmployee.PasswordHash);
			Assert.NotNull(checkEmployee.SecurityStamp);
			Assert.NotNull(checkEmployee.ConcurrencyStamp);
			var checkGroupEmployees = await _context.GroupEmployees
				.Where(x => x.EmployeeId == checkEmployee.Id)
				.ToListAsync();
			Assert.NotEmpty(checkGroupEmployees);
			var checkGroupEmployee = checkGroupEmployees.FirstOrDefault();
			Assert.NotNull(checkGroupEmployee);
			Assert.Equal("fakeGroupId1", checkGroupEmployee.GroupId);

			var checkMetadataValue1 = await _context.MetadataResourceValues
				.FirstOrDefaultAsync(x => x.ResourceType == ResourceType.Employee
				                          && x.ResourceId == checkEmployee.Id
				                          && x.MetadataId == "fakeMetadata4");
			Assert.NotNull(checkMetadataValue1);
			Assert.Equal("val4", checkMetadataValue1.Value);
			var checkMetadataValue2 = await _context.MetadataResourceValues
				.FirstOrDefaultAsync(x => x.ResourceType == ResourceType.Employee
				                          && x.ResourceId == checkEmployee.Id
				                          && x.MetadataId == "fakeMetadata2");
			Assert.NotNull(checkMetadataValue2);
			Assert.Equal("fakeMetadataPredefinedValue1", checkMetadataValue2.Value);
		}

		[Fact]
		public async Task HandleAsync_PublishEvent_And_Audit()
		{
			// Arrange
			await AddFakeDataForMetadata();
			var passwordHasher = new Mock<IPasswordHasher<Employee>>();
			passwordHasher.Setup(x => x.HashPassword(It.IsAny<Employee>(), It.IsAny<string>()))
				.Returns("fakeHashedPassword");
			var handler = new AddNewEmployeeHandler(
				_context,
				passwordHasher.Object,
				new Mock<Serilog.ILogger>().Object,
				new Mock<IAuthorizationService>().Object,
				_mediatorMock.Object);

			var command = new AddNewEmployee("fakeEmail2@fakecompany.com",
				"fakeFirstName",
				"fakeLastName",
				new[] { "fakeGroupId1" },
				[new EmployeeMetadataRequest("fakeMetadata4", "val4"),
					new EmployeeMetadataRequest("fakeMetadata2", "fakeMetadataPredefinedValue1")]);

			// Act
			var result = await handler.HandleAsync(command, _fakeUser, CancellationToken.None);
			var newEmployeeId = ((AddNewEmployeeCommandResult)result).EmployeeId;
			// Assert
			_mediatorMock.Verify(x =>
				x.Publish(It.Is<EmployeeAddedEvent>(
					 m => 
							m.EmployeeId == newEmployeeId
							  && m.EmployeeOrganisationId == "fakeOrganisationId"
							  && m.EmployeeOrganisationName == "fakeOrganisationName"
							  && m.EmployeeEmail == "fakeEmail2@fakecompany.com"
							  && m.EmployeeFirstName == "fakeFirstName"
							&& m.MetadataValues[0].MetadataId == "fakeMetadata4"
							&& m.MetadataValues[0].Value == "val4"
							&& m.MetadataValues[1].MetadataId == "fakeMetadata2"
							&& m.MetadataValues[1].Value == "fakeMetadataPredefinedValue1"
				), CancellationToken.None), Times.Once);

			var auditEvents = ((Configuration.DataProvider as InMemoryDataProvider)!).GetAllEvents();
			var checkAuditEvent = auditEvents.FirstOrDefault(x =>
				x.EventType == "AddNewEmployee"
				&& (string)x.CustomFields["SubType"] == "SaveData"
			);
			Assert.NotNull(checkAuditEvent);
		}
	}
}
