﻿using Domain;
using Moq;
using Serilog;
using Application.IntegrationTest.TestHelpers;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Queries.Employees;
using Application.Services;
using Serilog.Events;
using Domain.Entities.Duende;

namespace Application.IntegrationTest.Queries.Employees    
{
    public class GetEmployeeSessionByEmployeeIdTests
    {
        private ServerSideSessionWithAudit _serverSideSessionAudit = new ServerSideSessionWithAudit
        {
            SessionId = "fakeSessionId",
            SubjectId = "fakeEmployeeId",
            InitialIpAddress = "fakeInitialIpAddress",
            UserAgent = "fakeInitialUserAgent",
            Renewed = new DateTime(2021, 1, 2),
            Expires = new DateTime(2021, 1, 10),
            OrganisationId = "fakeOrganisationId1",
            Created = new DateTime(2021, 1, 1),
            Key = "fakeKey",
            Scheme = "fakeScheme",
            Data = "fakeData"
        };
        [Fact]
        public async Task ReturnEmptyArray_IfFindWrongEmployeeId()
        {
            //arrange
            var newUserProvider = new Mock<IUserProvider>();
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            context.ServerSideSessions.Add(_serverSideSessionAudit);
            await context.SaveChangesAsync();
            Mock<ILogger> logger = new();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetEmployeeSessionsByEmployeeIdExecutor(logger.Object, context, mockAuthorizationService.Object);
            var query = new GetEmployeeSessionsByEmployeeId("fakeEmployeeId1");
            var fakeUser = new FakeUser();
            //act & assert
            var result = await executor.ExecuteAsync(query, fakeUser);
            Assert.Empty(result!);
        }
        [Fact]
        public async Task ReturnEmptyArray_IfFindEmployee_NotBelongToCurrentOrg()
        {
            //arrange
            var newUserProvider = new Mock<IUserProvider>();
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            context.ServerSideSessions.Add(_serverSideSessionAudit);
            await context.SaveChangesAsync();
            Mock<ILogger> logger = new();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetEmployeeSessionsByEmployeeIdExecutor(logger.Object, context, mockAuthorizationService.Object);
            var query = new GetEmployeeSessionsByEmployeeId("fakeEmployeeId");
            var fakeUser = new FakeUser();
            //act & assert
            var result = await executor.ExecuteAsync(query, fakeUser);
            Assert.Empty(result!);
        }
        [Fact]
        public async Task ReturnCorrectData_IfFindEmployee_BelongToCurrentOrg()
        {
            //arrange
            var newUserProvider = new Mock<IUserProvider>();
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            var serverSideSessionAudit2 = new ServerSideSessionWithAudit
            {
                SessionId = "fakeSessionId",
                SubjectId = "fakeEmployeeId",
                InitialIpAddress = "fakeInitialIpAddress",
                UserAgent = "fakeInitialUserAgent",
                Renewed = new DateTime(2021, 1, 2),
                Expires = new DateTime(2021, 1, 10),
                OrganisationId = "fakeOrganisationId",
                Created = new DateTime(2021, 1, 1),
                Key = "fakeKey",
                Scheme = "fakeScheme",
                Data = "fakeData"
            };
            serverSideSessionAudit2.SetInitPlace("fakeInitialPlace");
            serverSideSessionAudit2.SetLastPlace("fakeLastPlace");
            context.ServerSideSessions.Add(_serverSideSessionAudit);
            context.ServerSideSessions.Add(serverSideSessionAudit2);
            await context.SaveChangesAsync();
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetEmployeeSessionsByEmployeeIdExecutor(serilogLogger, context, mockAuthorizationService.Object);
            var query = new GetEmployeeSessionsByEmployeeId("fakeEmployeeId");
            var fakeUser = new FakeUser();
            //act & assert
            var result = await executor.ExecuteAsync(query, fakeUser);
            Assert.NotEmpty(result!);
            var firstSession = result![0];
            //assert properties of first Session
            Assert.Equal("fakeSessionId", firstSession.SessionId);
            Assert.Equal(new DateTime(2021, 1, 1), firstSession.Created);
            Assert.Equal(new DateTime(2021, 1, 2), firstSession.Renewed);
            Assert.Equal(new DateTime(2021, 1, 10), firstSession.Expires);
            Assert.Equal("fakeInitialUserAgent", firstSession.UserAgent);
            Assert.Equal("fakeInitialIpAddress", firstSession.InitialIpAddress);
            Assert.Equal("fakeInitialPlace", firstSession.InitialPlace);
            Assert.Equal("fakeLastPlace", firstSession.LastUsedPlace);
            Assert.Null(firstSession.LastUsedIpAddress);
            Assert.Null(firstSession.Device);
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Information, evt.Level);
            Assert.Equal("Retrieve sessions of employee by Id {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
    }
}
