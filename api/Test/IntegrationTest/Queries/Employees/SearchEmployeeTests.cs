﻿using Domain;
using Moq;
using Serilog;
using Application.IntegrationTest.TestHelpers;
using Domain.Entities.Organisations;
using Application.Queries.Employees;
using Application.Services;
using Serilog.Events;
using Application.IntegrationTest.TestHelpers.Serilog;
using Infrastructure.Persistence;
using Serilog.Core;
using Application.Exceptions;
using Util;


namespace Application.IntegrationTest.Queries.Employees
{
	public class SearchEmployeeTests
	{
		private KensaDbContext context;
		private readonly List<LogEvent> _evts = new();
		private readonly IUser fakeUser = new FakeUser();
		private readonly Logger serilogLogger;
		private readonly Mock<IAuthorizationService> _authorizationServiceMock;

		public SearchEmployeeTests()
		{
			var newUserProvider = new Mock<IUserProvider>();
			newUserProvider.Setup(x => x.User).Returns(fakeUser);
			context = new InMemoryKensaDbContext(newUserProvider.Object);
			context.Employees.Add(new Employee("employeeId1", fakeUser.OrganisationId, "ALice@fakecompany.com", "Alice", "Johnson"));
			context.Employees.Add(new Employee("employeeId2", fakeUser.OrganisationId, "bob@fakecompany.com", "Bob", "SmIth"));
			context.Employees.Add(new Employee("employeeId3", fakeUser.OrganisationId, "charlie@fakecompany.com", "Charlie", ""));
			context.Employees.Add(new Employee("employeeId4", fakeUser.OrganisationId, "saM@fakecompany.com", "", "Brown"));
			context.SaveChanges();
			_authorizationServiceMock = new Mock<IAuthorizationService>();
			serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => _evts.Add(e)))
				.CreateLogger();
		}

		[Fact]
		public async Task HandleAsync_ShouldThrowException_If_Keyword_IsNullOrEmpty()
		{

			//act & assert
			var executor = new SearchEmployeesExecutor(serilogLogger,
				context,
				_authorizationServiceMock.Object);
			var ex = await Assert.ThrowsAsync<InvalidException<SearchEmployees>>(async () =>
			{
				await executor.ExecuteAsync(new SearchEmployees(""), fakeUser, CancellationToken.None);
			});
			// Assert
			Assert.Equal(ErrorCodes.APPLICATION_INVALID, ex.ErrorCode);
			Assert.Equal("Command Application.Queries.Employees.SearchEmployees is invalid to handle. Detail: Keyword must be provided to search employees", ex.Message);
			ex = await Assert.ThrowsAsync<InvalidException<SearchEmployees>>(async () =>
			{
				await executor.ExecuteAsync(new SearchEmployees(null!), fakeUser, CancellationToken.None);
			});
			// Assert
			Assert.Equal(ErrorCodes.APPLICATION_INVALID, ex.ErrorCode);
			Assert.Equal("Command Application.Queries.Employees.SearchEmployees is invalid to handle. Detail: Keyword must be provided to search employees", ex.Message);
		}
		[Fact]
		public async Task HandleAsync_ShouldReturnsEmployee_With_Paging()
		{
			//arrange
			//act & assert
			var executor = new SearchEmployeesExecutor(serilogLogger,
			 context,
			 _authorizationServiceMock.Object);
			var result = await executor.ExecuteAsync(new SearchEmployees("i", 0, 2), fakeUser);
			Assert.NotNull(result);
			Assert.Equal(3, result.Total);
			Assert.Equal(2, result.Employees.Length);
			result = await executor.ExecuteAsync(new SearchEmployees("i", 2, 2), fakeUser);
			Assert.NotNull(result);
			Assert.Equal(3, result.Total);
			Assert.Equal(1, result.Employees.Length);
		}
		[Fact]
		public async Task HandleAsync_ShouldReturnsEmployee_With_LowerKeyword()
		{
			//arrange
			//act & assert
			var executor = new SearchEmployeesExecutor(serilogLogger,
				context,
				_authorizationServiceMock.Object);
			var result = await executor.ExecuteAsync(new SearchEmployees("li"), fakeUser);
			Assert.NotNull(result);
			Assert.Equal(2, result.Total);
			Assert.Equal(2, result.Employees.Length);
			result = await executor.ExecuteAsync(new SearchEmployees("bo"), fakeUser);
			Assert.Equal(1, result!.Total);
			result = await executor.ExecuteAsync(new SearchEmployees("fakecompany"), fakeUser);
			Assert.Equal(4, result!.Total);
		}
	}
}
