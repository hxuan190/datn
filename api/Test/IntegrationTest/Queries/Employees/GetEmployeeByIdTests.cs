﻿using Application.Exceptions;
using Domain;
using Moq;
using Serilog;
using Application.IntegrationTest.TestHelpers;
using Domain.Entities.Organisations;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Queries.Employees;
using Application.Services;
using Serilog.Events;
using Util;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Util.Extensions;

namespace Application.IntegrationTest.Queries.Employees    
{
    public class GetEmployeeByIdTests
    {
        private readonly IDistributedCache _distributedCache;

        public GetEmployeeByIdTests()
        {
            var cacheOptions = Options.Create(new MemoryDistributedCacheOptions());
            _distributedCache = new MemoryDistributedCache(cacheOptions);
        }
        [Fact]
        public async Task ShouldThrowNotFoundEntityException_WhenEmployeeDoesNotExist()
        {
            //arrange
            var newUserProvider = new Mock<IUserProvider>();
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            Mock<ILogger> logger = new();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetEmployeeByIdExecutor(logger.Object, context, _distributedCache, mockAuthorizationService.Object);
            var query = new GetEmployeeById("nonExistentEmployeeId");
            var fakeUser = new FakeUser();
            //act & assert
            var exception = await Assert.ThrowsAsync<NotFoundEntityException<GetEmployeeById>>(() =>
                executor.ExecuteAsync(query, fakeUser));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_EMPLOYEE, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Queries.Employees.GetEmployeeById. Detail: Not found employee", exception.Message);
        }
        [Fact]
        public async Task ShouldThrowNotFoundEntityException_WhenEmployeeExist_ButAccessByDifferentOrgId()
        {
            //arrange
            var newUserProvider = new Mock<IUserProvider>();
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            context.Employees.Add(new Employee("fakeEmployeeId","tempId","fakeEmail","fakeFirstName","fakeLastName",""));
            await context.SaveChangesAsync();
            Mock<ILogger> logger = new();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetEmployeeByIdExecutor(logger.Object, context, _distributedCache, mockAuthorizationService.Object);
            var query = new GetEmployeeById("fakeEmployeeId");
            var fakeUser = new FakeUser();
            //act & assert
            var exception = await Assert.ThrowsAsync<NotFoundEntityException<GetEmployeeById>>(() =>
                executor.ExecuteAsync(query, fakeUser));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_EMPLOYEE, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Queries.Employees.GetEmployeeById. Detail: Not found employee", exception.Message);
        }
        [Fact]
        public async Task ShouldReturnEmployee_WhenEmployeeExist_AnddAccessByCorrectOrg()
        {
            //arrange
            var fakeUser = new FakeUser();
            var fakeEmployeeId = "fakeEmployeeId";
            var newUserProvider = new Mock<IUserProvider>();
            newUserProvider.Setup(x => x.User)
                .Returns(fakeUser);
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            context.Employees.Add(new Employee(fakeEmployeeId, fakeUser.OrganisationId,
                "fakeEmail", "fakeFirstName", "fakeLastName", ""));
            await context.SaveChangesAsync();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            var executor = new GetEmployeeByIdExecutor(serilogLogger, 
                context, 
                _distributedCache, 
                mockAuthorizationService.Object);
            var query = new GetEmployeeById(fakeEmployeeId);

            //act & assert
            var result = await executor.ExecuteAsync(query, fakeUser);
            Assert.NotNull(result);
            Assert.Equal(fakeEmployeeId, result.Id);
            Assert.Equal("fakeEmail", result.Email);
            Assert.Equal("fakeFirstName", result.FirstName);
            Assert.Equal("fakeLastName", result.LastName);
            Assert.False(result.IsDeleted);
            //log
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Information, evt.Level);
            Assert.Equal("Retrieve employee by Id {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
        [Fact]
        public async Task ShouldReturnEmployee_FromCache()
        {
            //arrange
            var fakeUser = new FakeUser();
            var fakeEmployee = new Employee("fakeEmployeeId", fakeUser.OrganisationId,
                "fakeEmail", "fakeFirstName", "fakeLastName", "");
            await _distributedCache.Set(
                CacheKeys.EMPLOYEE_BY_ORG_ID_AND_EMPLOYEE_ID(fakeUser.OrganisationId,fakeEmployee.Id),
                fakeEmployee,
                60);
            var newUserProvider = new Mock<IUserProvider>();
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            Mock<IAuthorizationService> mockAuthorizationService = new();
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            var executor = new GetEmployeeByIdExecutor(serilogLogger,
                context,
                _distributedCache,
                mockAuthorizationService.Object);
            var query = new GetEmployeeById("fakeEmployeeId");
            
            //act & assert
            var result = await executor.ExecuteAsync(query, fakeUser);
            Assert.NotNull(result);
            Assert.Equal("fakeEmployeeId", result.Id);
            Assert.Equal("fakeEmail", result.Email);
            Assert.Equal("fakeFirstName", result.FirstName);
            Assert.Equal("fakeLastName", result.LastName);
            Assert.False(result.IsDeleted);
            //log
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Information, evt.Level);
            Assert.Equal("Retrieve employee by Id {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
    }
}
