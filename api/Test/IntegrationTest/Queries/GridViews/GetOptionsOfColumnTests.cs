﻿using Application.IntegrationTest.TestHelpers.Serilog;
using Application.IntegrationTest.TestHelpers;
using Domain;
using Moq;
using Serilog.Events;
using Serilog;
using Application.Services;
using Application.Queries.GridViews;
using Domain.Entities.Organisations;

namespace Application.IntegrationTest.Queries.GridViews
{
	public class GetOptionsOfColumnTests
	{
		[Theory]
		[MemberData(nameof(GetDefaultColumnsData_ForEmployee))]
		public async Task Handler_Should_Return_Options_For_Default_Column_Of_Employees(List<Employee> listEmployees,
			string defaultColumnId,
			string searchKeyword,
			ColumnOptionResult[] expectedResult,
			int expectedTotal)
		{
			//arrange
			var fakeUser = new FakeUser();
			var fakeEmployeeId = "fakeEmployeeId";
			var newUserProvider = new Mock<IUserProvider>();
			newUserProvider.Setup(x => x.User)
				.Returns(fakeUser);
			var context = new InMemoryKensaDbContext(newUserProvider.Object);
			context.Employees.AddRange(listEmployees);
			await context.SaveChangesAsync();
			Mock<IAuthorizationService> mockAuthorizationService = new();
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var executor = new GetOptionsOfColumnExecutor(serilogLogger,
				context,
				mockAuthorizationService.Object);
			var query = new GetOptionsOfColumn(ResourceType.Employee, defaultColumnId, keyword: searchKeyword);

			//act & assert
			var result = await executor.ExecuteAsync(query, fakeUser);
			Assert.NotNull(result);
			Assert.Equal(expectedTotal, result.Total);
			//assert all value in result will appear in the  expectedResult
			foreach (var column in expectedResult)
			{
				Assert.Contains(result.Data, x => x.Value == column.Value);
			}
			//assert all display in result will appear in the  expectedResult
			foreach (var column in expectedResult)
			{
				Assert.Contains(result.Data, x => x.Display == column.Display);
			}
			//log
			Assert.NotNull(evt);
			Assert.Equal(LogEventLevel.Information, evt.Level);
			Assert.Equal("Retrieve options of default column {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
		}
		/// <summary>
		/// its for Employee entity, we need to add more later
		/// </summary>
		public static TheoryData<List<Employee>, string, string, ColumnOptionResult[], int> GetDefaultColumnsData_ForEmployee
		{
			get
			{
				var listEmployees = new List<Employee>
				{
					new Employee("id1", "fakeOrganisationId", "fakeEmail1@xxx.com", "fakeFirstName1", "fakeLastName1"),
					new Employee("id2", "fakeOrganisationId", "fakeEmail2@xxx.com", "fakeFirstName1", "fakeLastName2")
				};
				return new()
				{
					{   listEmployees, ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_EMAIL, "",[
						new ColumnOptionResult("fakeEmail1@xxx.com"),
						new ColumnOptionResult("fakeEmail2@xxx.com")
						],2
					},
					{   listEmployees, ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_FIRST_NAME, "",[
							new ColumnOptionResult("fakeFirstName1")
						],1
					},
					{   listEmployees, ResourceTypeColumnHelpers.EMPLOYEE_COLUMN_LAST_NAME,"1", [
							new ColumnOptionResult("fakeLastName1")
						],1
					}
				};
			}
		}

		[Theory]
		[MemberData(nameof(GetMetadataColumnsData_ForEmployee))]
		public async Task Handler_Should_Return_Options_For_Metadata_Column_Of_Employees(
			List<Employee> listEmployees,
			List<Metadata> listMetadata,
			List<MetadataPredefinedValue> listMedataMetadataPredefinedValues,
			List<MetadataResourceValue> listMetadataResourceValues,
			string metadataColumnId,
			string searchKeyword,
			ColumnOptionResult[] expectedResult,
			int expectedTotal)
		{
			//arrange
			var fakeUser = new FakeUser();
			var fakeEmployeeId = "fakeEmployeeId";
			var newUserProvider = new Mock<IUserProvider>();
			newUserProvider.Setup(x => x.User)
				.Returns(fakeUser);
			var context = new InMemoryKensaDbContext(newUserProvider.Object);
			context.Employees.AddRange(listEmployees);
			context.Metadatas.AddRange(listMetadata);
			context.MetadataPredefinedValues.AddRange(listMedataMetadataPredefinedValues);
			context.MetadataResourceValues.AddRange(listMetadataResourceValues);
			await context.SaveChangesAsync();

			Mock<IAuthorizationService> mockAuthorizationService = new();
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var executor = new GetOptionsOfColumnExecutor(serilogLogger,
				context,
				mockAuthorizationService.Object);
			var query = new GetOptionsOfColumn(ResourceType.Employee, metadataColumnId, keyword: searchKeyword);

			//act & assert
			var result = await executor.ExecuteAsync(query, fakeUser);
			Assert.NotNull(result);
			Assert.Equal(expectedTotal, result.Total);
			//assert all value in result will appear in the  expectedResult
			foreach (var column in expectedResult)
			{
				Assert.Contains(result.Data, x => x.Value == column.Value);
			}
			//assert all display in result will appear in the  expectedResult
			foreach (var column in expectedResult)
			{
				Assert.Contains(result.Data, x => x.Display == column.Display);
			}
			//log
			Assert.NotNull(evt);
			Assert.Equal(LogEventLevel.Information, evt.Level);
			Assert.Equal("Retrieve options of metadata column {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
		}
		/// <summary>
		/// its for Employee entity, we need to add more later
		/// </summary>
		public static TheoryData<List<Employee>, List<Metadata>, List<MetadataPredefinedValue>, List<MetadataResourceValue>, string, string, ColumnOptionResult[], int> GetMetadataColumnsData_ForEmployee
		{
			get
			{
				var listEmployees = new List<Employee>
				{
					new Employee("id1", "fakeOrganisationId", "fakeEmail1@xxx.com", "fakeFirstName1", "fakeLastName1"),
					new Employee("id2", "fakeOrganisationId", "fakeEmail2@xxx.com", "fakeFirstName1", "fakeLastName2")
				};
				var listMetadata = new List<Metadata>
				{
					new Metadata("metadata1", "fakeOrganisationId", "fakeName1",
												MetadataDataType.Text, ResourceType.Employee, true, null),
					new Metadata("metadata2", "fakeOrganisationId", "fakeName2",
												MetadataDataType.Text, ResourceType.Employee, false, null)
				};
				var listMetadataPredefinedValues = new List<MetadataPredefinedValue>
				{
					new MetadataPredefinedValue("meta_predefined_id1","fakeOrganisationId", "metadata2","meta_predefined_val1"),
					new MetadataPredefinedValue("meta_predefined_id2","fakeOrganisationId", "metadata2", "meta_predefined_val2")
				};
				var listMetadataResourceValues = new List<MetadataResourceValue>
				{
					new MetadataResourceValue("fakeOrganisationId",ResourceType.Employee,"id1", "metadata1", "metadata_val1"),
					new MetadataResourceValue("fakeOrganisationId",ResourceType.Employee,"id2", "metadata1", "metadata_val2"),
					new MetadataResourceValue("fakeOrganisationId",ResourceType.Employee,"id1", "metadata2", "meta_predefined_id1"),
					new MetadataResourceValue("fakeOrganisationId",ResourceType.Employee,"id2", "metadata2", "meta_predefined_id2")
				};

				return new()
				{
					{   listEmployees, listMetadata,listMetadataPredefinedValues,listMetadataResourceValues,"metadata1", "",
						[
							new ColumnOptionResult("metadata_val1"),
							new ColumnOptionResult("metadata_val2")
						],2
					},
					{   listEmployees, listMetadata,listMetadataPredefinedValues,listMetadataResourceValues,"metadata1", "2",
						[
							new ColumnOptionResult("metadata_val2")
						],1
					},
					{   listEmployees, listMetadata,listMetadataPredefinedValues,listMetadataResourceValues,"metadata2", "",
						[
							new ColumnOptionResult("meta_predefined_id1","meta_predefined_val1"),
							new ColumnOptionResult("meta_predefined_id2","meta_predefined_val2")
						],2
					},
					{   listEmployees, listMetadata,listMetadataPredefinedValues,listMetadataResourceValues,"metadata2", "2",
						[
							new ColumnOptionResult("meta_predefined_id2","meta_predefined_val2")
						],1
					}
				};
			}
		}

	}
}
