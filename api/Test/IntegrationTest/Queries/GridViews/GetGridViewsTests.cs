﻿using Application.IntegrationTest.TestHelpers.Serilog;
using Application.IntegrationTest.TestHelpers;
using Domain;
using Moq;
using Serilog.Events;
using Serilog;
using Application.Services;
using Application.Queries.GridViews;
using Domain.Entities.Organisations;

namespace Application.IntegrationTest.Queries.GridViews
{
	public class GetGridViewsTests
	{
		private LogEvent evt;
		private readonly IUser _fakeUser = new BaseUser("fakeOrganisationId", "fakeEmail1", "fakeEmployeeId1");
		private readonly GetGridViewsExecutor _executor;

		public GetGridViewsTests()
		{
			//arrange
			var newUserProvider = new Mock<IUserProvider>();
			newUserProvider.Setup(x => x.User)
				.Returns(_fakeUser);
			var context = new InMemoryKensaDbContext(newUserProvider.Object);
			//scenario:
			//employee 1 creates a view 1 for employees
			//employee 2 creates a view 2 for employees
			//employee 3 creates a view 3 for employees
			//employee 2 shared view 2 to employee 1
			//employee 3 shared view 3 to group 1 (contain employee 1 & employee 2)
			context.Organisations.Add(FakeData.FakeOrganisation);
			context.Employees.Add(new Employee(_fakeUser.Id, _fakeUser.OrganisationId, _fakeUser.Email, "fakeFirstName", "fakeLastname"));
			context.Employees.Add(new Employee("fakeEmployeeId2", _fakeUser.OrganisationId, "fakeEmail2", "fakeFirstName2", "fakeLastname2"));
			context.Employees.Add(new Employee("fakeEmployeeId3", _fakeUser.OrganisationId, "fakeEmail3", "fakeFirstName3", "fakeLastname3"));

			context.Groups.Add(new Group("fakeGroupId1", "fakeGroup1", "fakeGroup1", false, _fakeUser.OrganisationId));
			context.Groups.Add(new Group("fakeGroupId2", "fakeGroup2", "fakeGroup2", false, _fakeUser.OrganisationId));
			context.GroupEmployees.Add(new GroupEmployee(_fakeUser.OrganisationId, "fakeGroupId1", _fakeUser.Id));
			context.GroupEmployees.Add(new GroupEmployee(_fakeUser.OrganisationId, "fakeGroupId1", "fakeEmployeeId2"));

			var gridView1 = new GridView("fakeGridViewId1", "fakeGridView1", "fakeGridStructure1",
				_fakeUser.OrganisationId, ResourceType.Employee);
			gridView1.MarkAsCreated(_fakeUser.Id);
			var gridView2 = new GridView("fakeGridViewId2", "fakeGridView2", "fakeGridStructure2",
				_fakeUser.OrganisationId, ResourceType.Employee);
			gridView2.MarkAsCreated("fakeEmployeeId2");
			var gridView3 = new GridView("fakeGridViewId3", "fakeGridView3", "fakeGridStructure3",
				_fakeUser.OrganisationId, ResourceType.Employee);
			gridView3.MarkAsCreated("fakeEmployeeId3");
			context.GridViews.AddRange(new List<GridView>()
			{
				gridView1, gridView2, gridView3
			});

			context.GridViewSharings.AddRange(new List<GridViewSharing>()
			{
				new GridViewSharing(_fakeUser.OrganisationId, "fakeGridViewId2", _fakeUser.Id),
				new GridViewSharing(_fakeUser.OrganisationId, "fakeGridViewId3", "fakeGroupId1")
			});
			context.SaveChanges();
			Mock<IAuthorizationService> mockAuthorizationService = new();
			evt = null;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			_executor = new GetGridViewsExecutor(serilogLogger, context, mockAuthorizationService.Object);
		}

		[Fact]
		public async Task Handler_Should_Return_Correct_GridViews()
		{
			//act & assert
			var _query = new GetGridViews(ResourceType.Employee);
			
			Assert.True(true);
			var result = await _executor.ExecuteAsync(_query, _fakeUser);
			Assert.NotNull(result);
			//self created
			var selfCreatedView = result.FirstOrDefault(x => x.Id == "fakeGridViewId1");
			Assert.NotNull(selfCreatedView); 
			Assert.True(selfCreatedView.Own);
			//shared directly by someone
			var sharedDirectlyView = result.FirstOrDefault(x => x.Id == "fakeGridViewId2");
			Assert.NotNull(sharedDirectlyView);
			Assert.False(sharedDirectlyView.Own);
			//shared to group of current user
			var sharedViaGroupView = result.FirstOrDefault(x => x.Id == "fakeGridViewId3");
			Assert.NotNull(sharedViaGroupView);
			Assert.False(sharedViaGroupView.Own);
			//log
			Assert.NotNull(evt);
			Assert.Equal(LogEventLevel.Information, evt.Level);
			Assert.Equal("Retrieve available grid views for {resourceType} {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
		}
		
	}
}
