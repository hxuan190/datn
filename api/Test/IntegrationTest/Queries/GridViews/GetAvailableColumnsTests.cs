﻿using Application.IntegrationTest.TestHelpers.Serilog;
using Application.IntegrationTest.TestHelpers;
using Domain;
using Moq;
using Serilog.Events;
using Serilog;
using Application.Services;
using Application.Queries.GridViews;
using Domain.Entities.Organisations;

namespace Application.IntegrationTest.Queries.GridViews
{
	public class GetAvailableColumnsTests
	{
		[Fact]
		public async Task Handler_Should_Return_DefaultColumns_FromEmployees()
		{
			//arrange
			var fakeUser = new FakeUser();
			var fakeEmployeeId = "fakeEmployeeId";
			var newUserProvider = new Mock<IUserProvider>();
			newUserProvider.Setup(x => x.User)
				.Returns(fakeUser);
			var context = new InMemoryKensaDbContext(newUserProvider.Object);

			Mock<IAuthorizationService> mockAuthorizationService = new();
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var executor = new GetAvailableColumnsExecutor(serilogLogger,
				context,
				mockAuthorizationService.Object);
			var query = new GetAvailableColumns(ResourceType.Employee);

			//act & assert
			var defaultColumnsOfResourceEmployee = ResourceType.Employee.GetDefaultColumnsOfResources();
			var result = await executor.ExecuteAsync(query, fakeUser);
			Assert.NotNull(result);
			Assert.Equal(defaultColumnsOfResourceEmployee.Length, result.Length);
			//assert all in column Id in defaultColumnsOfResourceEmployee will appear in the result
			foreach (var column in defaultColumnsOfResourceEmployee)
			{
				Assert.Contains(result, x => x.Id == column.Id);
			}

			//log
			Assert.NotNull(evt);
			Assert.Equal(LogEventLevel.Information, evt.Level);
			Assert.Equal("Retrieve available columns for a resource type {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
		}
		[Fact]
		public async Task Handler_Should_Return_MetadataColumn_FromEmployees()
		{
			//arrange
			var fakeUser = new FakeUser();
			var fakeEmployeeId = "fakeEmployeeId";
			var newUserProvider = new Mock<IUserProvider>();
			newUserProvider.Setup(x => x.User)
				.Returns(fakeUser);
			var listMetadata = new List<Metadata>
			{
				new Metadata("id1", "fakeOrganisationId", "fakeName1",
					MetadataDataType.Text, ResourceType.Employee, true, null),
				new Metadata("id2", "fakeOrganisationId", "fakeName2",
					MetadataDataType.Text, ResourceType.Employee, true, null)
			};
			var context = new InMemoryKensaDbContext(newUserProvider.Object);
			context.Metadatas.AddRange(listMetadata);
			await context.SaveChangesAsync();

			Mock<IAuthorizationService> mockAuthorizationService = new();
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var executor = new GetAvailableColumnsExecutor(serilogLogger,
				context,
				mockAuthorizationService.Object);
			var query = new GetAvailableColumns(ResourceType.Employee);

			//act & assert
			var result = await executor.ExecuteAsync(query, fakeUser);
			Assert.NotNull(result);
			var metadataColumns = result.Where(p => !p.IsDefaultColumn).ToList();

			Assert.Equal(listMetadata.Count, metadataColumns.Count);
			//assert all in column Id in metadata  will appear in the metadataColumns
			foreach (var column in listMetadata)
			{
				Assert.Contains(result, x => x.Id == column.Id);
			}

			//log
			Assert.NotNull(evt);
			Assert.Equal(LogEventLevel.Information, evt.Level);
			Assert.Equal("Retrieve available columns for a resource type {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
		}
	}
}
