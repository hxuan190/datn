﻿using Domain;
using Moq;
using Serilog;
using Application.IntegrationTest.TestHelpers;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Services;
using Serilog.Events;
using Domain.Entities.Organisations;
using Application.Queries.Groups;
using Util;
using Application.Exceptions;

namespace Application.IntegrationTest.Queries.Groups    
{
    public class GetEmployeesOfGroupTests
    {
        [Fact]
        public async Task ShouldReturnException_WhenGroupNotExists()
        {
            var fakeUser = new FakeUser();
            var newUserProvider = new Mock<IUserProvider>();
            newUserProvider.Setup(x => x.User)
                .Returns(fakeUser);
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            var groupId = "notExistingGroupId";
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetEmployeesOfGroupExecutor(serilogLogger, context, mockAuthorizationService.Object);

            var query = new GetEmployeesOfGroup(groupId);
            var exception = await Assert.ThrowsAsync<NotFoundEntityException<GetEmployeesOfGroupResult>>(async () => await executor.ExecuteAsync(query, fakeUser));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_GROUP, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Queries.Groups.GetEmployeesOfGroupResult. Detail: Not found group", exception.Message);
           
        }   

        [Fact]
        public async Task ShouldReturnEmployees_WhenGroupExists()
        {
            var fakeUser = new FakeUser();
            var newUserProvider = new Mock<IUserProvider>();
            newUserProvider.Setup(x => x.User)
                .Returns(fakeUser);
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            var groupId = "existingGroupId";

            var group = new Group(groupId, "Test Group", "Test Description", false, FakeData.FakeOrganisation.Id);
            context.Groups.Add(group);

            var employee1 = new Employee("fakeEmployeeId1", "fakeOrganisationId", "fakeEmail@fakecompany.com", "fakeFirstName", "fakeLastName", "");
            context.Employees.Add(employee1);

            var groupEmployee1 = new GroupEmployee(FakeData.FakeOrganisation.Id, group.Id, employee1.Id);
            context.GroupEmployees.Add(groupEmployee1);

            await context.SaveChangesAsync();
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetEmployeesOfGroupExecutor(serilogLogger, context, mockAuthorizationService.Object);

            var query = new GetEmployeesOfGroup(groupId);
            var result = await executor.ExecuteAsync(query, fakeUser);
            var employee = result!.FirstOrDefault(x => x.Id == "fakeEmployeeId1");
            Assert.NotNull(employee);
            Assert.Equal("fakeEmployeeId1", employee.Id);
            Assert.Equal("fakeEmail@fakecompany.com", employee.Email);
			//log
			Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Information, evt.Level);
            Assert.Equal("Retrieve employees of a group {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
        }
    }
}
