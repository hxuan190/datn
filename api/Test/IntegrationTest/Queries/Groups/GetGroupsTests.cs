﻿using Application.Queries.Groups;
using Domain;
using Moq;
using Serilog;
using Application.IntegrationTest.TestHelpers;
using Domain.Entities.Organisations;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Services;
using Serilog.Events;

namespace Application.IntegrationTest.Queries.Groups
{
    public class GetGroupsTests
    { 
        [Fact]
        public async Task ShouldReturnGroups_WhenGroupsExists()
        {
            var fakeUser = new FakeUser();
            var newUserProvider = new Mock<IUserProvider>();
            newUserProvider.Setup(x => x.User)
                .Returns(fakeUser);
            var context = new InMemoryKensaDbContext(newUserProvider.Object); 
            context.Groups.Add(new Group("fakeGroupId1", "Test Group", "Test Description", false, FakeData.FakeOrganisation.Id));
            context.Groups.Add(new Group("fakeGroupId2", "Test Group", "Test Description", false, FakeData.FakeOrganisation.Id));
            await context.SaveChangesAsync();
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetGroupsExecutor(serilogLogger, context, mockAuthorizationService.Object);
            
            var query = new GetGroups();
            var result = await executor.ExecuteAsync(query, fakeUser);
            Assert.NotNull(result);
            Assert.True(result.Length == 2);
            var group = result!.FirstOrDefault(x => x.Id == "fakeGroupId1");
            Assert.NotNull(group);
            Assert.Equal("fakeGroupId1", group.Id);
            //log
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Information, evt.Level);
            Assert.Equal("Get all groups {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);

        }
    }
}
