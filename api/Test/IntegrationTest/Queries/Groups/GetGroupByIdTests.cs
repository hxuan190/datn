﻿using Application.Exceptions;
using Application.Queries.Groups;
using Domain;
using Moq;
using Serilog;
using Application.IntegrationTest.TestHelpers;
using Domain.Entities.Organisations;
using Application.IntegrationTest.TestHelpers.Serilog;
using Application.Services;
using Serilog.Events;
using Util;

namespace Application.IntegrationTest.Queries.Groups
{
    public class GetGroupByIdTests
    {
        [Fact]
        public async Task ShouldThrowNotFoundEntityException_WhenGroupDoesNotExist()
        {
            //arrange
            var newUserProvider = new Mock<IUserProvider>();
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            Mock<ILogger> logger = new();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetGroupByIdExecutor(logger.Object, context, mockAuthorizationService.Object);
            var query = new GetGroupById("nonexistentGroupId");
            var fakeUser = new FakeUser();
            //act & assert
            var exception = await Assert.ThrowsAsync<NotFoundEntityException<GetGroupById>>(() =>
                executor.ExecuteAsync(query, fakeUser));
            Assert.Equal(ErrorCodes.APPLICATION_NOTFOUND_ENTITY_GROUP, exception.ErrorCode);
            Assert.Equal("Not found entity when handling Application.Queries.Groups.GetGroupById. Detail: Not found group", exception.Message);
        }

        [Fact]
        public async Task ShouldReturnGroup_WhenGroupExists()
        {
            var fakeUser = new FakeUser();
            var newUserProvider = new Mock<IUserProvider>();
            newUserProvider.Setup(x => x.User)
                .Returns(fakeUser);
            var context = new InMemoryKensaDbContext(newUserProvider.Object);
            var groupId = "existingGroupId";
            var group = new Group(groupId, "Test Group", "Test Description", false, FakeData.FakeOrganisation.Id);
            context.Groups.Add(group);
            await context.SaveChangesAsync();
            LogEvent evt = null!;
            var serilogLogger = new LoggerConfiguration()
                .WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
                .CreateLogger();
            Mock<IAuthorizationService> mockAuthorizationService = new();
            var executor = new GetGroupByIdExecutor(serilogLogger, context, mockAuthorizationService.Object);
            
            var query = new GetGroupById(groupId);
            var result = await executor.ExecuteAsync(query, fakeUser);
            Assert.NotNull(result);
            Assert.Equal(groupId, result.Id);
            Assert.Equal("Test Group", result.Name);
            Assert.Equal("Test Description", result.Description);
            Assert.Equal("fakeOrganisationId", result.OrganisationId);
            Assert.False(result.IsDefault);
            Assert.False(result.IsDeleted);
            //log
            Assert.NotNull(evt);
            Assert.Equal(LogEventLevel.Information, evt.Level);
            Assert.Equal("Retrieve group by Id {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);

        }
    }
}
