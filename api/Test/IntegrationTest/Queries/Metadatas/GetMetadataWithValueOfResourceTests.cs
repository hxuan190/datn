﻿using Application.IntegrationTest.TestHelpers.Serilog;
using Application.IntegrationTest.TestHelpers;
using Application.Services;
using Domain.Entities.Organisations;
using Domain;
using Moq;
using Serilog.Events;
using Serilog;
using Application.Queries.Metadata;

namespace Application.IntegrationTest.Queries.Metadatas
{
	public class GetMetadataWithValueOfResourceTests
	{
		[Fact]
		public async Task Handler_Should_Return_Correct_Data()
		{
			//arrange
			var fakeOrganisationId = "fakeOrganisationId";
			var fakeUserId = "fakeUserId";
			var fakeUser = new BaseUser(fakeOrganisationId, "fakeUserEmail", fakeUserId);
			var fakeEmployeeId = "fakeEmployeeId";
			var newUserProvider = new Mock<IUserProvider>();
			newUserProvider.Setup(x => x.User)
				.Returns(fakeUser);
			var context = new InMemoryKensaDbContext(newUserProvider.Object);
			
			//org
			context.Organisations.Add(new Organisation(fakeOrganisationId, "fakeOwnerId", "fakeOrg", "fakeAddress",
				"fakeAddress2", "fakeCity", "fakeState", "fakeCountry", "fakeZipCode"));
			//employee
			context.Employees.Add(new Employee(fakeEmployeeId, fakeOrganisationId, "fakeEmail", "fakeFirstName", "fakeLastName"));
			//metadata
			context.MetadataCategories.Add(new MetadataCategory("fakeMetadataCategory1", fakeOrganisationId, "fakeMetadataCategory1", 1));
			context.MetadataCategories.Add(new MetadataCategory("fakeMetadataCategory2", fakeOrganisationId, "fakeMetadataCategory2", 2));
			context.Metadatas.Add(new Metadata("fakeMetadata1", fakeOrganisationId, "fakeMetadata1", MetadataDataType.Number, ResourceType.Employee,true, "fakeMetadataCategory1", 1));
			context.Metadatas.Add(new Metadata("fakeMetadata2", fakeOrganisationId, "fakeMetadata2", MetadataDataType.Text, ResourceType.Employee, false, "fakeMetadataCategory1", 2));
			context.Metadatas.Add(new Metadata("fakeMetadata3", fakeOrganisationId, "fakeMetadata3", MetadataDataType.Number, ResourceType.Employee, true, "fakeMetadataCategory2", 1));
			context.Metadatas.Add(new Metadata("fakeMetadata4", fakeOrganisationId, "fakeMetadata4", MetadataDataType.Text, ResourceType.Employee, true, "", 1));
			context.MetadataPredefinedValues.Add(new MetadataPredefinedValue("fakeMetadataPredefinedValue1", fakeOrganisationId, "fakeMetadata2", "value1"));
			context.MetadataPredefinedValues.Add(new MetadataPredefinedValue("fakeMetadataPredefinedValue2", fakeOrganisationId, "fakeMetadata2", "value2"));
			//metadata resource
			context.MetadataResourceValues.Add(new MetadataResourceValue(fakeOrganisationId, ResourceType.Employee,
				fakeEmployeeId, "fakeMetadata1", "fakeValue1"));
			context.MetadataResourceValues.Add(new MetadataResourceValue(fakeOrganisationId, ResourceType.Employee,
				fakeEmployeeId, "fakeMetadata2", "fakeMetadataPredefinedValue1"));
			context.MetadataResourceValues.Add(new MetadataResourceValue(fakeOrganisationId, ResourceType.Employee,
				fakeEmployeeId, "fakeMetadata3", "fakeValue3"));
			context.MetadataResourceValues.Add(new MetadataResourceValue(fakeOrganisationId, ResourceType.Employee,
				fakeEmployeeId, "fakeMetadata4", "fakeValue4"));
			//acl
			context.Acls.Add(new Acl("fakeAclId1", fakeOrganisationId, fakeUserId, SubjectType.Employee,
					AclAccess.Deny, ResourceType.Employee, "fakeMetadata1"));
			context.Acls.Add(new Acl("fakeAclId2", fakeOrganisationId, fakeUserId, SubjectType.Employee,
				AclAccess.Deny, ResourceType.Employee, "fakeMetadata4", fakeEmployeeId));

			await context.SaveChangesAsync();

			Mock<IAuthorizationService> mockAuthorizationService = new();
			
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var aclService = new AclService(context, serilogLogger);
			var executor = new GetMetadataWithValueOfResourceExecutor(serilogLogger,
				context,
				mockAuthorizationService.Object,
				aclService
				);
			var query = new GetMetadataWithValueOfResource(ResourceType.Employee, fakeEmployeeId);

			//act & assert
			var result = await executor.ExecuteAsync(query, fakeUser);
			Assert.NotNull(result);
			Assert.Equal(4, result.Length);
			Assert.Equal("fakeMetadata1", result[0].MetadataId);
			Assert.Equal(ResourceTypeColumnOptions.CLASSIFIED, result[0].Value);
			Assert.Equal("fakeMetadata2", result[1].MetadataId);
			Assert.Equal("fakeMetadataPredefinedValue1", result[1].Value);
			Assert.Equal("fakeMetadata3", result[2].MetadataId);
			Assert.Equal("fakeValue3", result[2].Value);
			Assert.Equal("fakeMetadata4", result[3].MetadataId);
			Assert.Equal(ResourceTypeColumnOptions.CLASSIFIED, result[3].Value);
			//log
			Assert.NotNull(evt);
			Assert.Equal(LogEventLevel.Information, evt.Level);
			Assert.Equal("Retrieve metadata for a resource with type {resourceType} {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
		}
	}
}
