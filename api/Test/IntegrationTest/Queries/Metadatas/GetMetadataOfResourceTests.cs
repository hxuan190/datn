﻿using Application.IntegrationTest.TestHelpers.Serilog;
using Application.IntegrationTest.TestHelpers;
using Application.Services;
using Domain.Entities.Organisations;
using Domain;
using Moq;
using Serilog.Events;
using Serilog;
using Application.Queries.Metadata;

namespace Application.IntegrationTest.Queries.Metadatas
{
	public class GetMetadataOfResourceTests
	{
		[Fact]
		public async Task Handler_Should_Return_Metadata_FromEmployees()
		{
			//arrange
			var fakeUser = new FakeUser();
			var fakeEmployeeId = "fakeEmployeeId";
			var newUserProvider = new Mock<IUserProvider>();
			newUserProvider.Setup(x => x.User)
				.Returns(fakeUser);
			var context = new InMemoryKensaDbContext(newUserProvider.Object);
			var fakeOrganisationId = "fakeOrganisationId";
			context.MetadataCategories.Add(new MetadataCategory("fakeMetadataCategory1", fakeOrganisationId, "fakeMetadataCategory1", 1));
			context.MetadataCategories.Add(new MetadataCategory("fakeMetadataCategory2", fakeOrganisationId, "fakeMetadataCategory2", 2));
			context.Metadatas.Add(new Metadata("fakeMetadata1", fakeOrganisationId, "fakeMetadata1", MetadataDataType.Number, ResourceType.Employee,true, "fakeMetadataCategory1", 1));
			context.Metadatas.Add(new Metadata("fakeMetadata2", fakeOrganisationId, "fakeMetadata2", MetadataDataType.Text, ResourceType.Employee, false, "fakeMetadataCategory1", 2));
			context.Metadatas.Add(new Metadata("fakeMetadata3", fakeOrganisationId, "fakeMetadata3", MetadataDataType.Number, ResourceType.Employee, true, "fakeMetadataCategory2", 1));
			context.Metadatas.Add(new Metadata("fakeMetadata4", fakeOrganisationId, "fakeMetadata4", MetadataDataType.Text, ResourceType.Employee, true, "", 1));
			context.Metadatas.Add(new Metadata("fakeMetadata5", fakeOrganisationId, "fakeMetadata5", MetadataDataType.Text, ResourceType.Site, true, "", 1));
			context.MetadataPredefinedValues.Add(new MetadataPredefinedValue("fakeMetadataPredefinedValue1", fakeOrganisationId, "fakeMetadata2", "value1"));
			context.MetadataPredefinedValues.Add(new MetadataPredefinedValue("fakeMetadataPredefinedValue2", fakeOrganisationId, "fakeMetadata2", "value2"));
			await context.SaveChangesAsync();

			Mock<IAuthorizationService> mockAuthorizationService = new();
			LogEvent evt = null!;
			var serilogLogger = new LoggerConfiguration()
				.WriteTo.Sink(new DelegateSink.DelegatingSink(e => evt = e))
				.CreateLogger();
			var executor = new GetMetadataOfResourceExecutor(serilogLogger,
				context,
				mockAuthorizationService.Object);
			var query = new GetMetadataOfResource(ResourceType.Employee);

			//act & assert
			var result = await executor.ExecuteAsync(query, fakeUser);
			Assert.NotNull(result);
			Assert.Equal(3, result.Length);
			var firstCategory = result[0];
			Assert.Equal(1, firstCategory.DisplayOrder);
			Assert.Equal("fakeMetadataCategory1", firstCategory.Id);
			Assert.Equal("fakeMetadataCategory1", firstCategory.Name);
			Assert.Equal(2, firstCategory.Metadata.Length);
			Assert.Equal(1, firstCategory.Metadata[0].DisplayOrder);
			Assert.Equal("fakeMetadata1", firstCategory.Metadata[0].Id);
			Assert.Equal(2, firstCategory.Metadata[1].DisplayOrder);
			Assert.Equal("fakeMetadata2", firstCategory.Metadata[1].Id);
			Assert.Equal(2, firstCategory.Metadata[1].PredefinedValues.Length);
			Assert.Equal("fakeMetadataPredefinedValue1", firstCategory.Metadata[1].PredefinedValues[0].Id);
			Assert.Equal("fakeMetadataPredefinedValue2", firstCategory.Metadata[1].PredefinedValues[1].Id);
			Assert.Equal("value1", firstCategory.Metadata[1].PredefinedValues[0].Value);
			Assert.Equal("value2", firstCategory.Metadata[1].PredefinedValues[1].Value);
			var secondCategory = result[1];
			Assert.Equal(2, secondCategory.DisplayOrder);
			Assert.Equal("fakeMetadataCategory2", secondCategory.Id);
			Assert.Equal("fakeMetadataCategory2", secondCategory.Name);
			Assert.Equal(1, secondCategory.Metadata.Length);
			Assert.Equal("fakeMetadata3", secondCategory.Metadata[0].Id);
			var thirdCategory = result[2];
			Assert.Equal(1000000, thirdCategory.DisplayOrder);
			Assert.Equal("", thirdCategory.Id);
			Assert.Equal("", thirdCategory.Name);
			Assert.Equal(1, thirdCategory.Metadata.Length);
			Assert.Equal("fakeMetadata4", thirdCategory.Metadata[0].Id);
			//log
			Assert.NotNull(evt);
			Assert.Equal(LogEventLevel.Information, evt.Level);
			Assert.Equal("Retrieve available metadata of resource {ResourceType} {Outcome} in {Elapsed:0.0} ms", evt.MessageTemplate.Text);
		}
	}
}
